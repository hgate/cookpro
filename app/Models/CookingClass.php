<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CookingClass extends Model
{
       	protected $fillable = ['teacher_id',
    					'name',
						'slug',						
						'description',
						'image',
						'time_period',
						'type'
						];
}
