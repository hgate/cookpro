<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
	
    protected $fillable = ['title',
						'slug',
						'preparation_time',
						'cooking_time',
						'difficulty',
						'serve_no_of_people',
						'category_id',						
						'description',
						'image',		
						'user_id',
						'status',
						'video_url'
						];
	public function getTitleAttribute($value)
    {
        return ucfirst($value);
    }	

	public function scopeWithUsers($query){
		$query->leftjoin('users','users.id', '=', 'recipes.user_id')
				->select('recipes.*', 'users.id as userid','users.name as username','users.profile_pic as avatar');
	}

	public function user(){
		return $this->belongsTo('App\Models\User');
	}

	public function teacher(){
		return $this->belongsTo('App\Models\Teacher');
	}

	public function dietitian(){
		return $this->belongsTo('App\Models\Dietitian','id','user_id');
	}

	public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

	public function ingredients(){
		return $this->hasMany('App\Models\RecipeIngredient','recipe_id','id');
	}

	public function nutritions(){
		return $this->hasMany('App\Models\RecipeNutrition','recipe_id','id');
	}

	public function instructions(){
		return $this->hasMany('App\Models\RecipeInstruction','recipe_id','id');
	}
}
