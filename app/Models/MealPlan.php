<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MealPlan extends Model
{
    protected $table = 'meal_plan';
	
	protected $fillable = ['user_id','meal_plan_name','created_at'];
	public function user(){
		return $this->hasMany('App\Models\User','user_id','id');
	}	
}
