<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function recipe() {
    	return $this->belongsTo('App\Models\Recipe');
    }
}
