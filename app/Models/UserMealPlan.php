<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMealPlan extends Model
{
    protected $fillable = ['user_id', 'meal_plan'];
}
