<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DietPlanNutrition extends Model
{
     protected $fillable = ['diet_plan_id','nutrition','value'];

    public function dietplan() {
    	return $this->belongsTo('App\Models\DietPlan');
    }
}
