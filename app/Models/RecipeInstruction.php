<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeInstruction extends Model
{
    protected $fillable = ['recipe_id', 'instructions', 'status'];

    public function recipe() {
    	return $this->belongsTo('App\Models\Recipe');
    }
}
