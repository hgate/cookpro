<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DietPlan extends Model
{
    protected $fillable = ['dietitian_id',
    					'name',
						'slug',						
						'description',
						'sun_breakfast',
						'sun_lunch',
			            'sun_dinner',            
			            'mon_breakfast',
			            'mon_lunch',
			            'mon_dinner',
			            'tue_breakfast',
			            'tue_lunch',
			            'tue_dinner',
			            'wed_breakfast',
			            'wed_lunch',
			            'wed_dinner',
			            'thur_breakfast',
			            'thur_lunch',
			            'thur_dinner', 
			            'fri_breakfast',
			            'fri_lunch',
			            'fri_dinner', 
			            'sat_breakfast',
			            'sat_lunch',
			            'sat_dinner'
						];

	public function dietitian(){
		return $this->belongsTo('App\Models\Dietitian');
	}		

	public function dietplannutrition(){
		return $this->hasMany('App\Models\DietPlanNutrition','diet_plan_id','id');
	}
}
