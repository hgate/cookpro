<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealPlanDetail extends Model
{
    protected $fillable = ['plan_id', 'sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
}
