<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMealRecipe extends Model
{
     protected $table = 'user_meal_recipe';
	
	protected $fillable = ['user_id','meal_plan_id','recipe_id','meal_day', 'created_at', 'updated_at'];
	
	public function user(){
		return $this->belongsTo('App\Models\User','user_id','id');
	}
	
	public function mealplan(){
		return $this->belongsTo('App\Models\MealPlan','meal_plan_id','id');
	}
	
	public function recipe(){
		return $this->belongsTo('App\Models\Recipe','recipe_id','id');
	}
	
}
