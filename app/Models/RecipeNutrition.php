<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeNutrition extends Model
{
    protected $fillable = ['recipe_id', 'name', 'quantity', 'category', 'status'];

    public function recipe() {
    	return $this->belongsTo('App\Models\Recipe');
    }
}
