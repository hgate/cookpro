<?php
$router->resource('/planWeeklyMenu','MealPlanController');

$router->get('/planWeeklyMenu/{id?}','MealPlanController@index');

$router->get('/destroy/{id?}','MealPlanController@destroy');

$router->post('planWeeklyMenu/store','MealPlanController@store');