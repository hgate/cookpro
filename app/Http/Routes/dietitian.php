<?php
$router->get('register','AuthController@showRegistrationForm');
$router->post('register','AuthController@register');
$router->get('logout','AuthController@logout');
$router->get('register/confirm/{token}','AuthController@confirmEmail');
$router->get('myAccount','AuthController@myAccount');

$router->post('addDietPlan','DietitianPlanController@store');

$router->get('dietitianPlan','DietitianPlanController@index');

$router->get('/diet-plan/{slug?}','DietitianPlanController@show');

$router->get('/dietitian-profile/{id?}','DietitianPlanController@dietitianprofile');

