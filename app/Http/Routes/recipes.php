<?php
$router->get('/submitrecipe','RecipeController@create');
$router->get('/listrecipes','RecipeController@recipes');
$router->get('/recipe/{slug?}','RecipeController@recipe');
$router->get('/userprofile/{id?}','RecipeController@userprofile');
$router->resource('recipes','RecipeController');
$router->post('/addmealplan','RecipeController@addmealplan');