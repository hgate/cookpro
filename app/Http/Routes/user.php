<?php
//Verify email Address
$router->get('register/confirm/{token}','AuthController@confirmEmail');

$router->get('myAccount','AuthController@myAccount');

$router->post('myAccount/changeProfilePic','AuthController@changeProfilePic');

$router->post('myAccount/updatePofile','AuthController@updatePofile');

$router->post('myAccount/deleteAccount','AuthController@deleteAccount');