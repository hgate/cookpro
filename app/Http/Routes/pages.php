<?php
$router->get('/','PagesController@home');

$router->get('/searchrecipe', function () {
    return view('searchrecipe');
});

$router->get('/planWeeklyMenu1', function () {
    return view('planWeeklyMenu.second_design');
});

$router->get('/planWeeklyMenu2', function () {
    return view('planWeeklyMenu.second_design2');
});

$router->get('/shop', function () {
    return view('shop.index');
});

$router->get('/shopping-list', function () {
    return view('shop.shopping');
});