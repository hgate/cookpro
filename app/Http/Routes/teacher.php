<?php
$router->get('register','AuthController@showRegistrationForm');

$router->post('register','AuthController@register');

$router->get('logout','AuthController@logout');

$router->get('register/confirm/{token}','AuthController@confirmEmail');

$router->get('myAccount','AuthController@myAccount');

$router->post('myAccount/changeProfilePic','AuthController@changeProfilePic');

$router->post('myAccount/updatePofile','AuthController@updatePofile');

$router->post('myAccount/deleteAccount','AuthController@deleteAccount');

$router->get('cookingclasses','CookingClasses@index');

$router->post('addCookingCourse','CookingClasses@store');

$router->get('cookingcourse/{slug?}','CookingClasses@show');

$router->get('instructorprofile/{id?}','CookingClasses@instructorprofile');

$router->get('joincourse','CookingClasses@joincourse');