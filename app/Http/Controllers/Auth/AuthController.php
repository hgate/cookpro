<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Auth;
use App\Mailers\Confirm\UserMailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/'; 

    public function __construct()
    {
        $this->middleware('auth', ['only' => ['changeProfilePic','myAccount','updatePofile']]);        
    }
     

     public function login(Request $request){          
        $this->validate($request, ['who' => 'required','email' => 'required|email', 'password' => 'required']);
        if($request->who === "user")
        {
            if ($this->userSignIn($request)) 
            {           
            return redirect()->intended('/');
            }
        } 
        elseif ($request->who === "teacher") 
        {
          if($this->teacherSignIn($request))
          {
            return redirect()->intended('/');
          }
        } 
        elseif ($request->who === "dietitian") 
        {
          if($this->dietitianSignIn($request))
          {
            return redirect()->intended('/');
          }
        }
                
        return redirect()->back()->with('error','Could not sign you in.');
     }

     protected function getCredentials(Request $request)
     {
        return [
            'email'    => $request->input('email'),
            'password' => $request->input('password'),
            'verified' => true
        ];
     }

     protected function userSignIn(Request $request)
     {
        return Auth::attempt($this->getCredentials($request), $request->has('remember'));
     }

     protected function teacherSignIn(Request $request)
     {        
        return Auth::guard('teacher')->attempt($this->getCredentials($request), $request->has('remember'));
     }

     protected function dietitianSignIn(Request $request)
     {
        return Auth::guard('dietitian')->attempt($this->getCredentials($request), $request->has('remember'));
     }   
   
    public function register(Request $request, UserMailer $mailer){          
        $rules =  [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'username' => 'required|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ];

        $this->validate($request,$rules);

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'username' => $request['username'],
            'role' => 'user',
            'password' => bcrypt($request['password']),
        ]); 

        $mailer->sendEmailConfirmationTo($user);    

        return redirect()->back()->with('status','Please verify your email Address');    
    }   
     
     public function confirmEmail($token)
     {
        User::whereToken($token)->firstOrFail()->confirmEmail();        
        return redirect('login')->with('status','You are now confirmed. Please login.');
     } 

     public function myAccount(){
        $recipes = User::find(Auth::user()->id)->recipes;        
        return view('auth.my_account',compact('recipes'));
     }

     public function changeProfilePic(Request $request){
        $this->validate($request,['profile_pic' => 'required|image|mimes:jpeg,jpg,bmp,png|max:5000']);

        $file = $request->file('profile_pic');

        $picName = str_random(30).$file->getClientOriginalName();

        $pervImage = Auth::user()->profile_pic;
        
        \File::delete('uploads/users/'.$pervImage);

        $user = User::findOrFail(Auth::user()->id);

        $user->profile_pic = $picName;

        $user->save();

        $file->move('uploads/users/',$picName);

        return redirect()->back();
     } 

     public function updatePofile(Request $request) {        

        $response = [];

        if($request->has('what')) {
            $user = User::findOrFail(Auth::user()->id);

            if($request->what === "name") {
                $this->validate($request,['name' => 'required|max:200']);
                $user->name = $request->name;
                $user->save();
            } elseif ($request->what === "pwd") {
                $this->validate($request,['old' => 'required','new' => 'required']);
                if (\Hash::check($request->old, Auth::user()->password)) {
                    $user->password = bcrypt($request->new);
                    $user->save();
                    $response['success'] = true;
                    $response['message'] = "Password changed successfully !";
                    return $response;
                } else {
                    $response['success'] = false;
                    $response['message'] = "Old password you entered is wrong !";
                    return $response;
                }                
                 
            }       

            $response['success'] = true;
        } else {
            $response['success'] = false;
        }

        return $response;
     }

     public function deleteAccount()
     {
        $user = User::findOrFail(Auth::user()->id);
        $user->delete();
        return redirect('/');
     }

}
