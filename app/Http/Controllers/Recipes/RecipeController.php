<?php

namespace App\Http\Controllers\Recipes;

use Illuminate\Http\Request;
use App\Models\Recipe;
use App\Models\MealPlan;
use App\Models\UserMealRecipe;
use App\Models\User;
use App\Models\Category;
use App\Models\RecipeIngredient;
use App\Models\RecipeInstruction;
use App\Models\RecipeNutrition;
use App\Http\Requests\RecipesRequest;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Input;
class RecipeController extends Controller
{
    
    public function __construct(){
       // $this->middleware('auth',['except' => ['recipes', 'recipe']]);
    }
    protected function getUserId(){
        if (Auth::check()) {
            return Auth::user()->id;
        } else if (Auth::guard('teacher')->check()) {
            return Auth::guard('teacher')->user()->id;
        } else if (Auth::guard('dietitian')->check()) {
            return Auth::guard('dietitian')->user()->id;
        }
    }
    public function index()
    {
        $recipes = Recipe::withUsers()->latest()->get();

        return view('recipes.index',compact('recipes'));
    }
    
    public function create()
    {
        $categories = DB::table('categories')->select('id','name')->get();
        return view('recipes.create')->withCategories($categories);
    }
    
    public function store(RecipesRequest $request)
    {                    

        $ingredients = [];

        $ingredients['name'] = $request->ingredients['name'];

        $ingredients['qty'] = $request->ingredients['qty'];

        $ingredients['unit'] = $request->ingredients['unit'];

        $ingredientsCounter = count($ingredients['name']);

        $nutrition = [];

        $nutrition['name'] = $request->nutrition['name'];

        $nutrition['size'] = $request->nutrition['size'];

        $nutrition['category'] = $request->nutrition['category'];         

        $nutritionCounter = count($nutrition['name']);       

        $publishOrNot = ($request->has('publish') && $request->publish === "1" || $request->publish === "0") ? $request->publish : 0;

        $file = $request->file('recepiephoto');
        $picName = str_random(30).$file->getClientOriginalName();

       

        $addRecipe = Recipe::create([
            'title' => $request->title,
            'slug' => str_slug($request->title,'-'),
            'user_id' => $this->getUserId(),
            'image' => $picName,
            'preparation_time' => $request->preparation_time,
            'cooking_time' => $request->cooking_time,
            'difficulty' => $request->difficulty,            
            'serve_no_of_people' => $request->serve_no_of_people,
            'category_id' => $request->category,
            'description' => $request->description,            
            'video_url' => $request->videourl,            
            'status' => $publishOrNot,            
        ]);

        $recipeId = $addRecipe->id;

        // add recipe ingredients

        for ($i=0; $i < $ingredientsCounter; $i++) { 
            RecipeIngredient::create([
                'recipe_id' => $recipeId,
                'name' => $ingredients['name'][$i],
                'quantity' => $ingredients['qty'][$i],
                'unit' => $ingredients['unit'][$i],
                'status' => $publishOrNot
            ]);
        }

        // add recipe instructions

        foreach ($request->instructions as $instruction) { 
            RecipeInstruction::create([
                'recipe_id' => $recipeId,
                'instructions' => $instruction,
                'status' => $publishOrNot                
            ]);
        }

        //add nutrition facts

        // recipe_id        name        quantity        category
        for ($j=0; $j < $nutritionCounter; $j++) { 
            RecipeNutrition::create([
                'recipe_id' => $recipeId,
                'quantity' => $nutrition['name'][$j],
                'unit' => $nutrition['size'][$j],
                'name' => $nutrition['category'][$j],
                'status' => $publishOrNot
            ]);
        }

        $file->move('uploads/recipes/',$picName);

        return redirect('/listrecipes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {        
        $recipe = Recipe::findOrFail($id);

        $categories = DB::table('categories')->lists('name','id');

        $nutritions  = $recipe->nutritions;

        return view('recipes.edit',[
             'recipe' => $recipe,
             'user' => $recipe->user, 
             'nutritions' => $recipe->nutritions, 
             'ingredients' =>  $recipe->ingredients,
             'instructions' => $recipe->instructions,
             'categories' => $categories
            ]);
    }

   
    public function update(RecipesRequest $request, $id)
    {        
        $recipe =  Recipe::findOrFail($id)->whereUserId($this->getUserId())->first();
        
        RecipeInstruction::whereRecipeId($id)->delete();
        RecipeNutrition::whereRecipeId($id)->delete();
        RecipeIngredient::whereRecipeId($id)->delete();

        $ingredients = [];

        $ingredients['name'] = $request->ingredients['name'];

        $ingredients['qty'] = $request->ingredients['qty'];

        $ingredients['unit'] = $request->ingredients['unit'];

        $ingredientsCounter = count($ingredients['name']);

        $nutrition = [];

        $nutrition['name'] = $request->nutrition['name'];

        $nutrition['size'] = $request->nutrition['size'];

        $nutrition['category'] = $request->nutrition['category'];         

        $nutritionCounter = count($nutrition['name']);       

        $publishOrNot = ($request->has('publish') && $request->publish === "1" || $request->publish === "0") ? $request->publish : 0;

        if($request->hasFile('recepiephoto')){
            $file = $request->file('recepiephoto');
            $picName = str_random(30).$file->getClientOriginalName();
        }

        $pervImage = $recipe->image;

        $recipe->title = $request->title;
        $recipe->slug = str_slug($request->title,'-');
        $recipe->user_id = $this->getUserId();
        $recipe->image = ($request->hasFile('recepiephoto')) ? $picName : $pervImage;
        $recipe->preparation_time = $request->preparation_time;
        $recipe->cooking_time = $request->cooking_time;
        $recipe->difficulty = $request->difficulty;
        $recipe->serve_no_of_people = $request->serve_no_of_people;
        $recipe->category_id = $request->category;
        $recipe->description = $request->description;
        $recipe->video_url = $request->videourl;
        $recipe->status = $publishOrNot;
        $recipe->save();

        $recipeId = $id;

        // add recipe ingredients

        for ($i=0; $i < $ingredientsCounter; $i++) { 
            RecipeIngredient::create([
                'recipe_id' => $recipeId,
                'name' => $ingredients['name'][$i],
                'quantity' => $ingredients['qty'][$i],
                'unit' => $ingredients['unit'][$i],
                'status' => $publishOrNot
            ]);
        }

        // add recipe instructions

        foreach ($request->instructions as $instruction) { 
            RecipeInstruction::create([
                'recipe_id' => $recipeId,
                'instructions' => $instruction,
                'status' => $publishOrNot                
            ]);
        }

        //add nutrition facts

        
        for ($j=0; $j < $nutritionCounter; $j++) { 
            RecipeNutrition::create([
                'recipe_id' => $recipeId,
                'quantity' => $nutrition['name'][$j],
                'unit' => $nutrition['size'][$j],
                'name' => $nutrition['category'][$j],
                'status' => $publishOrNot
            ]);
        }
        if($request->hasFile('recepiephoto')){
            \File::delete('uploads/recipes/'.$pervImage);

            $file->move('uploads/recipes/',$picName);
        }
        return redirect('/listrecipes');
       
       
    }
   
    public function destroy(int $id)
    {
       $recipe = Recipe::findOrFail($id);
       \File::delete('uploads/recipes/'.$$recipe->image);
       $recipe->delete();
       return redirect()->back();
    }    

    public function recipes()
    {
        $recipes = Recipe::withUsers()->latest()->get();
		$mymealplan=array();
		if (Auth::check()) {
			$mymealplan = MealPlan::where('user_id',Auth::user()->id)->lists('meal_plan_name', 'id');
			$mymealplan->prepend('Select Mealplan', '');
		}

        return view('recipes.index',compact(['recipes','mymealplan']));
    }
	
	public function addmealplan(Request $request)
    {
        $input=Input::all();
		if(isset($input['meal_plan_id']) && isset($input['meal_day'])){
			$add_meal_plan = UserMealRecipe::create([
				'user_id' => $this->getUserId(),
				'meal_plan_id' => $input['meal_plan_id'],
				'recipe_id' => $input['recipe_id'],
				'meal_day'=>$input['meal_day']
			]);
		}
		
		return redirect('listrecipes')->with('status','Reciepe Added to your mealplan');
    }

    public function recipe($slug = null)
    {
        if($slug === null)
        {
            return $this->recipes();
        }

        $recipe = Recipe::where('slug', 'like', '%'.$slug.'%')->first();
        
        return view('recipes.recipe',
            [
             'recipe' => $recipe,
             'user' => $recipe->user,
             'category' => $recipe->category,  
             'nutritions' => $recipe->nutritions, 
             'ingredients' =>  $recipe->ingredients,
             'instructions' => $recipe->instructions
            ]);
    }

    public function userprofile($id = null)
    {
        $user = User::where('id', $id)->first();
        return view('userprofile',
            [
             'user' => $user,
             'recipes' => $user->recipes,
            ]);

    }
    
}
