<?php

namespace App\Http\Controllers\Teachers;

use App\Models\Teacher;
use Auth;
use App\Mailers\Confirm\TeacherMailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    
    // use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/'; 

    public function __construct()
    {
        $this->middleware('teacher', ['only' => 'logout','myAccount']);
    } 

    public function showRegistrationForm()
    {
        return view('auth.teacher.register');
    }      
   
    public function register(Request $request, TeacherMailer $mailer){ 

        $rules =  [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:teachers',
            'username' => 'required|max:255|unique:teachers',
            'password' => 'required|min:6|confirmed',
        ];

        $this->validate($request,$rules);

        $teacher = Teacher::create([
            'name' => $request['name'],
            'email' => $request['email'],            
            'username' => $request['username'],            
            'password' => bcrypt($request['password']),
        ]); 

        $mailer->sendEmailConfirmationTo($teacher);    

        return redirect()->back()->with('status','Please verify your email Address');    
    }

     public function logout() 
    {        
        Auth::guard('teacher')->logout();
        return redirect('/');
    }   
     
     public function confirmEmail($token)
     {
        Teacher::whereToken($token)->firstOrFail()->confirmEmail();        
        return redirect('login')->with('status','You are now confirmed. Please login.');
     }  

     public function myAccount(){
        $recipes = Teacher::findOrFail(Auth::guard('teacher')->user()->id)->recipes; 
        
        return view('auth.teacher.teacher_account')->withRecipes($recipes);
     }

     public function changeProfilePic(Request $request){
        $this->validate($request,['profile_pic' => 'required|image|mimes:jpeg,jpg,bmp,png|max:5000']);

        $file = $request->file('profile_pic');

        $picName = str_random(30).$file->getClientOriginalName();

        $pervImage = Auth::guard('teacher')->user()->profile_pic;
        
        \File::delete('uploads/users/'.$pervImage);

        $user = Teacher::findOrFail(Auth::guard('teacher')->user()->id);

        $user->profile_pic = $picName;

        $user->save();

        $file->move('uploads/users/',$picName);

        return redirect()->back();
     } 

     public function updatePofile(Request $request) {        

        $response = [];

        if($request->has('what')) {
            $user = User::findOrFail(Auth::guard('teacher')->user()->id);

            if($request->what === "name") {
                $this->validate($request,['name' => 'required|max:200']);
                $user->name = $request->name;
                $user->save();
            } elseif ($request->what === "pwd") {
                $this->validate($request,['old' => 'required','new' => 'required']);
                if (\Hash::check($request->old, Auth::guard('teacher')->user()->password)) {
                    $user->password = bcrypt($request->new);
                    $user->save();
                    $response['success'] = true;
                    $response['message'] = "Password changed successfully !";
                    return $response;
                } else {
                    $response['success'] = false;
                    $response['message'] = "Old password you entered is wrong !";
                    return $response;
                }                
                 
            }       

            $response['success'] = true;
        } else {
            $response['success'] = false;
        }

        return $response;
     }

     public function deleteAccount()
     {
        $user = User::findOrFail(Auth::guard('teacher')->user()->id);
        $user->delete();
        return redirect('/');
     }

}
