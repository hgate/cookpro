<?php

namespace App\Http\Controllers\Teachers;

use App\Models\CookingClass;
use App\Models\User;
use App\Models\Teacher;
use Auth;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CookingClasses extends Controller
{
    
    protected function getUserId(){
        if (Auth::check()) {
            return Auth::user()->id;
        } else if (Auth::guard('teacher')->check()) {
            return Auth::guard('teacher')->user()->id;
        } else if (Auth::guard('dietitian')->check()) {
            return Auth::guard('dietitian')->user()->id;
        }
    }
    
    public function index()
    {   
        $cookingclasses = CookingClass::latest()->get();
        return view('cooking.index',compact('cookingclasses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $addDietPlan = CookingClass::create([
            'teacher_id' =>  $this->getUserId(),
            'name' => $request->name,
            'slug' => str_slug($request->name,'-'),
            'description' => $request->description, 
            'time_period' => $request->time_period,
            'type' => $request->type,
        ]);

        return redirect('teacher/cookingclasses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug = null)
    {
        $cookingclass = CookingClass::where('slug', 'like', '%'.$slug.'%')->first();
        return view('cooking.show',compact('cookingclass'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function instructorprofile($id = null)
    {
        $profile = Teacher::where('id', $id)->first();
        return view('cooking.instructor_profile',
            [
             'profile' => $profile
             
            ]);
    }

    public function joincourse()
    {
        return view('cooking.cooking_membership');
    }
}
