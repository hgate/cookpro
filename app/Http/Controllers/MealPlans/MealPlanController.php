<?php

namespace App\Http\Controllers\MealPlans;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use Auth;
use Carbon\Carbon;
use App\Models\MealPlan;
use App\Models\UserMealRecipe;

class MealPlanController extends Controller
{
    public function __construct(){
        //$this->middleware('auth');
    }
    
    public function index($id='')
    {
        if (Auth::check()) {
            $meal_plan = MealPlan::where('user_id',Auth::user()->id)->get();
            if($id){
                $user_meal = UserMealRecipe::where(['user_id'=>Auth::user()->id, 'meal_plan_id'=>$id])->get();
            }
            else{
                $user_meal = UserMealRecipe::where('user_id',Auth::user()->id)->get();
            }
        }
        else{
            $meal_plan='';
            $user_meal ='';
        }
        
        return view('planWeeklyMenu.index')->with('meal_plan', $meal_plan)->with('user_meal',$user_meal);
    }

    protected function getUserId(){
        if (Auth::check()) {
            return Auth::user()->id;
        } else if (Auth::guard('teacher')->check()) {
            return Auth::guard('teacher')->user()->id;
        } else if (Auth::guard('dietitian')->check()) {
            return Auth::guard('dietitian')->user()->id;
        }
    }
  
    
    public function store(Request $request)
    {
        $input=Input::all();
        if(Auth::check()){
            if(isset($input['meal_plan_name'])){
                $meal_plan = MealPlan::create([
                    'user_id' => $this->getUserId(),
                    'meal_plan_name' => $input['meal_plan_name'],
                    'created_at'=>Carbon::now()
                ]);
            }
            $status='Your Mealplan Added';
        }
        else{
            $status='Please Login to add Mealplan !';
        }
        
        return redirect('planWeeklyMenu')->with('status',$status);
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        UserMealRecipe::where('id',$id)->delete();
        return redirect('planWeeklyMenu')->with('status','Item Deleted');
    }
}
