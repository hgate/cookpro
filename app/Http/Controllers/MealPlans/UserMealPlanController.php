<?php

namespace App\Http\Controllers\MealPlans;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MealPlanRequest;

class UserMealPlanController extends Controller
{
    public function __construct(){
        //$this->middleware('auth');
    }
    
    public function index()
    {
        return view('planWeeklyMenu.index');
    }

    
    public function create()
    {
        //
    }

    
    public function store(MealPlanRequest $request)
    {
        return $request;
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
