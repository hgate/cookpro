<?php

namespace App\Http\Controllers\Dietitians;

use Illuminate\Http\Request;
use App\Models\DietPlan;
use App\Models\DietPlanNutrition;
use App\Models\User;
use App\Models\Dietitian;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class DietitianPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected function getUserId(){
        if (Auth::check()) {
            return Auth::user()->id;
        } else if (Auth::guard('teacher')->check()) {
            return Auth::guard('teacher')->user()->id;
        } else if (Auth::guard('dietitian')->check()) {
            return Auth::guard('dietitian')->user()->id;
        }
    }

    public function index()
    {
        $dietplans = DietPlan::latest()->get();
        return view('dietitianPlan.index',compact('dietplans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nutrition = [];

        $nutrition['nutrition'] = $request->nutrition['nutrition'];

        $nutrition['value'] = $request->nutrition['value'];        

        $nutritionCounter = count($nutrition['nutrition']);  
        
        $addDietPlan = DietPlan::create([
            'dietitian_id' => $this->getUserId(),
            'name' => $request->name,
            'slug' => str_slug($request->name,'-'),
            'description' => $request->description, 
            'sun_breakfast' => $request->sun_breakfast,
            'sun_lunch' => $request->sun_lunch,
            'sun_dinner' => $request->sun_dinner,            
            'mon_breakfast' => $request->mon_breakfast,
            'mon_lunch' => $request->mon_lunch,
            'mon_dinner' => $request->mon_dinner,
            'tue_breakfast' => $request->tue_breakfast,
            'tue_lunch' => $request->tue_lunch,
            'tue_dinner' => $request->tue_dinner,
            'wed_breakfast' => $request->wed_breakfast,
            'wed_lunch' => $request->wed_lunch,
            'wed_dinner' => $request->wed_dinner,
            'thur_breakfast' => $request->thur_breakfast,
            'thur_lunch' => $request->thur_lunch,
            'thur_dinner' => $request->thur_dinner, 
            'fri_breakfast' => $request->fri_breakfast,
            'fri_lunch' => $request->fri_lunch,
            'fri_dinner' => $request->fri_dinner, 
            'sat_breakfast' => $request->sat_breakfast,
            'sat_lunch' => $request->sat_lunch,
            'sat_dinner' => $request->sat_dinner,
        ]);

        $addDietPlanId = $addDietPlan->id;


        for ($j=0; $j < $nutritionCounter; $j++) { 
            DietPlanNutrition::create([
                'recipe_id' => $addDietPlanId,
                'nutrition' => $nutrition['nutrition'][$j],
                'value' => $nutrition['value'][$j]
            ]);
        }

        return redirect('dietitian/dietitianPlan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($slug = null)
    {   
        if($slug === null)
        {
            return $this->dietplans();
        }

        $dietplan = DietPlan::where('slug', 'like', '%'.$slug.'%')->first();

        return view('dietitianPlan.show',compact('dietplan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dietitianprofile($id = null)
    {
        $profile = Dietitian::where('id', $id)->first();
        return view('dietitianPlan.dietitian_profile',
            [
             'profile' => $profile,
             'dietplans' => $profile->dietplans
            ]);
    }

}
