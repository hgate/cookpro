<?php

namespace App\Http\Controllers\Dietitians;

use App\Models\Dietitian;
use App\Models\DietPlan;
use App\Models\DietPlanNutrition;
use Auth;
use App\Mailers\Confirm\DietitianMailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    
    // use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/'; 

    public function __construct()
    {
        //$this->middleware('dietitian', ['only' => 'logout','myAccount','addDietPlan']);
    } 

    protected function getUserId(){
        if (Auth::check()) {
            return Auth::user()->id;
        } else if (Auth::guard('teacher')->check()) {
            return Auth::guard('teacher')->user()->id;
        } else if (Auth::guard('dietitian')->check()) {
            return Auth::guard('dietitian')->user()->id;
        }
    }

    public function showRegistrationForm()
    {
        return view('auth.dietitian.register');
    }      
   
    public function register(Request $request, DietitianMailer $mailer){          
        $rules =  [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:dietitians',
            'username' => 'required|max:255|unique:teachers',
            'password' => 'required|min:6|confirmed',
        ];

        $this->validate($request,$rules);

        $dietitian = Dietitian::create([
            'name' => $request['name'],
            'email' => $request['email'],            
            'username' => $request['username'],            
            'password' => bcrypt($request['password']),
        ]); 

        $mailer->sendEmailConfirmationTo($dietitian);    

        return redirect()->back()->with('status','Please verify your email Address');    
    }

    public function logout() 
    {        
        Auth::guard('dietitian')->logout();
        return redirect('/');
    }   
     
    public function confirmEmail($token)
    {
        Dietitian::whereToken($token)->firstOrFail()->confirmEmail();        
        return redirect('login')->with('status','You are now confirmed. Please login.');
    }  

    public function myAccount()
    {   
        
        $dietitian = Dietitian::findOrFail(Auth::guard('dietitian')->user()->id); 
        return view('auth.dietitian.dietitian_account',
            [
             'dietitian'=>  $dietitian,
             'dietplans' => $dietitian->dietplans
            ]);
    }

    public function changeProfilePic(Request $request){
        $this->validate($request,['profile_pic' => 'required|image|mimes:jpeg,jpg,bmp,png|max:5000']);

        $file = $request->file('profile_pic');

        $picName = str_random(30).$file->getClientOriginalName();

        $pervImage = Auth::guard('dietitian')->user()->profile_pic;
        
        \File::delete('uploads/users/'.$pervImage);

        $user = Dietitian::findOrFail(Auth::guard('dietitian')->user()->id);

        $user->profile_pic = $picName;

        $user->save();

        $file->move('uploads/users/',$picName);

        return redirect()->back();
    } 

    public function updatePofile(Request $request) {        

        $response = [];

        if($request->has('what')) {
            $user = User::findOrFail(Auth::guard('dietitian')->user()->id);

            if($request->what === "name") {
                $this->validate($request,['name' => 'required|max:200']);
                $user->name = $request->name;
                $user->save();
            } elseif ($request->what === "pwd") {
                $this->validate($request,['old' => 'required','new' => 'required']);
                if (\Hash::check($request->old, Auth::guard('dietitian')->user()->password)) {
                    $user->password = bcrypt($request->new);
                    $user->save();
                    $response['success'] = true;
                    $response['message'] = "Password changed successfully !";
                    return $response;
                } else {
                    $response['success'] = false;
                    $response['message'] = "Old password you entered is wrong !";
                    return $response;
                }                
                 
            }       

            $response['success'] = true;
        } else {
            $response['success'] = false;
        }

        return $response;
    }

     public function deleteAccount()
     {
        $user = User::findOrFail(Auth::guard('dietitian')->user()->id);
        $user->delete();
        return redirect('/');
     }

     public function store(Request $request)
     {
        $nutrition = [];

        $nutrition['nutrition'] = $request->nutrition['nutrition'];

        $nutrition['value'] = $request->nutrition['value'];        

        $nutritionCounter = count($nutrition['nutrition']);  
        
        $addDietPlan = DietPlan::create([
            'dietitian_id' => $this->getUserId(),
            'name' => $request->name,
            'slug' => str_slug($request->name,'-'),
            'description' => $request->description, 
            'sun_breakfast' => $request->sun_breakfast,
            'sun_lunch' => $request->sun_lunch,
            'sun_dinner' => $request->sun_dinner,            
            'mon_breakfast' => $request->mon_breakfast,
            'mon_lunch' => $request->mon_lunch,
            'mon_dinner' => $request->mon_dinner,
            'tue_breakfast' => $request->tue_breakfast,
            'tue_lunch' => $request->tue_lunch,
            'tue_dinner' => $request->tue_dinner,
            'wed_breakfast' => $request->wed_breakfast,
            'wed_lunch' => $request->wed_lunch,
            'wed_dinner' => $request->wed_dinner,
            'thur_breakfast' => $request->thur_breakfast,
            'thur_lunch' => $request->thur_lunch,
            'thur_dinner' => $request->thur_dinner, 
            'fri_breakfast' => $request->fri_breakfast,
            'fri_lunch' => $request->fri_lunch,
            'fri_dinner' => $request->fri_dinner, 
            'sat_breakfast' => $request->sat_breakfast,
            'sat_lunch' => $request->sat_lunch,
            'sat_dinner' => $request->sat_dinner,
        ]);

        $addDietPlanId = $addDietPlan->id;


        for ($j=0; $j < $nutritionCounter; $j++) { 
            DietPlanNutrition::create([
                'recipe_id' => $addDietPlanId,
                'nutrition' => $nutrition['nutrition'][$j],
                'value' => $nutrition['value'][$j]
            ]);
        }

        return redirect('/dietitianPlan');
     }
}
