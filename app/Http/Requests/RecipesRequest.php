<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\Request as RecipeRquestHandler;
class RecipesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(RecipeRquestHandler $handler)
    {       

        $handler->session()->flash('ingredients_counter', count($handler->ingredients['name']));

        $handler->session()->flash('instructions_counter', count($handler->instructions));

        $handler->session()->flash('nutrition_counter', count($handler->nutrition['name']));        

        return [
            'title' => 'required|max:100',
            'description' => 'required|max:500',
            'preparation_time' => 'required|numeric',
            'cooking_time' => 'required|numeric',
            'difficulty' => 'required|in:hard,easy,medium',
            'recepiephoto' => 'required_if:image_uploaded,empty|image|mimes:jpeg,jpg,bmp,png|max:5000',
            'serve_no_of_people' => 'required',
            'category' => 'required',
            'videourl' => 'url|active_url',
            'nutrition.name.*' => 'required|max:100',
            'nutrition.size.*' => 'required',
            'nutrition.category.*' => 'required',
            'ingredients.name.*' => 'required|max:100',
            'ingredients.qty.*' => 'required|numeric',
            'ingredients.unit.*' => 'required',
            'instructions.*' => 'required',

        ];
    }

    public function messages(){
        return [
        'serve_no_of_people.required' => 'Serve No. of people field is required',
        'category.required' => 'Please select category',
        'recepiephoto.required_if' => 'Please add Recipe Photo',
        'recepiephoto.mimes' => 'Only JPG|PNG|BMP Formats are allowed (Recepie image)',
        'instructions.*.required' => 'Please fill up all the instructions',
        'nutrition.name.*.required' => 'Nutrition name can not be empty',
        'nutrition.size.*.required' => 'Nutrition size can not be empty',
        'nutrition.category.*.required' => 'Nutrition category can not be empty',

        'ingredients.name.*.required' => 'Ingredient name can not be empty',
        'ingredients.qty.*.required' => 'Ingredient quantity can not be empty',
        'ingredients.unit.*.required' => 'Ingredient unit can not be empty'
        ];
    }
}
