<?php

$router->group(['middleware' => ['web','auth']], function ($router) {  

});

$router->group(['middleware' => 'web'], function ($router) {
    
    $router->auth(); 

    /*
	|-------------------------------
	|  Auth Router
	|-------------------------------
    */
    
    $router->group(['namespace' => 'Auth'],function($router){
        require app_path('Http/Routes/user.php');
    });

    /*
	|-------------------------------
	|  Pages Router
	|-------------------------------
    */

	$router->group(['namespace' => 'Pages'],function($router){
        require app_path('Http/Routes/pages.php');
    });

    /*
    |-------------------------------
    |  Recipes Router
    |-------------------------------
    */

    $router->group(['namespace' => 'Recipes'],function($router){
        require app_path('Http/Routes/recipes.php');
    });

    /*
    |-------------------------------
    |  Teachers Router
    |-------------------------------
    */

    $router->group(['namespace' => 'Teachers', 'prefix' => 'teacher'],function($router){
        require app_path('Http/Routes/teacher.php');
    });

    /*
    |-------------------------------
    |  Dietitian Router
    |-------------------------------
    */

    $router->group(['namespace' => 'Dietitians', 'prefix' => 'dietitian'],function($router){
        require app_path('Http/Routes/dietitian.php');
    });

    /*
    |-------------------------------
    |  Meal Plan Router
    |-------------------------------
    */

    $router->group(['namespace' => 'MealPlans'],function($router){
        require app_path('Http/Routes/meal_plans.php');
    });
    
});