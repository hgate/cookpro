/**
 * Created by Purohit on 4/24/2016.
 */
var RowCount = 1;
var NutritionCount = 1;
var DietNutritionCount = 1;
var StepCount = 1;

function addIndrediants() {
    RowCount = RowCount + 1;
    var data="<div class='f-row ingredient'>" +
        "<div class='large'>" +
            "<input type='text' name='ingredients[name][]' id='inputIngredients"+RowCount +"' placeholder='Ingredient' />" +
        "</div>" +
        "<div class='small'>" +
            "<input type='text' name='ingredients[qty][]' id='inputQuantity"+RowCount +"' placeholder='Quantity' />" +
        "</div>" +
        "<div class='third'><select class='f-row ingredient' name='ingredients[unit][]' id='inputUnit"+RowCount +"'>" +
            "<option selected disabled>Select Unit</option>" +
            "<option value='g'>g</option>" +
            "<option value='tbsp'>tbsp</option>" +
            "<option value='ml'>ml</option>" +
            "<option value='sprinkles'>Sprinkles</option>" +
        "</select>" +
        "</div>" +
        "<button class='remove' id='removeRow'>-</button>" +
        "</div>";
    $("#mainDivIngredients").append(data);
    $("#mainDivIngredients").on('click', '#removeRow', function () {
        $(this).parent().remove();
    });
}

function addNutrition() {
    NutritionCount = NutritionCount + 1;
    var data="<div class='f-row ingredient'>" +
        "<div class='large'><input type='text' id='inputNutrition"+NutritionCount+"'  name='nutrition[name][]' placeholder='Nutrition' /></div>" +
        "<div class='small'><input type='number' id='inputServingSize"+NutritionCount+"' name='nutrition[size][]' placeholder='Serving Size' /></div>" +
        "<div class='third'><select name='nutrition[category][]' class='f-row ingredient' id='inputNutritionCategory"+NutritionCount+"'>" +
        "<option selected disabled>Select a category</option>" +
        "<option value='calories'>Calories</option>" +
        "<option value='protien'>Protien</option>" +
        "<option value='carbs'>Carbs</option>" +
        "<option value='fat'>Fat</option>" +
        "<option value='saturates'>Saturates</option>" +
        "<option value='fibre'>Fibre</option>" +
        "<option value='sugar'>Sugar</option>" +
        "<option value='salt'>Salt</option>" +
        "</select>" +
        "</div>" +
        "<button class='remove' type='button' id='removeNutrition'>-</button>" +
        "</div>";
    $("#mainDivNutrition").append(data);
    $("#mainDivNutrition").on('click', '#removeNutrition', function () {
        $(this).parent().remove();
    });
}
function addStep() {
    StepCount = StepCount + 1;
    var data="<div class='f-row instruction'>" +
        "<div class='full'><input type='text' name='instructions[]' placeholder='Instructions' /></div>" +
        "<button type='button' class='remove' id='removeStep'>-</button>" +
        "</div>";
    $("#mainDivStep").append(data);
    $("#mainDivStep").on('click', '#removeStep', function () {
        $(this).parent().remove();
    });
}

function adddietNutrition() {
    DietNutritionCount = DietNutritionCount + 1;
    var data="<div class='f-row ingredient'>" +
        "<div class='third'><select name='nutrition[nutrition][]' class='f-row ingredient' id='inputNutritionCategory"+DietNutritionCount+"'>" +
        "<option selected disabled>Select a category</option>" +
        "<option value='calories'>Calories</option>" +
        "<option value='protien'>Protien</option>" +
        "<option value='carbs'>Carbs</option>" +
        "<option value='fat'>Fat</option>" +
        "<option value='saturates'>Saturates</option>" +
        "<option value='fibre'>Fibre</option>" +
        "<option value='sugar'>Sugar</option>" +
        "<option value='salt'>Salt</option>" +
        "</select>" +
        "</div>" +
        "<div class='third'><input type='text' id='inputNutrition"+DietNutritionCount+"'  name='nutrition[value][]' placeholder='Nutrition' /></div>" +
        
        
        "<button class='remove' type='button' id='removeNutrition'>-</button>" +
        "</div>";
    $("#mainDivNutrition").append(data);
    $("#mainDivNutrition").on('click', '#removeNutrition', function () {
        $(this).parent().remove();
    });
}