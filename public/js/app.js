
jQuery( document ).ready(function( $ ){

	$("#mainDivStep").on('click', '#removeStep', function () {
        $(this).parent().remove();
    });

    $("#mainDivNutrition").on('click', '#removeNutrition', function () {
        $(this).parent().remove();
    });

    $("#mainDivIngredients").on('click', '#removeRow', function () {
        $(this).parent().remove();
    });
    
	$(".upload_pic_btn").hide();
	$("#inline-update-name").hide();
	$("#inline-update-pwd").hide();
	$("#old-pwd-div").hide();
	$("#message-div").hide();

	$("#upload_image_click").on('click', function(e){
        e.preventDefault();
        $("#upload_profile_pic:hidden").trigger('click');
    });

	$("#upload_profile_pic").change(function(){
	    yieldProfilePic(this);
	    $(".upload_pic_btn").show();
	    $("#upload_image_click").html("change");
	});

	$("#inline-edit-name").on('click',function(){
		$(".inline-edit-name").attr('contenteditable',true);
		$(".inline-edit-name").focus();
		$(".inline-edit-name").css('outline','none');
		$("#inline-edit-name").hide();
		$("#inline-update-name").show();
	});

	$("#inline-edit-pwd").on('click',function(e){
	$("#new-pwd").html("");		
		$("#old-pwd-div").show();
		$("#inline-update-pwd").show();
		$("#inline-edit-pwd").hide();
		$("#new-pass-content").html("New Password");
		$("#old-pwd").attr('contenteditable',true);
		$("#new-pwd").attr('contenteditable',true);
		$("#old-pwd").css('outline','none');
		$("#new-pwd").css('outline','none');
		$("#old-pwd").focus();

	});

	//update access key
	$("#inline-update-pwd").on('click',function(e){
		e.preventDefault();
		var old = $.trim($("#old-pwd").html());
		var newPwd = $.trim($("#new-pwd").html());
		var token = $("input[name='_token']").val();		
		$.post("myAccount/updatePofile",{'what':'pwd','_token':token,'old' : old,'new':newPwd},function(response,status,xhr){
			if(status === "success"){
				console.log(response);
				if(response.success === true) {
					$("#inline-edit-pwd").show();
					$("#old-pwd-div").hide();
					$("#new-pass-content").html("Password");
					$("#inline-update-pwd").hide();	
					$("#msg-type").html("Success:");	
					$("#message-div").show();				
					$("#display-msg-here").html(response.message);
					$("#new-pwd").html("*******");				
					$("#old-pwd").html("");				
				} else if(response.success === false) {
					$("#message-div").show();
					$("#msg-type").html("Error:");				
					$("#display-msg-here").html(response.message);
				}
			}
		},'json')
		.error(function(error){console.log(error)});
		
		});

	$("#inline-update-name").on('click',function(e){
		e.preventDefault();
		var content = $(".inline-edit-name").html();
		var token = $("input[name='_token']").val();		
		$.post("myAccount/updatePofile",{'what':'name','_token':token,'name' : content},function(response,status,xhr){
			if(status === "success"){
				if(response.success === true) {
					$("#inline-edit-name").show();
					$("#inline-update-name").hide();
					$("#my-full-name").html(content);
				}
			}
		},'json')
		.error(function(error){console.log(error)});

	});

	function yieldProfilePic(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#yieldProfilePic').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}
});