<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDietPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diet_plans', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('dietitian_id');
			$table->string('name',70);
            $table->string('slug',70);
			$table->string('description');
			$table->string('image');
			$table->string('sun_breakfast');
			$table->string('sun_lunch');
			$table->string('sun_dinner');
			$table->string('mon_breakfast');
			$table->string('mon_lunch');
			$table->string('mon_dinner');
			$table->string('tue_breakfast');
			$table->string('tue_lunch');
			$table->string('tue_dinner');
			$table->string('wed_breakfast');
			$table->string('wed_lunch');
			$table->string('wed_dinner');
			$table->string('thur_breakfast');
			$table->string('thur_lunch');
			$table->string('thur_dinner');
			$table->string('fri_breakfast');
			$table->string('fri_lunch');
			$table->string('fri_dinner');
			$table->string('sat_breakfast');
			$table->string('sat_lunch');
			$table->string('sat_dinner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('diet_plans');
    }
}
