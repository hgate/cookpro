<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDietPlanNutritionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diet_plan_nutritions', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('diet_plan_id')->unsigned()->default(0);
            $table->foreign('diet_plan_id')->references('id')->on('diet_plans')->onDelete('cascade');
			$table->string('nutrition',70);
			$table->string('value',70);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('diet_plan_nutritions');
    }
}
