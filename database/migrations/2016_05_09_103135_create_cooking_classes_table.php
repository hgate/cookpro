<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCookingClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cooking_classes', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('teacher_id');
			$table->string('name',70);
            $table->string('slug',70);
			$table->string('description');
			$table->string('image');
			$table->string('time_period');
			$table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cooking_classes');
    }
}
