<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    
    public function run()
    {        
        DB::table('users')->delete();

        DB::table('dietitians')->delete();
        
        DB::table('teachers')->delete();

        $users = [
        ['name' => 'Jagroop [User]','email' => 'jagroop@user.com', 'username' => 'jagroop.singh', 'password' => bcrypt('qwerty'), 'verified' => 1]
        ];

        $teachers = [
        ['name' => 'Jagroop [Teacher]','email' => 'jagroop@teacher.com', 'username' => 'jagroop.singh', 'password' => bcrypt('qwerty'), 'verified' => 1]
        ];

        $dietitians = [
        ['name' => 'Jagroop [Dietitian]','email' => 'jagroop@dietitian.com', 'username' => 'jagroop.singh', 'password' => bcrypt('qwerty'), 'verified' => 1]
        ];

        DB::table('users')->insert($users);

        DB::table('teachers')->insert($teachers);

        DB::table('dietitians')->insert($dietitians);
    }
}
