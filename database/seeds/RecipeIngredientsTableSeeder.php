<?php

use Illuminate\Database\Seeder;

class RecipeIngredientsTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('recipe_ingredients')->delete();

        $recipeIngredients = array(
            ['id' => 1, 'recipe_id' => 1, 'name' => 'Saffron', 'quantity' => 2, 'unit' => 'g', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'recipe_id' => 1, 'name' => 'Ginger', 'quantity' => 5, 'unit' => 'g', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 3, 'recipe_id' => 1, 'name' => 'Cinnamon', 'quantity' => 3, 'unit' => 'g', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        //// Uncomment the below to run the seeder
        DB::table('recipe_ingredients')->insert($recipeIngredients);
    }

}