<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call('UsersSeeder');
		$this->call('CategoriesTableSeeder');
		$this->call('IngredientsTableSeeder');
        $this->call('RecipesTableSeeder');
		$this->call('RecipeIngredientsTableSeeder');
		$this->call('RecipeNutritionsTableSeeder');
		$this->call('RecipeInstructionsTableSeeder');	
		
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
