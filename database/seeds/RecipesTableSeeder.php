<?php

use Illuminate\Database\Seeder;

class RecipesTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('recipes')->delete();

        $recipes = array(
            ['id' => 1, 'title' => 'fried rice', 'slug' => 'fried-rice', 'user_id' => 1, 'preparation_time' => 10, 'cooking_time' => 25, 'difficulty' => 'medium', 'serve_no_of_people' => 5, 'category_id' => 7, 'description' => 'description here', 'image' => '', 'video_url' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'title' => 'Hot dog', 'slug' => 'hot-dog', 'user_id' => 1, 'preparation_time' => 15, 'cooking_time' => 10, 'difficulty' => 'simple', 'serve_no_of_people' => 1, 'category_id' => 4, 'description' => 'description here', 'image' => '', 'video_url' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 3, 'title' => 'Pizza', 'slug' => 'pizza', 'user_id' => 1,'preparation_time' => 5, 'cooking_time' => 20, 'difficulty' => 'medium', 'serve_no_of_people' => 5, 'category_id' => 8, 'description' => 'description here', 'image' => '', 'video_url' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 4, 'title' => 'Burger', 'slug' => 'burger', 'user_id' => 1, 'preparation_time' => 5, 'cooking_time' => 10, 'difficulty' => 'simple', 'serve_no_of_people' => 2, 'category_id' => 4, 'description' => 'description here', 'image' => '', 'video_url' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 5, 'title' => 'Sandwich', 'slug' => 'sandwich', 'user_id' => 1, 'preparation_time' => 5, 'cooking_time' => 5, 'difficulty' => 'simple', 'serve_no_of_people' => 3, 'category_id' => 4, 'description' => 'description here', 'image' => '', 'video_url' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 6, 'title' => 'Bread', 'slug' => 'bread', 'user_id' => 1, 'preparation_time' => 25, 'cooking_time' => 15, 'difficulty' => 'hard', 'serve_no_of_people' => 10, 'category_id' => 4, 'description' => 'description here', 'image' => '', 'video_url' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 7, 'title' => 'Cake', 'slug' => 'cake', 'user_id' => 1, 'preparation_time' => 15, 'cooking_time' => 10, 'difficulty' => 'medium', 'serve_no_of_people' => 10, 'category_id' => 8, 'description' => 'description here', 'image' => '', 'video_url' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],

        );

        //// Uncomment the below to run the seeder
        DB::table('recipes')->insert($recipes);
    }

}