<?php

use Illuminate\Database\Seeder;

class RecipeNutritionsTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('recipe_nutritions')->delete();

        $recipeNutritions = array(
            ['id' => 1, 'recipe_id' => 1, 'name' => 'Calories', 'quantity' => 50, 'category' => 'g', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'recipe_id' => 1, 'name' => 'Protien', 'quantity' => 59, 'category' => 'g', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 3, 'recipe_id' => 1, 'name' => 'Carbs', 'quantity' => 17, 'category' => 'g', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        //// Uncomment the below to run the seeder
        DB::table('recipe_nutritions')->insert($recipeNutritions);
    }

}