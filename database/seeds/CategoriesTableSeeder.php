<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('categories')->delete();

        $categories = array(
            ['id' => 1, 'name' => 'Fruits', 'slug' => 'fruits', 'image' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'name' => 'Cereals', 'slug' => 'cereals', 'image' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 3, 'name' => 'Vegetables', 'slug' => 'vegetables', 'image' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 4, 'name' => 'Breakfast', 'slug' => 'breakfast', 'image' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 5, 'name' => 'Dairy', 'slug' => 'dairy', 'image' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 6, 'name' => 'Grocery', 'slug' => 'grocery', 'image' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 7, 'name' => 'Lunch', 'slug' => 'lunch', 'image' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 8, 'name' => 'Sweets', 'slug' => 'sweets', 'image' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 9, 'name' => 'Snacks', 'slug' => 'snacks', 'image' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 10, 'name' => 'Bakery', 'slug' => 'bakery', 'image' => '', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        //// Uncomment the below to run the seeder
        DB::table('categories')->insert($categories);
    }

}