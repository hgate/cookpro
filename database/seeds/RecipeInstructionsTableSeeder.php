<?php

use Illuminate\Database\Seeder;

class RecipeInstructionsTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('recipe_instructions')->delete();

        $recipeInstructions = array(
		['id' => 1, 'recipe_id' => 1, 'instructions' => 'step1', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
		['id' => 2, 'recipe_id' => 1, 'instructions' => 'step2', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
		['id' => 3, 'recipe_id' => 1, 'instructions' => 'step3', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        //// Uncomment the below to run the seeder
        DB::table('recipe_instructions')->insert($recipeInstructions);
    }

}