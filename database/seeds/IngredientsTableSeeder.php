<?php

use Illuminate\Database\Seeder;

class IngredientsTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('ingredients')->delete();

        $ingredients = array(
            ['id' => 1, 'name' => 'Saffron', 'slug' => 'saffron', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'name' => 'Coriander', 'slug' => 'coriander', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 3, 'name' => 'Garlic', 'slug' => 'garlic', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 4, 'name' => 'Curry leaves', 'slug' => 'curry-leaves', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 5, 'name' => 'Ginger', 'slug' => 'ginger', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 6, 'name' => 'Turmeric', 'slug' => 'turmeric', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 7, 'name' => 'Onion', 'slug' => 'onion', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 8, 'name' => 'Cinnamon', 'slug' => 'cinnamon', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 9, 'name' => 'Black Pepper', 'slug' => 'black-pepper', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 10, 'name' => 'Cardamom', 'slug' => 'cardamom', 'status' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        //// Uncomment the below to run the seeder
        DB::table('ingredients')->insert($ingredients);
    }

}