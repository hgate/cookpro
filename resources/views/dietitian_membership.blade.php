@extends('layouts.master')

@section('title', 'Cookpro | Search For Recipe')

@section('content') 
	<div class="full-images-bg dietain">
        <div class="wrap clearfix">
            <div class="banner-txt text-center">
            	<h3>We Have Best Dietitians Here</h3>
            </div>
            <div class="container recipefinder"> 
                <div class="row">
                    <div class="one-third">
                        <select>
                            <option selected="selected">Select a Diet Plans</option>
                            <option>Anemia Diet Plan</option>
                            <option>Bone & Joint Disease Diet Plan</option>
                            <option>Cancer Diet Plan</option>
                            <option>Cholesterol Diet Plan</option>
                            <option>Diabetes Diet Plan</option>
                            <option>Free Diet Plans</option>
                            <option>Heart Healthy Diet Plan</option>
                            <option>Infertility Diet Plan</option>
                            <option>Kidney Disease Diet Plan</option>
                            <option>Kids Diet Plan</option>
                            <option>Lifestyle Disease Diet Plan</option>
                            <option>Liver & GI Disease Diet Plan</option>
                            <option>PCOD/ PCOS Diet Plan</option>
                            <option>Personalized Diet Plan</option>
                            <option>Pregnancy & Lactation Diet Plan</option>
                            <option>Skin Care Diet Plan</option>
                            <option>Thyroid Diet Plans</option>
                            <option>Weight Gain Diet Plan</option>
                            <option>Weight Loss Diet Plan</option>            
                        </select>
                    </div>
                    <div class="one-third">
                        <select>
                            <option selected="selected">Type of Consultation</option>
                            <option>Indian diet plan</option>
                            <option>Email Consultation</option>
                            <option>Free Diet Plan</option>
                            <option>In Clinic Consultation</option>
                            <option>Onsite Consultation</option>
                            <option>Paid Diet Plan</option>
                            <option>Personalised: Daily follow up</option>
                            <option>Skype Call</option>
                            <option>Standard: Weekly followup</option>
                            <option>Telephonic Consultation</option>
                            <option>Whats app consultation</option>          
                        </select>
                    </div>
                    <div class="one-third">
                        <select>
                            <option selected="selected">Duration</option>
                            <option>14 days</option>
                            <option>20 mins</option>
                            <option>30 mins</option>
                            <option>7 Days</option>
                            <option>One month</option>
                            <option>One Time</option>
                            <option>Three months</option>        
                        </select>
                    </div>
                     <div class="one-third submit">
                        <input type="submit" value="Submit">
                    </div>
                </div>
            </div>
		</div>
	</div>	
	<!--main-->
	<main class="main" role="main">
		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
					<li>Our Packages</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->
			
			<div class="row">
				<header class="s-title wow fadeInLeft">
					<h1>Our Packages</h1>
				</header>
				
				<!--content-->
				<section class="content full-width wow fadeInUp">
					<div class="row">
						<div class="full-width">
							<div class="pricing three-col secondary">
								<div>
									<table>
										<tr>
											<th>SILVER</th>
										</tr>
										<tr>
											<td><sup>$</sup>50<span> 3 Month Programme</span></td>
										</tr>
										<tr>
											<td><strong>Individually created 7 day meal plan</strong></td>
										</tr>
										<tr>
											<td><strong>Extensive supporting document and recipes guide</strong></td>
										</tr>
										<tr>
											<td><strong>Regular help from your own nutritionist</strong></td>
										</tr>
										<tr>
											<td><strong>Individual supplement recommendations</strong></td>
										</tr>
										<tr>
											<td><strong>3 months unlimited email support</strong></td>
										</tr>
                                        <tr>
                                        	<td><strong>Access to our private members Facebook community</strong></td>
                                        </tr>
										<tr>
											<td><a href="#" class="button">Buy Now</a></td>
										</tr>
									</table>
								</div>
								<div class="active">
									<table>
										<tr>
											<th>GOLD - MOST POPULAR</th>
										</tr>
										<tr>
											<td><sup>$</sup>100<span> 3 Month Programme</span></td>
										</tr>
										<tr>
											<td><strong>Individually created 7 day meal plan</strong></td>
										</tr>
										<tr>
											<td><strong>Extensive supporting document and recipes guide</strong></td>
										</tr>
										<tr>
											<td><strong>Regular help from your own nutritionist</strong></td>
										</tr>
										<tr>
											<td><strong>Individual supplement recommendations</strong></td>
										</tr>
										<tr>
											<td><strong>3 months unlimited email support</strong></td>
										</tr>
                                        <tr>
                                        	<td><strong>Access to our private members Facebook community</strong></td>
                                        </tr>
                                        <tr>
                                        	<td><strong>1 follow-up face to face consultation</strong></td>
                                        </tr>
                                        <tr>
                                        	<td><strong>2 advanced body fat assessments</strong></td>
                                        </tr>
                                        <tr>
                                        	<td><strong>* split payments available</strong></td>
                                        </tr>
										<tr>
											<td><a href="#" class="button">Buy Now</a></td>
										</tr>
									</table>
								</div>
								<div>
									<table>
										<tr>
											<th>PLATINUM</th>
										</tr>
										<tr>
											<td><sup>$</sup>200<span>6 Month Programme</span></td>
										</tr>
										<tr>
											<td><strong>Individually created 7 day meal plan</strong></td>
										</tr>
										<tr>
											<td><strong>Extensive supporting document and recipes guide</strong></td>
										</tr>
										<tr>
											<td><strong>Regular help from your own nutritionist</strong></td>
										</tr>
										<tr>
											<td><strong>Individual supplement recommendations</strong></td>
										</tr>
										<tr>
											<td><strong>12 weeks unlimited email support</strong></td>
										</tr>
                                        <tr>
                                        	<td><strong>Access to our private members Facebook community</strong></td>
                                        </tr>
                                        <tr>
                                        	<td><strong>4 follow-up face to face consultations</strong></td>
                                        </tr>
                                        <tr>
                                        	<td><strong>4 advanced body fat assessments</strong></td>
                                        </tr>
                                        <tr>
                                        	<td><strong>* split payments available</strong></td>
                                        </tr>
										<tr>
											<td><a href="#" class="button">Buy Now</a></td>
										</tr>
									</table>
								</div>
							</div>
							<!--//pricing-->
						</div>
					</div>
					<!--//row-->
				</section>
				<!--//content-->
			</div>
			<!--//row-->
		</div>
		<!--//wrap-->
	</main>
	<!--//main-->
	
@endsection