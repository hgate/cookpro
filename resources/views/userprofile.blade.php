@extends('layouts.master')

@section('title', 'Cookpro | User Profile')

@section('content')

<!--wrap-->
<div class="wrap clearfix">
    <!--breadcrumbs-->
    <nav class="breadcrumbs">
        <ul>
            <li><a href="index-2.html" title="Home">Home</a></li>
            <li>My account</li>
        </ul>
    </nav>
    <!--//breadcrumbs-->

    <!--content-->
    <section class="content">
        <!--row-->
        <div class="row">
            <!--profile left part-->
            <div class="my_account one-fourth wow fadeInLeft">
                <figure>
                    <img src="images/avatar4.jpg" alt="" />
                </figure>
                <div class="container">
                    <h2>{{ $user->name }}</h2> 
                </div>
            </div>
            <!--//profile left part-->
            
            <div class="three-fourth wow fadeInRight">
                <nav class="tabs">
                    <ul>
                        <li class="active"><a href="#about" title="About me">About me</a></li>
                        <li><a href="#recipes" title="My recipes">My recipes</a></li>
                        <li><a href="#favorites" title="My favorites">My favorites</a></li>
                        <li><a href="#posts" title="My posts">My posts</a></li>
                    </ul>
                </nav>
                
                <!--about-->
                <div class="tab-content" id="about">
                    <div class="row">
                        <dl class="basic two-third">
                            <dt>Name</dt>
                            <dd>{{ $user->name }}</dd>
                            <dt>Favorite cusine</dt>
                            <dd></dd>
                            <dt>Favorite appliances</dt>
                            <dd></dd>
                            <dt>Favorite spices</dt>
                            <dd></dd>
                            <dt>Recipes submitted</dt>
                            <dd></dd>
                            <dt>Posts submitted</dt>
                            <dd></dd>
                        </dl>
                        
                        <div class="one-third">
                            <ul class="boxed gold">
                                <li class="light"><a href="#" title="Best recipe"><i class="ico i-had_best_recipe"></i> <span>Had a best recipe</span></a></li>
                                <li class="medium"><a href="#" title="Was featured"><i class="ico i-was_featured"></i> <span>Was featured</span></a></li>
                                <li class="dark"><a href="#" title="Added a first recipe"><i class="ico i-added_first_recipe"></i> <span>Added a first recipe</span></a></li>
                                
                                <li class="medium"><a href="#" title="Added 10-20 recipes"><i class="ico i-added_several_recipes"></i> <span>Added 10-20 recipes</span></a></li>
                                <li class="dark"><a href="recipes.php" title="Events"><i class="ico i-wrote_blog_post"></i> <span>Wrote a blog post</span></a></li>
                                <li class="light"><a href="recipes.php" title="Fish"><i class="ico i-wrote_comment"></i> <span>Wrote a comment</span></a></li>
                                
                                <li class="dark"><a href="recipes.php" title="Fish"><i class="ico i-won_contest"></i> <span>Won a contest</span></a></li>
                                <li class="light"><a href="recipes.php" title="Healthy"><i class="ico i-shared_recipe"></i> <span>Shared a recipe</span></a></li>
                                <li class="medium"><a href="#" title="Was featured"><i class="ico i-was_featured"></i> <span>Was featured</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--//about-->
            
                <!--my recipes-->
                <div class="tab-content" id="recipes">
                    <div class="entries row">
                        <!--item-->
                        @forelse($recipes as $key => $recipe)
                            <li></li>
                        
                        <div class="entry one-third">
                            <figure>
                                @if($recipe->video_url != "")
                                    <!--<iframe width="580" height="405"
                                        src="{{ $recipe->video_url.'?autoplay=1' }}">
                                    </iframe>-->
                                @elseif ($recipe->image != "" || !file_exists('uploads/recipes/'.$recipe->image))
                                    <a href="#"><img src="{{ asset('uploads/recipes/'.$recipe->image) }}" alt="" /></a>
                                @else
                                    <img src="{{ asset('uploads/recipes/img6.jpg') }}" alt="" />   
                                @endif
                                <figcaption><a href="{{ url('/recipe/'.$recipe->slug) }}"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                            </figure>
                            <div class="container">
                                <h2><a href="{{ url('/recipe/'.$recipe->slug) }}">{{ $recipe->title }}</a></h2> 
                                <div class="actions">
                                    <div>
                                        <div class="difficulty"><i class="ico i-medium"></i><a href="#">{{ $recipe->difficulty }}</a></div>
                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                        <div class="comments"><i class="ico i-comments"></i><a href="recipe.html#comments">27</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                            <p>No Recipes found</p>
                        @endforelse 
                        <!--item-->
                    </div>
                </div>
                <!--//my recipes-->
                
                
                <!--my favorites-->
                <div class="tab-content" id="favorites">
                    <div class="entries row">
                        <!--item-->
                        <div class="entry one-third">
                            <figure>
                                <img src="images/img6.jpg" alt="" />
                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                            </figure>
                            <div class="container">
                                <h2><a href="recipe.php">Thai fried rice with fruit and vegetables</a></h2> 
                                <div class="actions">
                                    <div>
                                        <div class="difficulty"><i class="ico i-medium"></i><a href="#">medium</a></div>
                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                        <div class="comments"><i class="ico i-comments"></i><a href="recipe.html#comments">27</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--item-->
                        
                        <!--item-->
                        <div class="entry one-third">
                            <figure>
                                <img src="images/img5.jpg" alt="" />
                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                            </figure>
                            <div class="container">
                                <h2><a href="recipe.php">Spicy Morroccan prawns with cherry tomatoes</a></h2> 
                                <div class="actions">
                                    <div>
                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                        <div class="comments"><i class="ico i-comments"></i><a href="recipe.html#comments">27</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--item-->
                        
                        <!--item-->
                        <div class="entry one-third">
                            <figure>
                                <img src="images/img8.jpg" alt="" />
                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                            </figure>
                            <div class="container">
                                <h2><a href="recipe.php">Super easy blueberry cheesecake</a></h2> 
                                <div class="actions">
                                    <div>
                                        <div class="difficulty"><i class="ico i-easy"></i><a href="#">easy</a></div>
                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                        <div class="comments"><i class="ico i-comments"></i><a href="recipe.html#comments">27</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--item-->
                    </div>
                </div>
                <!--//my favorites-->
                
                <!--my posts-->
                <div class="tab-content" id="posts">
                    <!--entries-->
                    <div class="entries row">
                        <!--item-->
                        <div class="entry one-third">
                            <figure>
                                <img src="images/img12.jpg" alt="" />
                                <figcaption><a href="blog_single.php"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
                            </figure>
                            <div class="container">
                                <h2><a href="blog_single.php">Barbeque party</a></h2> 
                                <div class="actions">
                                    <div>
                                        <div class="date"><i class="ico i-date"></i><a href="#">22 Dec 2014</a></div>
                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                    </div>
                                </div>
                                <div class="excerpt">
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet . . . </p>
                                </div>
                            </div>
                        </div>
                        <!--item-->
                        
                        <!--item-->
                        <div class="entry one-third">
                            <figure>
                                <img src="images/img11.jpg" alt="" />
                                <figcaption><a href="blog_single.php"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
                            </figure>
                            <div class="container">
                                <h2><a href="blog_single.php">How to make sushi</a></h2> 
                                <div class="actions">
                                    <div>
                                        <div class="date"><i class="ico i-date"></i><a href="#">22 Dec 2014</a></div>
                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                    </div>
                                </div>
                                <div class="excerpt">
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet . . . </p>
                                </div>
                            </div>
                        </div>
                        <!--item-->
                        
                        <!--item-->
                        <div class="entry one-third">
                            <figure>
                                <img src="images/img10.jpg" alt="" />
                                <figcaption><a href="blog_single.php"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
                            </figure>
                            <div class="container">
                                <h2><a href="blog_single.php">Make your own bread</a></h2> 
                                <div class="actions">
                                    <div>
                                        <div class="date"><i class="ico i-date"></i><a href="#">22 Dec 2014</a></div>
                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                    </div>
                                </div>
                                <div class="excerpt">
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet . . . </p>
                                </div>
                            </div>
                        </div>
                        <!--item-->
                    </div>
                    <!--//entries-->
                </div>
                <!--//my posts-->
            </div>
        </div>
        <!--//row-->
    </section>
    <!--//content-->
</div>
<!--//wrap-->

@endsection