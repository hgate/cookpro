@extends('layouts.master')

@section('title', 'Cookpro | Recipes')

@section('content')

    <!--wrap-->
    <div class="wrap clearfix">
        <!--breadcrumbs-->
        <nav class="breadcrumbs">
            <ul>
                <li><a href="index-2.html" title="Home">Home</a></li>
                <li>Search for Recipes</li>
            </ul>
        </nav>
        <!--//breadcrumbs-->
        
        <!--row-->
        <div class="row">
            <!--<header class="s-title wow fadeInLeft">
                <h1>Search for Recipes</h1>
            </header>-->
            
            <!--content-->
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <section class="content full-width wow fadeInUp">
        
                <!--results-->
                <div class="entries row">
                @forelse($recipes as $key => $recipe)
                    <!--item-->
                    <div class="entry one-fourth wow fadeInLeft" data-wow-delay=".{{ $key+2 }}s">
                        <figure>
                        @if($recipe->image === "" || !file_exists('uploads/recipes/'.$recipe->image))
                              <img src="images/img6.jpg" alt="{{ $recipe->title }}" />
                            @else
                              <img src="uploads/recipes/{{ $recipe->image }}" alt="{{ $recipe->title }}" />
                        @endif
                            <figcaption><a href="{{ url('/recipe/'.$recipe->slug) }}"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                        </figure>
                        <div class="container">
                            <h2><a href="{{ url('/recipe/'.$recipe->slug) }}">{{ $recipe->title }}</a></h2> 
                            <div class="actions">
                                <div>
                                    <div class="difficulty"><i class="ico i-{{ $recipe->difficulty }}"></i><a href="#">
                                    {{ $recipe->difficulty }}
                                    </a></div>
                                    <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                    <div class="comments"><i class="ico i-comments"></i><a href="recipe.html#comments">27</a></div>
                                </div>
                            </div>
                            <div class="excerpt img-circle">
                                <a class="MasterChef" href="{{ url('/userprofile/'.$recipe->userid) }}">
                                 @if($recipe->avatar != "" && file_exists('uploads/users/'.$recipe->avatar))
                                   <img src="{{ asset('uploads/users/'.$recipe->avatar) }}" class="lazy" alt="{{ $recipe->username or 'Unknown'}}">
                                  @else
                                    <img src="{{ asset('images/users/chef11.jpg') }}" class="lazy" alt="{{ $recipe->username or 'Unknown'}}">
                                 @endif
                                </a>
                                <div class="dish-chef-detail">
                                  <span class="chef-name"><a href="{{ url('/userprofile/'.$recipe->userid) }}" class="chefNameLink">{{ $recipe->username or 'Unknown'}}</a></span>
                                  <br />
                                    <span class="chef-tag">MasterChef</span>
                                </div>

                                <button type="button" class="btn btn-primary btn-lg addmeal" data-toggle="modal" data-target="#myModal" data-value="{{$recipe->id}}">Add to meal plan</button>

                            </div>
                        </div>
                    </div>

                     @empty
                        <p>No recipes found</p>
                    <!--item-->
                    @endforelse
                    <div class="quicklinks">
                        <a href="#" class="button">More recipes</a>
                        <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
                    </div>
                </div>
                <!--//results-->
            </section>
            <!--//content-->
        </div>
        <!--//row-->
    </div>
        <!--//wrap-->
        
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add to Meal Plan</h4>
      </div>
      <div class="modal-body">
        @if(Auth::check())
            {!! Form::open(array('url' => '/addmealplan', 'method'=>'post')) !!}
                <label>Select Mealplan </label>
                
                {!! Form::select('meal_plan_id', $mymealplan) !!}
                
                <label>Select Weekdays </label>
                <select name="meal_day">
                     <option value="" selected="selected">Select Day</option>
                     <option value="Monday" selected="selected">Monday</option>
                     <option value="Tuesday" selected="selected">Tuesday</option>
                     <option value="Wednesday" selected="selected">Wednesday</option>
                     <option value="Thursday" selected="selected">Thursday</option>
                     <option value="Friday" selected="selected">Friday</option>
                     <option value="Saturday" selected="selected">Saturday</option>
                     <option value="Sunday" selected="selected">Sunday</option>                                
                </select>
                {!! Form::hidden('recipe_id', '', array('id' => 'recipe_id')) !!}
               <button type="submit" class="btn btn-primary">Add</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>           
            {!! Form::close() !!}
            @else
            
            Please login to continue !
            
        @endif
      </div>     
    </div>
  </div>
</div>

@endsection
