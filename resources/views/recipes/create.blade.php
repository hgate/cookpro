@extends('layouts.master')

@section('title', 'Cookpro | Submit Your Recipe Here')

@section('content')

<!--wrap-->
<div class="wrap clearfix">
    <!--breadcrumbs-->
    <nav class="breadcrumbs">
        <ul>
            <li><a href="{{ url('/') }}" title="Home">Home</a></li>
            <li>Submit a recipe</li>
        </ul>
    </nav>
    <!--//breadcrumbs-->
    
    <!--row-->
    <div class="row">
        <header class="s-title">
            <h1>Add a new recipe</h1>
        </header>
            
        <!--content-->
        <section class="content full-width wow fadeInUp">
            <div class="submit_recipe container">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif              
                

                <form method="POST" action="{{ url('recipes') }}" enctype="multipart/form-data">
                 {!! csrf_field() !!}
                    <section>
                        <h2>Basics</h2>
                        <p>All fields are required.</p>
                        <div class="f-row">
                            <div class="full"><input type="text" name="title" value="{{ old('title') }}" placeholder="Recipe title" /></div>
                        </div>
                        <div class="f-row">
                            <div class="third"><input type="number" name="preparation_time" value="{{ old('preparation_time') }}" placeholder="Preparation time" /></div>
                            <div class="third"><input type="number" name="cooking_time" value="{{ old('cooking_time') }}" placeholder="Cooking time" /></div>
                            <div class="third">                            
                            <select name="difficulty">
                                <option value=""> Select Difficulty </option>
                                <option value="easy">Easy</option>
                                <option value="medium">Medium</option>
                                <option value="hard">Hard</option>
                            </select>
                            </div>
                        </div>
                        <div class="f-row">
                            <div class="third"><input type="number" name="serve_no_of_people" value="{{ old('serve_no_of_people') }}" placeholder="Serves how many people?" /></div>
                            <div class="third">
                            <select name="category">                            	
                                 @if(count($categories) > 0)
                                        <option value="" selected="selected">Select a category</option> 
                                    @foreach($categories as $key => $category) 
                                        <option value="{{ $category->id }}" selected="selected">{{ $category->name }}</option>
                                    @endforeach 
                                @else
                                    <option value="" selected="selected">No category Avialable</option>                                   
                                @endif 
                            </select></div>
                        </div>
                    </section>


                    <section>
                        <h2>Ingredients</h2>
                        <div id="mainDivIngredients" >                           

                            @if(session('ingredients_counter'))

                              @foreach(range(0, session('ingredients_counter')-1) as $indexIngredient)
                                    <div class="f-row ingredient">
                                        <div class="large"><input type="text" name="ingredients[name][]" value="{{ old('ingredients.name.'.$indexIngredient) }}" id="inputIngredients1" placeholder="Ingredient" /></div>
                                        <div class="small"><input type="text" name="ingredients[qty][]" id="inputQuantity1" value="{{ old('ingredients.qty.'.$indexIngredient) }}" placeholder="Quantity" /></div>
                                        <div class="third">
                                            {!! Form::select('ingredients[unit][]', array(
                                                '' => 'Unit',
                                                'g' => 'g',
                                                'tbsp' => 'tbsp',
                                                'ml' => 'ml',
                                                'sprinkles' => 'Sprinkles'
                                            ), old('ingredients.unit.'.$indexIngredient)) !!}
                                        </div>
                                        <button class="remove" id="removeNutrition">-</button>
                                    </div>
                              @endforeach

                            @else
                               
                                <div class="f-row ingredient">
                                    <div class="large"><input type="text" name="ingredients[name][]" id="inputIngredients1" placeholder="Ingredient" /></div>
                                    <div class="small"><input type="text" name="ingredients[qty][]" id="inputQuantity1" placeholder="Quantity" /></div>
                                    <div class="third">
                                        <select id="inputUnit" name="ingredients[unit][]">
                                        <option disabled selected>Unit</option>
                                            <option value="g">g</option>
                                            <option value="tbsp">tbsp</option>
                                            <option value="ml">ml</option>
                                            <option value="sprinkles">Sprinkles</option>
                                        </select>
                                    </div>
                                    <button class="remove" id="removeRow">-</button>
                                </div>

                        @endif

                        </div>
                        <div class="f-row full">
                            <button type="button" class="add" onclick="addIndrediants();">Add an ingredient</button>
                        </div>

                    </section>	

                    <section>
                        <h2>Nutrition facts per serving (optional)</h2>
                        <div id="mainDivNutrition">
                            @if(session('nutrition_counter'))

                                  @foreach(range(0, session('nutrition_counter')-1) as $index)                                   
                                        <div class="f-row ingredient">
                                            <div class="large"><input type="text" id="inputNutrition1" name="nutrition[name][]"  value="{{ old('nutrition.name.'.$index) }}" placeholder="Nutrition" /></div>
                                            <div class="small"><input type="number" id="inputServingSize1" name="nutrition[size][]"  value="{{ old('nutrition.size.'.$index) }}" placeholder="Serving Size" /></div>
                                            <div class="third">
                                                {!! Form::select('nutrition[category][]', array(
                                                    '' => 'Select a category',
                                                    'calories' => 'Calories',
                                                    'protien' => 'Protien',
                                                    'carbs' => 'Carbs',
                                                    'fat' => 'Fat',
                                                    'saturates' => 'Saturates',
                                                    'fibre' => 'Fibre',
                                                    'sugar' => 'Sugar',
                                                    'salt' => 'Salt'
                                                ), old('nutrition.category.'.$index)) !!}
                                            </div>
                                            <button class="remove" id="removeNutrition">-</button>
                                        </div>
                                  @endforeach

                                @else

                                    <div class="f-row ingredient">
                                        <div class="large"><input type="text" id="inputNutrition1" name="nutrition[name][]" placeholder="Nutrition" /></div>
                                        <div class="small"><input type="number" id="inputServingSize1" name="nutrition[size][]" placeholder="Serving Size" /></div>
                                        <div class="third">
                                        <select id="inputNutritionCategory1" name="nutrition[category][]">
                                            <option value="">Select a category</option>
                                            <option value="calories">Calories</option>
                                            <option value="protien">Protien</option>
                                            <option value="carbs">Carbs</option>
                                            <option value="fat">Fat</option>
                                            <option value="saturates">Saturates</option>
                                            <option value="fibre">Fibre</option>
                                            <option value="sugar">Sugar</option>
                                            <option value="salt">Salt</option>
                                        </select>
                                        </div>
                                        <button class="remove" id="removeNutrition">-</button>
                                    </div>

                            @endif
                        </div>
                        <div class="f-row full">
                            <button class="add" type="button" onclick="addNutrition();">Add A Nutrition facts</button>
                        </div>
                    </section>
                    
                    <section>
                        <h2>Description</h2>
                        <div class="f-row">
                            <div class="full"><textarea placeholder="Recipe title" name="description">{{ old('description') }}</textarea></div>
                        </div>
                    </section>
                    
                    <section>
                        <h2>Instructions <span>(enter instructions, each step at a time)</span></h2>
                        <div id="mainDivStep">
                            @if(session('instructions_counter'))

                                  @foreach(range(0, session('instructions_counter')-1) as $index)
                                    <div class="f-row instruction">
                                      <div class="full">
                                        <input type="text" name="instructions[]" placeholder="Instructions" value="{{ old('instructions.'.$index) }}" />       
                                      </div>
                                      <button class="remove" id="removeStep" type="button">-</button>
                                    </div>
                                  @endforeach

                                @else
                                    <div class="f-row instruction">
                                      <div class="full">
                                        <input type="text" name="instructions[]" placeholder="Instructions" />       
                                      </div>                                     
                                    </div>

                            @endif
                        </div>                      
                        <div class="f-row full">
                            <button class="add" type="button" onclick="addStep();">Add a step</button>
                        </div>
                    </section>
                    
                    <section>
                        <h2>Photo</h2>
                        <div class="f-row full">
                            <input name="recepiephoto" type="file" />
                        </div>
                    </section>
                    
                    <section>
                        <h2>Video URL</h2>
                        <div class="f-row full">
                            <input name="videourl" type="text" />
                        </div>
                    </section>
                    
                    <section>
                        <h2>Status <span>(would you like to further edit this recipe or are you ready to publish it?)</span></h2>
                        <div class="f-row full">
                            <input type="radio" id="r1" value="0" name="publish" checked/>
                            <label for="r1">I am still working on it</label>
                        </div>
                        <div class="f-row full">
                            <input type="radio" id="r2" value="1" name="publish"/>
                            <label for="r2">I am ready to publish this recipe</label>
                        </div>
                    </section>
                    <div class="f-row full">
                    {!! Form::hidden('image_uploaded','empty') !!}
                        <input type="submit" class="button" id="submitRecipe" value="Publish this recipe" />
                    </div>
                </form>
            </div>
        </section>
        <!--//content-->
    </div>
    <!--//row-->
</div>
<!--//wrap-->
<script src="{{asset('js/addRow.js')}}"></script>
@endsection