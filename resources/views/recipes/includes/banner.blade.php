<div class="full-images-bg">
    <div class="wrap clearfix">
        <div class="banner-txt text-center">
           <h3>Find All Recipes Here</h3>
        </div>
        <div class="container recipefinder ">
            <div class="recipes_form">
                <div class="row">
                    <div class="search one-half">
                        <input type="search" placeholder="Find recipe by name" />
                        <input type="submit" value="Search" />
                    </div>
                    
                    <div class="one-half">
                        <select>
                            <option>or select a category</option>
                            <option>Apetizers</option>
                            <option>Cocktails</option>
                            <option>Deserts</option>
                            <option>Main courses</option>
                            <option>Snacks</option>
                            <option>Soups</option>
                        </select>
                    </div>
                    
                     <div class="one-third submit">
                        <input type="submit" value="Submit">
                    </div>

                </div>
                <div class="ingredients">
                    <a href="#" class="button pink">My Fav</a>
                    <a href="#" class="button pink">Healthy Recipes</a>
                    <a href="#" class="button pink">Veg Recipes</a>
                    <a href="#" class="button pink">Non- Veg</a>
                    <a href="#" class="button pink">High Protein</a>
                    <a href="#" class="button pink">Real Simple</a>
                    <a href="#" class="button pink">Breakfast</a>
                    <a href="#" class="button pink">Lunch</a>
                    <a href="#" class="button pink">Dinner</a>
                    <a href="#" class="button pink">Snacks</a>
                    <a href="#" class="button pink">Indian</a>
                </div>
            </div>
        </div>
    </div>
</div>