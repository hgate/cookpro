@extends('layouts.master')

@section('title', 'Cookpro | Submit Your Recipe Here')

@section('content')

		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
					<li>Cooking Classes Membership Plan</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->
			
			<div class="row">
				<!--<header class="s-title wow fadeInLeft">
					<h1>Cooking Classes Membership Plan</h1>
				</header>-->
				
				<!--content-->
				<section class="content full-width wow fadeInUp">
					<div class="row">
						<div class="full-width">
							<div class="pricing three-col secondary">
								<div>
									<table>
										<tr>
											<th>BEGINNER</th>
										</tr>
										<tr>
											<td><sup>$</sup>50<span>/month</span></td>
										</tr>
										<tr>
											<td><strong>60 hours of instructional videos</strong></td>
										</tr>
										<tr>
											<td><strong>Complete coursework from basics to masters</strong></td>
										</tr>
										<tr>
											<td><strong>Schools’ in session 24/7</strong></td>
										</tr>
										<tr>
											<td><strong>One-on-one training with your favorite Top Chef</strong></td>
										</tr>
										<tr>
											<td><strong>Learn for a fraction of the cost</strong></td>
										</tr>
										<tr>
											<td><a href="#" class="button">Buy Now</a></td>
										</tr>
									</table>
								</div>
								<div class="active">
									<table>
										<tr>
											<th>INTERMEDIATE</th>
										</tr>
										<tr>
											<td><sup>$</sup>100<span>/month</span></td>
										</tr>
										<tr>
											<td><strong>60 hours of instructional videos</strong></td>
										</tr>
										<tr>
											<td><strong>Complete coursework from basics to masters</strong></td>
										</tr>
										<tr>
											<td><strong>Schools’ in session 24/7</strong></td>
										</tr>
										<tr>
											<td><strong>One-on-one training with your favorite Top Chef</strong></td>
										</tr>
										<tr>
											<td><strong>Learn for a fraction of the cost</strong></td>
										</tr>
										<tr>
											<td><a href="#" class="button">Buy Now</a></td>
										</tr>
									</table>
								</div>
								<div>
									<table>
										<tr>
											<th>ADVANCED</th>
										</tr>
										<tr>
											<td><sup>$</sup>200<span>/month</span></td>
										</tr>
										<tr>
											<td><strong>60 hours of instructional videos</strong></td>
										</tr>
										<tr>
											<td><strong>Complete coursework from basics to masters</strong></td>
										</tr>
										<tr>
											<td><strong>Schools’ in session 24/7</strong></td>
										</tr>
										<tr>
											<td><strong>One-on-one training with your favorite Top Chef</strong></td>
										</tr>
										<tr>
											<td><strong>Learn for a fraction of the cost</strong></td>
										</tr>
										<tr>
											<td><a href="#" class="button">Buy Now</a></td>
										</tr>
									</table>
								</div>
							</div>
							<!--//pricing-->
						</div>
					</div>
					<!--//row-->
				</section>
				<!--//content-->
			</div>
			<!--//row-->
		</div>
		<!--//wrap-->
	
@endsection 