<div class="full-images-bg cookingclass">
    <div class="wrap clearfix">
        <div class="banner-txt text-center">
            <h3>Cooking Classes</h3>
        </div>
        <div class="container recipefinder"> 
            <div class="row">
                <div class="one-third">
                    <select>
                        <option selected="selected">All Technique Lessons</option>
                        <option>Beginner</option>
                        <option>Intermediate</option>
                        <option>Advanced</option>      
                    </select>
                </div>
                <div class="one-third">
                    <select>
                        <option selected="selected">Main Ingredient</option>
                        <option>Eggs</option>
                        <option>Vegetables</option>
                        <option>Potatos</option>
                        <option>Pasta, Beans & Grains</option>
                        <option>Poultry</option>          
                    </select>
                </div>
                <div class="one-third">
                    <select>
                        <option selected="selected">Recipe Type</option>
                        <option>Baking & Brunch</option>
                        <option>Desserts</option>
                        <option>Everyday Essentials</option>
                        <option>Italian</option>
                        <option>Vegetarian</option>
                        <option>Outdoor Cooking</option>
                        <option>Light</option>
                        <option>Salads</option>
                        <option>Kitchen Essentials</option>
                        <option>Meat, Seafood & Poultry</option>
                        <option>Soups, Stocks & Sauces</option>
                        <option>Thanksgiving</option>          
                    </select>
                </div>
                <div class="one-third submit">
                    <input type="submit" value="Submit">
                </div>
            </div>
        </div>
    </div>
</div>