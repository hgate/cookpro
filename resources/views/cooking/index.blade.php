@extends('layouts.master')

@section('title', 'Cookpro | Submit Your Recipe Here')

@section('content')

		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
					<li>Course Classes</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->

			<div class="row">
				
				<section class="content three-fourth">
			
					<!--cwrap-->
					<div class="cwrap">
						
						<!--entries-->
						<div class="entries row">
							@forelse($cookingclasses as $key => $cookingclass)
                            
                            <!--item-->
                            <div class="entry one-third wow fadeInLeft">
                                <figure>
                                    <img src="{{ asset('images/img6.jpg') }}" alt="" />
                                    <figcaption><a href="{{ url('teacher/cookingcourse/'.$cookingclass->slug) }}"><i class="ico i-view"></i> <span>View Course</span></a></figcaption>
                                </figure>
                                <div class="container">
                                    <h2><a href="{{ url('teacher/cookingcourse/'.$cookingclass->slug) }}">{{ $cookingclass->name }}</a></h2>
                                    <div class="actions">
                                        <div>
	                                        <div class=""><a class="comment-reply-link" href="{{ url('teacher/joincourse') }}"> Join Course</a></div>
	                                        <div class="difficulty"><i class="ico i-medium"></i><a href="#">
                                    {{ $cookingclass->type }}
                                    </a></div>
                                        </div>
                                    </div>
                                    <div class="excerpt img-circle">
                                        <a class="MasterChef" href="{{ url('teacher/instructorprofile/'.$cookingclass->teacher_id) }}"><img src="images/chef11.jpg" class="lazy" alt="2 chef2"></a>
                                        <div class="dish-chef-detail">
                                          <span class="chef-name"><a href="{{ url('teacher/instructorprofile/'.$cookingclass->teacher_id) }}" class="chefNameLink">Chef Madhusudan</a></span>
                                          <br />
                                            <span class="chef-tag">MasterChef</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--item-->

                            @empty
		                    <p>No recipes found</p>
		                    <!--item-->
		                    @endforelse

                            <div class="quicklinks">
                                <a href="#" class="button">More Courses</a>
                                <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
                            </div>
                        </div>
                            <!--//entries-->
					</div>
					<!--//cwrap-->

				</section>
                <aside class="sidebar one-fourth">
					<div class="widget wow fadeInRight">
						<h3>Cooking Classes By Categories</h3>
						<ul class="boxed">
                            <li class="light"><a href="{{ url('teacher/cookingclasses') }}" title="Appetizers"><i class="ico i-appetizers"></i> <span>Apetizers</span></a></li>
                            <li class="medium"><a href="{{ url('teacher/cookingclasses') }}" title="Cocktails"><i class="ico i-cocktails"></i> <span>Cocktails</span></a></li>
                            <li class="dark"><a href="{{ url('teacher/cookingclasses') }}" title="Deserts"><i class="ico i-deserts"></i> <span>Deserts</span></a></li>
                            
                            <li class="medium"><a href="{{ url('teacher/cookingclasses') }}" title="Cocktails"><i class="ico i-eggs"></i> <span>Eggs</span></a></li>
                            <li class="dark"><a href="{{ url('teacher/cookingclasses') }}" title="Equipment"><i class="ico i-equipment"></i> <span>Equipment</span></a></li>
                            <li class="light"><a href="{{ url('teacher/cookingclasses') }}" title="Events"><i class="ico i-events"></i> <span>Events</span></a></li>
                        
                            <li class="dark"><a href="{{ url('teacher/cookingclasses') }}" title="Fish"><i class="ico i-fish"></i> <span>Fish</span></a></li>
                            <li class="light"><a href="{{ url('teacher/cookingclasses') }}" title="Ftness"><i class="ico i-fitness"></i> <span>Fitness</span></a></li>
                            <li class="medium"><a href="{{ url('teacher/cookingclasses') }}" title="Healthy"><i class="ico i-healthy"></i> <span>Healthy</span></a></li>
                            
                            <li class="light"><a href="{{ url('teacher/cookingclasses') }}" title="Asian"><i class="ico i-asian"></i> <span>Asian</span></a></li>
                            <li class="medium"><a href="{{ url('teacher/cookingclasses') }}" title="Mexican"><i class="ico i-mexican"></i> <span>Mexican</span></a></li>
                            <li class="dark"><a href="{{ url('teacher/cookingclasses') }}" title="Pizza"><i class="ico i-pizza"></i> <span>Pizza</span></a></li>
                            
                            <li class="medium"><a href="{{ url('teacher/cookingclasses') }}" title="Kids"><i class="ico i-kids"></i> <span>Kids</span></a></li>
                            <li class="dark"><a href="{{ url('teacher/cookingclasses') }}" title="Meat"><i class="ico i-meat"></i> <span>Meat</span></a></li>
                            <li class="light"><a href="{{ url('teacher/cookingclasses') }}" title="Snacks"><i class="ico i-snacks"></i> <span>Snacks</span></a></li>
                            
                            <li class="dark"><a href="{{ url('teacher/cookingclasses') }}" title="Salads"><i class="ico i-salads"></i> <span>Salads</span></a></li>
                            <li class="light"><a href="{{ url('teacher/cookingclasses') }}" title="Storage"><i class="ico i-storage"></i> <span>Storage</span></a></li>
                            <li class="medium"><a href="{{ url('teacher/cookingclasses') }}" title="Vegetarian"><i class="ico i-vegetarian"></i> <span>Vegetarian</span></a></li>
                        </ul>
					</div>
                    <div class="widget members wow fadeInRight" data-wow-delay=".4s">
						<h3>Top Chefs Provide Classes</h3>
						<div id="members-list-options" class="item-options">
						  <a href="#">Newest</a>
						  <a class="selected" href="#">Active</a>
						  <a href="#">Popular</a>
						</div>
						<ul class="boxed">
                            <li><div class="avatar"><a href="{{ url('teacher/instructorprofile/'.$cookingclass->teacher_id) }}"><img src="{{ asset('images/avatar1.jpg') }}" alt="" /><span>Kimberly C.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('teacher/instructorprofile/'.$cookingclass->teacher_id) }}"><img src="{{ asset('images/avatar2.jpg') }}" alt="" /><span>Alex J.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('teacher/instructorprofile/'.$cookingclass->teacher_id) }}"><img src="{{ asset('images/avatar3.jpg') }}" alt="" /><span>Denise M.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('/instructor-profile') }}"><img src="{{ asset('images/avatar4.jpg') }}" alt="" /><span>Jason H.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('/instructor-profile') }}"><img src="{{ asset('images/avatar5.jpg') }}" alt="" /><span>Jennifer W.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('/instructor-profile') }}"><img src="{{ asset('images/avatar6.jpg') }}" alt="" /><span>Anabelle Q.</span></a></div></li>                   
                            <li><div class="avatar"><a href="{{ url('/instructor-profile') }}"><img src="{{ asset('images/avatar7.jpg') }}" alt="" /><span>Thomas M.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('/instructor-profile') }}"><img src="{{ asset('images/avatar8.jpg') }}" alt="" /><span>Michelle S.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('/instructor-profile') }}"><img src="{{ asset('images/avatar9.jpg') }}" alt="" /><span>Bryan A.</span></a></div></li>      
                            <li><div class="avatar"><a href="{{ url('/instructor-profile') }}"><img src="{{ asset('images/avatar1.jpg') }}" alt="" /><span>Kimberly C.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('/instructor-profile') }}"><img src="{{ asset('images/avatar2.jpg') }}" alt="" /><span>Alex J.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('/instructor-profile') }}"><img src="{{ asset('images/avatar3.jpg') }}" alt="" /><span>Denise M.</span></a></div></li>
                        </ul>
					</div>
                    
                    <div class="widget wow fadeInRight"><a href="themeforest.net/item/socialchef-social-recipe-wordpress-theme/6786673?ref=themeenergy"><img src="http://themeenergy.mthitsolutions1.netdna-cdn.com/themes/wordpress/social-chef/wp-content/uploads/2015/01/banner.png"></a>
                    </div>
				</aside>
			</div>
			<!--//row-->
		</div>
		<!--//wrap-->
	
@endsection