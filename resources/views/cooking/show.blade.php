@extends('layouts.master')

@section('title', 'Cookpro | Cooking Classes')

@section('content')

		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
				  <li><a href="#" title="Recipes">Recipes</a></li>
				  <li><a href="recipes.php" title="Cocktails">Deserts</a></li>
				  <li>Recipe</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->
			
			<!--row-->
			<div class="row">
				<header class="s-title wow fadeInLeft">
					<h1>A luxurious black &amp; white chocolate cupcake</h1>
				</header>
				<!--content-->
				<section class="content three-fourth">
					<div class="row">
					<!--recipe-->
						<div class="recipe">
							<!--two-third-->
							<article class="full-width wow fadeInLeft">
								<div class="image"><a href="#"><img src="images/img.jpg" alt="" /></a></div>
								<div class="intro"><p><strong>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas</strong></p> <p>Molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p></div>
								<div class="instructions">
									<ol>
										<li>Heat oven to 160C/140C fan/gas 3 and line a 12-hole muffin tin with cases. Gently melt the butter, chocolate, sugar and 100ml hot water together in a large saucepan, stirring occasionally, then set aside to cool a little while you weigh the other ingredients.</li>
										<li>Stir the eggs and vanilla into the chocolate mixture. Put the flour into a large mixing bowl, then stir in the chocolate mixture until smooth. Spoon into cases until just over three-quarters full (you may have a little mixture leftover), then set aside for 5 mins before putting on a low shelf in the oven and baking for 20-22 mins. Leave to cool.</li>
										<li>For the icing, melt the chocolate in a heatproof bowl over a pan of barely simmering water. Once melted, turn off the heat, stir in the double cream and sift in the icing sugar. When spreadable, top each cake with some and decorate with your favourite sprinkles and sweets.</li>
									</ol>
								</div>
							</article>
							<!--//two-third-->
						</div>
						<!--//recipe-->
							
						<!--comments-->
						<div class="comments wow fadeInUp" id="comments">
							<h2>5 comments </h2>
							<ol class="comment-list">
								<!--comment-->
								<li class="comment depth-1">
									<div class="avatar"><a href="my_profile.php"><img src="images/avatar1.jpg" alt="" /></a></div>
									<div class="comment-box">
										<div class="comment-author meta"> 
											<strong>Kimberly C.</strong> said 1 month ago <a href="#" class="comment-reply-link"> Reply</a>
										</div>
										<div class="comment-text">
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
										</div>
									</div> 
								</li>
								<!--//comment-->
								
								<!--comment-->
								<li class="comment depth-1">
									<div class="avatar"><a href="my_profile.php"><img src="images/avatar2.jpg" alt="" /></a></div>
									<div class="comment-box">
										<div class="comment-author meta"> 
											<strong>Alex J.</strong> said 1 month ago <a href="#" class="comment-reply-link"> Reply</a>
										</div>
										<div class="comment-text">
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
										</div>
									</div> 
								</li>
								<!--//comment-->
								
								<!--comment-->
								<li class="comment depth-2">
									<div class="avatar"><a href="my_profile.php"><img src="images/avatar1.jpg" alt="" /></a></div>
									<div class="comment-box">
										<div class="comment-author meta"> 
											<strong>Kimberly C.</strong> said 1 month ago <a href="#" class="comment-reply-link"> Reply</a>
										</div>
										<div class="comment-text">
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
										</div>
									</div> 
								</li>
								<!--//comment-->
								
								<!--comment-->
								<li class="comment depth-3">
									<div class="avatar"><a href="my_profile.php"><img src="images/avatar2.jpg" alt="" /></a></div>
									<div class="comment-box">
										<div class="comment-author meta"> 
											<strong>Alex J.</strong> said 1 month ago <a href="#" class="comment-reply-link"> Reply</a>
										</div>
										<div class="comment-text">
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
										</div>
									</div> 
								</li>
								<!--//comment-->
								
								<!--comment-->
								<li class="comment depth-1">
									<div class="avatar"><a href="my_profile.php"><img src="images/avatar3.jpg" alt="" /></a></div>
									<div class="comment-box">
										<div class="comment-author meta"> 
											<strong>Denise M.</strong> said 1 month ago <a href="#" class="comment-reply-link"> Reply</a>
										</div>
										<div class="comment-text">
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
										</div>
									</div> 
								</li>
								<!--//comment-->
							</ol>
						</div>
						<!--//comments-->
						
						<!--respond-->
						<div class="comment-respond wow fadeInUp" id="respond">
							<h2>Leave a reply</h2>
							<div class="container">
								<p><strong>Note:</strong> Comments on the web site reflect the views of their authors, and not necessarily the views of the socialchef internet portal. Requested to refrain from insults, swearing and vulgar expression. We reserve the right to delete any comment without notice explanations.</p>
								<p>Your email address will not be published. Required fields are signed with <span class="req">*</span></p>
								<form>
									<div class="f-row">
										<div class="third">
											<input type="text" placeholder="Your name" />
											<span class="req">*</span>
										</div>
										
										<div class="third">
											<input type="email" placeholder="Your email" />
											<span class="req">*</span>
										</div>
										
										<div class="third">
											<input type="text" placeholder="Your website" />
										</div>
									
									</div>
									<div class="f-row">
										<textarea></textarea>
									</div>
									
									<div class="f-row">
										<div class="third captcha">
											<label>How much is  2+10?</label>
											<input type="text" />
											<span class="req">*</span>
										</div>
										
										<div class="third bwrap">
											<input type="submit" value="Submit comment" />
										</div>
									</div>
									
									<div class="bottom">
										<div class="f-row checkbox">
											<input type="checkbox" id="ch1" />
											<label for="ch1">Notify me of replies to my comment via e-mail</label>
										</div>
										<div class="f-row checkbox">
											<input type="checkbox" id="ch2" />
											<label for="ch2">Notify me of new articles by email.</label>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!--//respond-->
					</div>
				</section>
				<!--//content-->
				
				<!--right sidebar-->
				<aside class="sidebar one-fourth wow fadeInRight">
					<header class="s-title">
                        <h2 class="ribbon bright"><a href="{{ url('/cooking-membership') }}" title="Home" style="color:#fff;">Join This Course</a></h2>
                    </header>
                	<dl class="basic">
                        <dt>Course Time Period</dt>
                        <dd>1 month</dd>
                        <dt>Course Type</dt>
                        <dd>Beginner</dd>
                    </dl>
                    
                    <dl class="user">
                        <dt>Category</dt>
                        <dd>Deserts</dd>
                        <dt>Posted by</dt>
                        <dd>Jennifer W.</dd>
                    </dl>
					
					<div class="widget wow fadeInRight" data-wow-delay=".2s">
						<h3>Tips and tricks</h3>
						<ul class="articles_latest">
							<li>
								<a href="blog_single.php">
									<img src="images/img9.jpg" alt="" />
									<h6>How to decorate cookies</h6>
								</a>
							</li>
							<li>
								<a href="blog_single.php">
									<img src="images/img10.jpg" alt="" />
									<h6>Make your own bread</h6>
								</a>
							</li>
							<li>
								<a href="blog_single.php">
									<img src="images/img11.jpg" alt="" />
									<h6>How to make sushi</h6>
								</a>
							</li>
						</ul>
					</div>
					<div class="widget share">
						<ul class="boxed">
							<li class="light"><a href="#" title="Facebook"><i class="ico i-facebook"></i> <span>Share on Facebook</span></a></li>
							<li class="medium"><a href="#" title="Twitter"><i class="ico i-twitter"></i> <span>Share on Twitter</span></a></li>
							<li class="dark"><a href="#" title="Favourites"><i class="ico i-favourites"></i> <span>Add to Favourites</span></a></li>
						</ul>
					</div>
					
					<div class="widget members">
						<h3>Members who liked this course</h3>
						<ul class="boxed">
                            <li><div class="avatar"><a href="{{ url('userprofile') }}"><img src="images/avatar1.jpg" alt="" /><span>Kimberly C.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('userprofile') }}"><img src="images/avatar2.jpg" alt="" /><span>Alex J.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('userprofile') }}"><img src="images/avatar3.jpg" alt="" /><span>Denise M.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('userprofile') }}"><img src="images/avatar9.jpg" alt="" /><span>Jason H.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('userprofile') }}"><img src="images/avatar8.jpg" alt="" /><span>Jennifer W.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('userprofile') }}"><img src="images/avatar4.jpg" alt="" /><span>Anabelle Q.</span></a></div></li>                 
                            <li><div class="avatar"><a href="{{ url('userprofile') }}"><img src="images/avatar7.jpg" alt="" /><span>Thomas M.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('userprofile') }}"><img src="images/avatar5.jpg" alt="" /><span>Michelle S.</span></a></div></li>
                            <li><div class="avatar"><a href="{{ url('userprofile') }}"><img src="images/avatar6.jpg" alt="" /><span>Bryan A.</span></a></div></li>
                        </ul>
					</div>
                    
				</aside>
				<!--//right sidebar-->
			</div>
			<!--//row-->
		</div>
		<!--//wrap-->

@endsection