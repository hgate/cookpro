@extends('layouts.master')

@section('title', 'Cookpro | shop')

@section('content')

		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
					<li>Page</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->

			<!--row-->
			<div class="row">
				<header class="s-title wow fadeInLeft">
					<h1>Grocery List</h1>
				</header>
					
				<!--left sidebar-->
				<aside class="sidebar one-fourth wow fadeInLeft">
                	<div class="widget">
                    	<h3 style="background: #4c3c3c none repeat scroll 0 0; color: #fff;">Shop By Brand</h3>
						<ul class="categories left">
                        	<li><a href="#">Supermarket</a></li>
                            <li class="active"><a href="#">Fresh Direct</a></li>
							<li><a href="#">Peapod</a></li>
						</ul>
					</div>
					<div class="widget">
                    	<h3 style="background: #4c3c3c none repeat scroll 0 0; color: #fff;">Products</h3>
						<ul class="categories left">
                        	<li><a href="#">Fruits</a></li>
                            <li><a href="#">Cereals</a></li>
                            <li class="active"><a href="#">Vegetables</a></li>
                            <li><a href="#">Breakfast</a></li>
							<li><a href="#">Dairy</a></li>
                            <li><a href="#">Grocery</a></li>
							<li><a href="#">Feed & Seed</a></li>
							<li><a href="#">Sweets</a></li>
                            <li><a href="#">Sacks</a></li>
                            <li><a href="#">Frozen</a></li>
							<li><a href="#">Grains and Beans</a></li>
							<li><a href="#">Herbs & Spices</a></li>
							<li><a href="#">Meat & Fish</a></li>
							<li><a href="#">Bakery</a></li>
                            <li><a href="#">Coffee & Tea</a></li>
                            <li><a href="#">Beverages</a></li>
                            <li><a href="#">Alcohol</a></li>
                            <li><a href="#">Baby Foods</a></li>
                            <li><a href="#">Chocolates</a></li>
						</ul>
					</div>
				
				</aside>
				<!--//left sidebar-->
				
				<section class="content three-fourth">
			
					<!--cwrap-->
					<div class="cwrap">
						
						<!--entries-->
						<div class="entries row">
							<!--item-->
							<div class="entry one-third wow fadeInLeft">
								<figure>
									<img src="images/veg1.jpg" alt="" />
								</figure>
								<div class="container">
									<h2><a href="#">broccoli</a></h2> 
                                    <div class="actions">
										<div>
                                        	<div class=""><a href="#">Price : $35</a></div>
											<div class=""><a class="comment-reply-link" href="{{ url('shopping-list') }}"> Add</a></div>
										</div>
									</div>
								</div>
							</div>
							<!--item-->
							<div class="entry one-third wow fadeInLeft">
								<figure>
									<img src="images/veg2.jpg" alt="" />
								</figure>
								<div class="container">
									<h2><a href="">capsicum</a></h2> 
                                    <div class="actions">
										<div>
                                        	<div class=""><a href="#">Price : $35</a></div>
											<div class=""><a class="comment-reply-link" href="{{ url('shopping-list') }}"> Add</a></div>
										</div>
									</div>
								</div>
							</div>
							<!--item-->
							<div class="entry one-third wow fadeInLeft" data-wow-delay=".4s">
								<figure>
									<img src="images/veg3.jpg" alt="" />
								</figure>
								<div class="container">
									<h2><a href="recipe.php">carrot</a></h2> 
                                    <div class="actions">
										<div>
                                        	<div class=""><a href="#">Price : $35</a></div>
											<div class=""><a class="comment-reply-link" href="{{ url('shopping-list') }}"> Add</a></div>
										</div>
									</div>
								</div>
							</div>
							<!--item-->
							
							<!--item-->
							<div class="entry one-third wow fadeInLeft" data-wow-delay=".6s">
								<figure>
									<img src="images/veg4.jpg" alt="" />
								</figure>
								<div class="container">
									<h2><a href="recipe.php">Corn</a></h2> 
                                    <div class="actions">
										<div>
                                        	<div class=""><a href="#">Price : $35</a></div>
											<div class=""><a class="comment-reply-link" href="{{ url('shopping-list') }}"> Add</a></div>
										</div>
									</div>
								</div>
							</div>
							<!--item-->
							
							<!--item-->
							<div class="entry one-third wow fadeInLeft" data-wow-delay=".8s">
								<figure>
									<img src="images/veg5.jpg" alt="" />
								</figure>
								<div class="container">
									<h2><a href="recipe.php">Tomato</a></h2> 
                                    <div class="actions">
										<div>
                                        	<div class=""><a href="#">Price : $35</a></div>
											<div class=""><a class="comment-reply-link" href="{{ url('shopping-list') }}"> Add</a></div>
										</div>
									</div>
								</div>
							</div>
							<!--item-->
							
							<!--item-->
							<div class="entry one-third wow fadeInLeft" data-wow-delay="1s">
								<figure>
									<img src="images/veg6.jpg" alt="" />
								</figure>
								<div class="container">
									<h2><a href="recipe.php">Lettuce</a></h2> 
                                    <div class="actions">
										<div>
                                        	<div class=""><a href="#">Price : $35</a></div>
											<div class=""><a class="comment-reply-link" href="{{ url('shopping-list') }}"> Add</a></div>
										</div>
									</div>
								</div>
							</div>
							<!--item-->
							
							<div class="quicklinks">
								<a href="#" class="button">More</a>
								<a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
							</div>
						</div>
						<!--//entries-->
					</div>
					<!--//cwrap-->

                    <!--<div class="textwidget"><a href="themeforest.net/item/book-your-travel-online-booking-wordpress-theme/5632266?ref=themeenergy">
<img src="http://themeenergy.mthitsolutions1.netdna-cdn.com/themes/wordpress/social-chef/wp-content/uploads/2015/01/banner2.png">
</a></div>-->
				</section>	
			</div>
			<!--//row-->
		</div>
		<!--//wrap-->

@endsection