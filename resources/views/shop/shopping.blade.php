@extends('layouts.master')

@section('title', 'Cookpro | shop')

@section('content')

		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
					<li>Page</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->

			<!--row-->
			<div class="row">
				<header class="s-title wow fadeInLeft">
					<h1>Grocery List</h1>
				</header>
					
				<!--left sidebar-->
				<aside class="sidebar one-fourth wow fadeInLeft">
					<div class="widget">
                    	<h3>Products</h3>
						<ul class="categories left">
                        	<li><a href="#">Fruits</a></li>
                            <li><a href="#">Vegetables</a></li>
							<li><a href="#">Cereals</a></li>
                            <li><a href="#">Breakfast</a></li>
							<li class="active"><a href="#">Dairy</a></li>
                            <li><a href="#">Grocery</a></li>
							<li><a href="#">Feed & Seed</a></li>
							<li><a href="#">Sweets</a></li>
                            <li><a href="#">Sacks</a></li>
                            <li><a href="#">Frozen</a></li>
							<li><a href="#">Grains and Beans</a></li>
							<li><a href="#">Herbs & Spices</a></li>
							<li><a href="#">Meat & Fish</a></li>
							<li><a href="#">Bakery</a></li>
                            <li><a href="#">Coffee & Tea</a></li>
                            <li><a href="#">Beverages</a></li>
                            <li><a href="#">Alcohol</a></li>
                            <li><a href="#">Baby Foods</a></li>
                            <li><a href="#">Chocolates</a></li>
						</ul>
					</div>
					
					<div class="widget">
						<h3>Search</h3>
						<div class="f-row">
							<input type="search" placeholder="Enter your search term" />
						</div>
						<div class="f-row">
							<input type="submit" value="Search" />
						</div>
					</div>
					<div class="widget members">
						<h3>Our members</h3>
						<div id="members-list-options" class="item-options">
						  <a href="#">Newest</a>
						  <a class="selected" href="#">Active</a>
						  <a href="#">Popular</a>
						</div>
						<ul class="boxed">
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar1.jpg" alt="" /><span>Kimberly C.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar2.jpg" alt="" /><span>Alex J.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar3.jpg" alt="" /><span>Denise M.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar9.jpg" alt="" /><span>Jason H.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar8.jpg" alt="" /><span>Jennifer W.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar4.jpg" alt="" /><span>Anabelle Q.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar7.jpg" alt="" /><span>Thomas M.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar5.jpg" alt="" /><span>Michelle S.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar6.jpg" alt="" /><span>Bryan A.</span></a></div></li>
						</ul>
					</div>
				</aside>
				<!--//left sidebar-->
				
				<section class="content three-fourth">
			<!--container-->
			<article id="page-399" class="cartcontainer">
				<div class="woocommerce">
                    <table cellspacing="0" class="">
                        <thead>
                            <tr>
                                <th class="product-remove">&nbsp;</th>
                                <th class="product-thumbnail">&nbsp;</th>
                                <th class="product-name">Product</th>
                                <th class="product-price">Price</th>
                                <th class="product-quantity">Quantity</th>
                                <th class="product-subtotal">Total</th>
                            </tr>
                        </thead>
                        <tbody>
						<tr class="cart_item">
							<td class="product-remove"><a href="#">�</a></td>
							<td class="product-thumbnail"><a href="#"><img width="90" height="90" src="images/veg1.jpg"></a></td>
							<td data-title="Product" class="product-name"><a href="#">Happy Ninja</a></td>
							<td data-title="Price" class="product-price">
								<span class="amount">$18.00</span>
                            </td>
							<td data-title="Quantity" class="product-quantity">
								<div class="quantity">
								<input type="number" size="4" class="input-text qty text" title="Qty" value="1" name="" max="" min="0" step="1">
								</div>
							</td>
							<td data-title="Total" class="product-subtotal">
								<span class="amount">$18.00</span>
                            </td>
						</tr>
						<tr>
							<td class="" colspan="6">
								<div class="coupon" style="width:50%; float:left;">
                                    <input type="text" placeholder="Coupon code" value="" id="coupon_code" name="coupon_code" style="float:left;"> 
								</div>
                                <div class="coupon" style="width:50%; float:left;">
                                <input type="submit" value="Apply Coupon" name="apply_coupon" class="button" style="float:left; margin-left:30px;">
								</div>
                               	<div class="coupon" style="width:100%; float:right; margin-top:10px;">
									<input type="submit" value="Update Cart" name="update_cart" class="button" style="float:right;">
                                  </div>
                           </td>
						</tr>
					</tbody>
				</table>

				<div class="cart-collaterals">
					<div class="cart_totals ">
							<table cellspacing="0" style="width:50%; float:right;">
                                <tbody>
                                	<tr>
                                        <th colspan="2">Cart Totals</th>
                                    </tr>
                                    <tr class="cart-subtotal">
                                        <th>Subtotal</th>
                                        <td data-title="Subtotal"><span class="amount">$18.00</span></td>
                                    </tr>
                                    <tr class="shipping">
                                        <th>Shipping</th>
                                        <td data-title="Shipping">
											<p>Shipping costs will be calculated once you have provided your address.</p>
                                            <form method="post" action="">
                                                <p><a class="shipping-calculator-button" href="#">Calculate Shipping</a></p>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr class="order-total">
                                        <th>Total</th>
                                        <td data-title="Total"><strong><span class="amount">$18.00</span></strong> </td>
                                    </tr>
                                    <tr>
                                    	<td colspan="2" style="background:none; border:0px;"><a class="checkout-button button alt wc-forward" href="">Proceed to Checkout</a>
                                        </td>
                                    </tr>    
								</tbody>
                           </table>

	
</div>

</div>

</div>
							</article><!--//container-->
		</section>	
			</div>
			<!--//row-->
		</div>
		<!--//wrap-->

@endsection