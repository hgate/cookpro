@extends('layouts.home')

@section('title', 'welcome')

@section('content')
	
		<!--intro-->
        
		<div class="intro">
			<figure class="bg"><img src="images/intro.jpg" alt="" /></figure>
			
			<!--wrap-->
			<div class="wrap clearfix">
				<!--row-->
                <div class="row">
					<article class="ten-percent wow fadeInLeft"></article>
					
					<article class="eighty-percent text wow zoomIn" data-wow-delay=".2s">
						<h1>Hand Selected, seasonal ingredients from the best farms </h1>
						<p>Organic & non-GMO produce</p>
						<p>Sustainably sourced, seasonal ingredients from top farms</p>
						<a href="{{ url('register') }}" class="button white more medium">Join Us</a>
					
					</article>
					
				</div>
				<!--//row-->
			</div>
			<!--//wrap-->
		</div>
		<!--//intro-->
         <section class="catbox">
            <div class="wrap">
                <div class="">
                    <!--content-->
                    <div class="how_it_work">
					   <div class="w-divider6"><i class="colora fa fa-cutlery"></i><h3><span class="spr">How it works</span></h3></div>
					   <div class="colounm_box">
					      <span><img src="images/people.png" alt="" /></span>
						  <h4>We create mouth-watering recipes</h4>
						  <p>that are delicious and nutritious</p>
					   </div>
					   
					   <div class="colounm_box">
					      <span><img src="images/food-1.png" alt="" /></span>
						  <h4 style="color:#F4716A;">You choose what you like</h4>
						  <p>and we send you all the ingredients</p>
					   </div>
					   
					   <div class="colounm_box">
					      <span><img src="images/transport.png" alt="" /></span>
						  <h4>We deliver to your door for free</h4>
						  <p>so you can skip the stress of the trip to the supermarket</p>
					   </div>
					   
					   <div class="colounm_box last">
					      <span><img src="images/food.png" alt="" /></span>
						  <h4 style="color:#F4716A;">You cook a fun, healthy and tasty meal </h4>
						  <p>in around 30 minutes</p>
					   </div>
					</div>
                    <!--//content-->
                    						
                    <div class="cta">
                        <a href="login.html" class="button big">Join us!</a>
                    </div>
                </div>
            </div>
		</section>
        <section class="cta" style="background-color:#F4716A; background-image:none;">
            <div class="wrap">
            <div class="w-divider6"><i class="colora fa fa-cutlery"></i><h3><span class="spr" style="color:#fff; ">Recipes</span></h3></div>

            	<div id="hometabs" class="hometabs" style="background:none; box-shadow:none;">
				<nav>
					<ul>
						<li><a href="#section-1" class="icon-shop"><span>Breakfast</span></a></li>
						<li><a href="#section-2" class="icon-cup"><span>Lunch</span></a></li>
						<li><a href="#section-3" class="icon-food"><span>Dinner</span></a></li>
						<li><a href="#section-4" class="icon-lab"><span>Dessert</span></a></li>
						<li><a href="#section-5" class="icon-truck"><span>Drinks</span></a></li>
					</ul>
				</nav>
				<div class="content">
					<section id="section-1">
						<div class="mediabox">
							<img src="img/img1.jpg" alt="img01" />
							<h3>Sushi Gumbo Beetroot</h3>
						</div>
						<div class="mediabox">
							<img src="img/img2.jpg" alt="img02" />
							<h3>Pea Sprouts Fava Soup</h3>
						</div>
						<div class="mediabox">
							<img src="img/img3.jpg" alt="img03" />
							<h3>Turnip Broccoli Sashimi</h3>
						</div>
					</section>
					<section id="section-2">
						<div class="mediabox">
							<img src="img/img4.jpg" alt="img04" />
							<h3>Asparagus Cake</h3>
						</div>
						<div class="mediabox">
							<img src="img/img5.jpg" alt="img05" />
							<h3>Magis Kohlrabi Gourd</h3>
						</div>
						<div class="mediabox">
							<img src="img/img6.jpg" alt="img06" />
							<h3>Ricebean Rutabaga</h3>
						</div>
					</section>
					<section id="section-3">
						<div class="mediabox">
							<img src="img/img7.jpg" alt="img02" />
							<h3>Noodle Curry</h3>
						</div>
						<div class="mediabox">
							<img src="img/img8.jpg" alt="img06" />
							<h3>Leek Wasabi</h3>
						</div>
						<div class="mediabox">
							<img src="img/img9.jpg" alt="img01" />
							<h3>Green Tofu Wrap</h3>
						</div>
					</section>
					<section id="section-4">
						<div class="mediabox">
							<img src="img/img10.jpg" alt="img03" />
							<h3>Tomato Cucumber Curd</h3>
						</div>
						<div class="mediabox">
							<img src="img/img11.jpg" alt="img01" />
							<h3>Mushroom Green</h3>
						</div>
						<div class="mediabox">
							<img src="img/img12.jpg" alt="img04" />
							<h3>Swiss Celery Chard</h3>
						</div>
					</section>
					<section id="section-5">
						<div class="mediabox">
							<img src="img/img13.jpg" alt="img02" />
							<h3>Radish Tomato</h3>
						</div>
						<div class="mediabox">
							<img src="img/img14.jpg" alt="img06" />
							<h3>Fennel Wasabi</h3>
						</div>
						<div class="mediabox">
							<img src="img/img15.jpg" alt="img01" />
							<h3>Red Tofu Wrap</h3>
						</div>
					</section>
				</div><!-- /content -->
			</div>
            <script src="js/cbpFWTabs.js"></script>
		<script>
			new CBPFWTabs( document.getElementById( 'hometabs' ) );
		</script>
            </div>
        </section>    
        
        
		
		<section class="catbox">
            <div class="wrap">
               <section class="content full-width">
					<div class="icons dynamic-numbers">
						<header class="s-title wow fadeInDown">
							<h2 class="ribbon large">Cookpro in numbers</h2>
						</header>
						
						<div class="row wow fadeInUp">
							<div class="one-sixth">
								<div class="container">
                                        <i class="ico i-recipes"></i>
                                        <span class="title dynamic-number" data-dnumber="1250">0</span>
                                        <span class="subtitle"><a href="recipes.php" title="Recipes">Recipes</a></span>
                                    </div>
							</div>
							<div class="one-sixth">
								<div class="container">
                                        <i class="ico i-photos"></i>
                                        <span class="title dynamic-number" data-dnumber="5300">0</span>
                                        <span class="subtitle"><a href="video.php" title="Recipes">Videos</a></span>
                                </div>
							</div>
							<div class="one-sixth">
								<div class="container">
                                    <i class="ico i-members"></i>
                                    <span class="title dynamic-number" data-dnumber="1730">0</span>
                                    <span class="subtitle"><a href="#" title="Chefs">Chefs</a></span>
                               </div>
							</div>
							<div class="one-sixth">
								<div class="container">
                                        <i class="ico i-posts"></i>
                                        <span class="title dynamic-number" data-dnumber="2300">0</span>
                                        <span class="subtitle"><a href="features.html" title="Services">Groups</a></span>
                                    </div>
							</div>
							<div class="one-sixth">
								<div class="container">
                                        <i class="ico i-members"></i>
                                        <span class="title dynamic-number" data-dnumber="7400">0</span>
                                        <span class="subtitle"><a href="#" title="Dietitians">Dietitians</a></span>
                                </div>
							</div>
							<div class="one-sixth">
								<div class="container">
                                        <i class="ico i-articles"></i>
                                        <span class="title dynamic-number" data-dnumber="1700">0</span>
                                        <span class="subtitle"><a href="blog.php" title="Recipes">Articles</a></span>
                                </div>
							</div>
						</div>
					</div>
				</section>
            </div>
		</section>
        
		<!--wrap-->
        <section class="cooking_bg" style="background-color:#F4716A; background-image:none;">
            <div class="wrap">
                <div class="w-divider6"><i class="colora fa fa-cutlery"></i>
                        <h3><span style="color:#fff;" class="spr">Cooking Classes</span></h3>
                </div>
            </div>
            <div id="ca-container" class="ca-container">
				<div class="ca-wrapper">
					<div class="ca-item ca-item-1">
						<div class="ca-item-main">
                        	<div class="entry entry">
								<figure>
									<img alt="" src="images/img2.jpg">
									<figcaption><a href="blog_single.html"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
								</figure>
								<div class="container">
									<h2><a href="blog_single.html">Baking Classes</a></h2> 
									<div class="excerpt">
										<p>If you want to keep your weight in check the healthy way by eating right and being active, then this is for you!</p>
									</div>
								</div>
						     </div>
	 						 <a href="#" class="ca-more">more...</a>
						</div>
						<div class="ca-content-wrapper">
							<div class="ca-content">
								<h6>Baking Classes</h6>
								<a href="#" class="ca-close">close</a>
								<div class="ca-content-text">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
								</div>
								<ul>
									<li><a href="#">Read more</a></li>
									<li><a href="#">Share this</a></li>
									<li><a href="#">Become a member</a></li>
									<li><a href="#">Donate</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="ca-item ca-item-2">
						<div class="ca-item-main">
							<div class="entry entry">
								<figure>
									<img alt="" src="images/img.jpg">
									<figcaption><a href="blog_single.html"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
								</figure>
								<div class="container">
									<h2><a href="blog_single.html">Delicious Dessert</a></h2> 
									<div class="excerpt">
										<p>If you want to keep your weight in check the healthy way by eating right and being active, then this is for you!</p>
									</div>
								</div>
						     </div>
                             <a href="#" class="ca-more">more...</a>
						</div>
						<div class="ca-content-wrapper">
							<div class="ca-content">
								<h6>Delicious Dessert</h6>
								<a href="#" class="ca-close">close</a>
								<div class="ca-content-text">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
								</div>
								<ul>
									<li><a href="#">Read more</a></li>
									<li><a href="#">Share this</a></li>
									<li><a href="#">Become a member</a></li>
									<li><a href="#">Donate</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="ca-item ca-item-3">
						<div class="ca-item-main">
							<div class="entry entry">
								<figure>
									<img alt="" src="images/food3.jpg">
									<figcaption><a href="blog_single.html"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
								</figure>
								<div class="container">
									<h2><a href="blog_single.html">Breakfast Recipes</a></h2> 
									<div class="excerpt">
										<p>If you want to keep your weight in check the healthy way by eating right and being active, then this is for you!</p>
									</div>
								</div>
						     </div>
								<a href="#" class="ca-more">more...</a>
						</div>
						<div class="ca-content-wrapper">
							<div class="ca-content">
								<h6>Breakfast Recipes</h6>
								<a href="#" class="ca-close">close</a>
								<div class="ca-content-text">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
								</div>
								<ul>
									<li><a href="#">Read more</a></li>
									<li><a href="#">Share this</a></li>
									<li><a href="#">Become a member</a></li>
									<li><a href="#">Donate</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="ca-item ca-item-4">
						<div class="ca-item-main">
							<div class="ca-icon"></div>
							<h3>Healthy Recipes</h3>
							<h4>
								<span class="ca-quote">&ldquo;</span>
								<span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</span>
							</h4>
								<a href="#" class="ca-more">more...</a>
						</div>
						<div class="ca-content-wrapper">
							<div class="ca-content">
								<h6>Healthy Recipes</h6>
								<a href="#" class="ca-close">close</a>
								<div class="ca-content-text">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
								</div>
								<ul>
									<li><a href="#">Read more</a></li>
									<li><a href="#">Share this</a></li>
									<li><a href="#">Become a member</a></li>
									<li><a href="#">Donate</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="ca-item ca-item-5">
						<div class="ca-item-main">
							<div class="ca-icon"></div>
							<h3>Deserts Recipes</h3>
							<h4>
								<span class="ca-quote">&ldquo;</span>
								<span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</span>
							</h4>
								<a href="#" class="ca-more">more...</a>
						</div>
						<div class="ca-content-wrapper">
							<div class="ca-content">
								<h6>Deserts Recipes</h6>
								<a href="#" class="ca-close">close</a>
								<div class="ca-content-text">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
								</div>
								<ul>
									<li><a href="#">Read more</a></li>
									<li><a href="#">Share this</a></li>
									<li><a href="#">Become a member</a></li>
									<li><a href="#">Donate</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="ca-item ca-item-6">
						<div class="ca-item-main">
							<div class="ca-icon"></div>
							<h3>Cooking Classes</h3>
							<h4>
								<span class="ca-quote">&ldquo;</span>
								<span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</span>
							</h4>
								<a href="#" class="ca-more">more...</a>
						</div>
						<div class="ca-content-wrapper">
							<div class="ca-content">
								<h6>Cooking Classes</h6>
								<a href="#" class="ca-close">close</a>
								<div class="ca-content-text">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
								</div>
								<ul>
									<li><a href="#">Read more</a></li>
									<li><a href="#">Share this</a></li>
									<li><a href="#">Become a member</a></li>
									<li><a href="#">Donate</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="ca-item ca-item-7">
						<div class="ca-item-main">
							<div class="ca-icon"></div>
							<h3>Cooking Classes</h3>
							<h4>
								<span class="ca-quote">&ldquo;</span>
								<span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</span>
							</h4>
								<a href="#" class="ca-more">more...</a>
						</div>
						<div class="ca-content-wrapper">
							<div class="ca-content">
								<h6>Healthy Recipes</h6>
								<a href="#" class="ca-close">close</a>
								<div class="ca-content-text">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
								</div>
								<ul>
									<li><a href="#">Read more</a></li>
									<li><a href="#">Share this</a></li>
									<li><a href="#">Become a member</a></li>
									<li><a href="#">Donate</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="ca-item ca-item-8">
						<div class="ca-item-main">
							<div class="ca-icon"></div>
							<h3>Cooking Classes</h3>
							<h4>
								<span class="ca-quote">&ldquo;</span>
								<span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</span>
							</h4>
								<a href="#" class="ca-more">more...</a>
						</div>
						<div class="ca-content-wrapper">
							<div class="ca-content">
								<h6>Healthy Recipes</h6>
								<a href="#" class="ca-close">close</a>
								<div class="ca-content-text">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
								</div>
								<ul>
									<li><a href="#">Read more</a></li>
									<li><a href="#">Share this</a></li>
									<li><a href="#">Become a member</a></li>
									<li><a href="#">Donate</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script type="text/javascript" src="circular-js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="circular-js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>
			</div>
        </section>    
            
        


		 <section class="diatain_bg"  style="background-image:none;">
            <div class="wrap">
                <div class="row">
				<!--content-->
				<section class="content full-width">
					<div class="cwrap">
					<div class="w-divider6"><i class="colora fa fa-cutlery"></i><h3><span class="spr"  style="color:#F4716A;">Dietician Plan</span></h3></div>
						<!--entries-->
						<div class="entries">
							<!--featured recipe-->
						       <div class="entry entry one-fourth">


								<figure>
									<img alt="" src="images/img2.jpg">
									<figcaption><a href="blog_single.html"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
								</figure>
								<div class="container">
									<h2><a href="blog_single.html">Vegetarian Plan</a></h2> 
									<div class="excerpt">
										<p>If you want to keep your weight in check the healthy way by eating right and being active, then this is for you!</p>
									</div>
								</div>
						     </div>	
							 
							  <div class="entry entry one-fourth">
								<figure>
									<img alt="" src="images/img5.jpg">
									<figcaption><a href="blog_single.html"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
								</figure>
								<div class="container">
									<h2><a href="blog_single.html">Weight Loss</a></h2> 
									<div class="excerpt">
										<p>If you want to keep your weight in check the healthy way by eating right and being active, then this is for you!</p>
									</div>
								</div>
						     </div>	
							 
							  <div class="entry entry one-fourth">
								<figure>
									<img alt="" src="images/img7.jpg">
									<figcaption><a href="blog_single.html"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
								</figure>
								<div class="container">
									<h2><a href="blog_single.html">Athletes Diet Plan</a></h2> 
									<div class="excerpt">
										<p>If you want to keep your weight in check the healthy way by eating right and being active, then this is for you!</p>
									</div>
								</div>
						     </div>	
							 
							  <div class="entry entry one-fourth">
								<figure>
									<img alt="" src="images/img8.jpg">
									<figcaption><a href="blog_single.html"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
								</figure>
								<div class="container">
									<h2><a href="blog_single.html">Low Calorie Diet</a></h2> 
									<div class="excerpt">
										<p>If you want to keep your weight in check the healthy way by eating right and being active, then this is for you!</p>
									</div>
								</div>
						     </div>	
							<!--//featured member-->
						</div>
						<!--//entries-->
					</div>
				</section>
				<!--//content-->
			
			</div>	
            </div>
		</section>
		
		
		<!--//wrap-->
	

@endsection

