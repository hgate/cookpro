<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="keywords" content="SocialChef - Social Recipe HTML Template" />
	<meta name="description" content="SocialChef - Social Recipe HTML Template">
	<meta name="author" content="themeenergy.com">

	<title>@yield('title')</title>
    
    {{ Html::style('css/style.css') }}
    {{ Html::style('css/app.css') }}
    {{ Html::style('css/animate.css') }}
    {{ Html::style('http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800') }}

	<link rel="shortcut icon" href="images/favicon.ico" />
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<!--header-->
	@include('includes.header')

    @if(Route::getCurrentRoute()->getPath() === 'listrecipes') 
    	@include('recipes.includes.banner')
    @elseif((Route::getCurrentRoute()->getPath() === 'cooking-classes') || (Route::getCurrentRoute()->getPath() === 'cooking-catalog')|| (	Route::getCurrentRoute()->getPath() === 'cooking-membership'))
    	@include('cooking.includes.banner')
    @elseif(Route::getCurrentRoute()->getPath() === 'dietitianPlan')
        @include('dietitianPlan.includes.banner')
    @endif
    
    
	<!--main-->
	<main class="main" role="main" style="padding-bottom:10px;">
    	@yield('content')
    </main>
	<!--//main-->
    
    @include('includes.footer')

	<!--preloader-->
	<div class="preloader">
		<div class="spinner"></div>
	</div>
	<!--//preloader-->

	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	
    {{ Html::script("js/jquery-1.11.1.min.js") }}
    {{ Html::script("js/jquery.uniform.min.js") }}
    {{ Html::script("js/wow.min.js") }}
    {{ Html::script("js/jquery.slicknav.min.js") }}
    {{ Html::script("js/scripts.js") }}
    {{ Html::script("js/app.js") }}
	{{ Html::script("js/custom.js") }}
	<script>new WOW().init();</script>
	
</body>
</html>


