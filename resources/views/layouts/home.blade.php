<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="keywords" content="SocialChef - Social Recipe HTML Template" />
	<meta name="description" content="SocialChef - Social Recipe HTML Template">
	<meta name="author" content="themeenergy.com">

	<title>@yield('title')</title>
    
    {{ Html::style('css/bootstrap.min.css') }}
    {{ Html::style('css/style.css') }}
    {{ Html::style('css/animate.css') }}
    {{ Html::style('http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800') }}
	<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
    {{ Html::style('https://fonts.googleapis.com/css?family=Lobster') }}
    {{ Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css') }}
    {{ Html::style('css/component.css') }}
    {{ Html::style('css/circular.css') }}
    {{ Html::style('css/jquery.jscrollpane.css') }}
    {{ Html::style('css/theme.min.css') }}
        
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body class="home" style="background-color:#FDF9EE;">
	<!--header-->
	@include('includes.header')
    	
	<!--main-->
	<main class="main" role="main" style="padding-bottom:10px;">
    	@yield('content')
    </main>
	<!--//main-->
    
	<!--call to action-->
	<section class="catbox red_bg">
		<div class="wrap clearfix">
			<a href="login.html" class="button big white right">View Now</a>
			<h2>EXPERIENCE OUR DELICIOUS MENU!</h2>
		</div>
	</section>
	<!--//call to action-->
    
    @include('includes.footer')

	{{ Html::script("js/packages.min.js") }}
    {{ Html::script("js/theme.min.js") }}
    {{ Html::script("js/jquery-1.11.1.min.js") }}
    {{ Html::script("js/jquery.uniform.min.js") }}
    {{ Html::script("js/wow.min.js") }}
    {{ Html::script("js/scripts.js") }}
    {{ Html::script("js/jquery.slicknav.min.js") }}
    {{ Html::script("js/scripts.js") }}

	<script>
		window.dynamicNumbersBound = false;
		var wow = new WOW();
		WOW.prototype.show = function(box) {
			wow.applyStyle(box);
			if (typeof box.parentNode !== 'undefined' && hasClass(box.parentNode, 'dynamic-numbers') && !window.dynamicNumbersBound) {
				bindDynamicNumbers();
				window.dynamicNumbersBound = true;
			}
			return box.className = "" + box.className + " " + wow.config.animateClass;
		};
		wow.init();
		
		function hasClass(element, cls) {
			return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
		}
		
		function bindDynamicNumbers() {
			$('.dynamic-number').each(function() {				
				var startNumber = $(this).text();
				var endNumber = $(this).data('dnumber');
				var dynamicNumberControl = $(this);
				
				$({numberValue: startNumber}).animate({numberValue: endNumber}, {
					duration: 4000,
					easing: 'swing',
					step: function() { 
						$(dynamicNumberControl).text(Math.ceil(this.numberValue)); 
					}
				});
			});	
		}
		
		
		
	</script>

	
</body>
</html>


