@extends('layouts.master')

@section('content')

<div class="wrap clearfix">
    <!--row-->
    <div class="row">
        <!--content-->
        <section class="content center full-width wow fadeInUp">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
    
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}
                <div class="thememodal container">
                    <h3>Login</h3>

                    <div class="f-row">
                        {{-- select who are you ?? starts --}}
                        <select name="who">
                            <option value="user">User</option>
                            <option value="teacher">Teacher</option>
                            <option value="dietitian">Dietitian</option>
                        </select>
                    {{-- select who are you ?? ends --}}
                    </div>
                    
                    <div class="f-row">                    
                        <input type="email" placeholder="Your email" name="email" value="{{ old('email') }}">
    
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="f-row">
                        <input type="password" name="password" placeholder="Your password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div>
                    
                    <div class="f-row">
                        <input type="checkbox" name="remember" />
                        <label>Remember me next time</label>
                    </div>
                    
                    <div class="f-row bwrap">
                        <input type="submit" value="login" />
                    </div>
                    <p><a class="btn btn-link" href="{{ url('/password/reset') }}">Forgotten password?</a></p>
                    <p>Dont have an account yet? <a href="register.html">Sign up.</a></p>
                </div>
            </form>
        </section>
        <!--//content-->
    </div>
    <!--//row-->
</div>
@endsection
