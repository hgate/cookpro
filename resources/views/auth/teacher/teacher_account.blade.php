@extends('layouts.master')

@section('content')
		
	<!--main-->
	<main class="main" role="main">
		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
					<li>My account</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->
			
		
			<!--content-->
			<section class="content">
				<!--row-->
				<div class="row">
					<!--profile left part-->
					<div class="my_account one-fourth wow fadeInLeft">
						<figure>
							<img id="yieldProfilePic" src="@if(Auth::guard('teacher')->user()->profile_pic == '') {{ asset('images/avatar4.jpg') }} @else {{ asset('uploads/users/') }}/{{ Auth::guard('teacher')->user()->profile_pic }} @endif" height="270" width="270" alt="{{ Auth::guard('teacher')->user()->name }}" />	

							@if ($errors->has('profile_pic'))
								<span style="color:#FF8E88;">{{ $errors->first('profile_pic') }}</span>
							@endif						
						</figure>
						<div class="container">
							<h2 id="my-full-name">{{ Auth::guard('teacher')->user()->name }}</h2> 
							<form method="post" action="{{ url('teacher/myAccount/changeProfilePic') }}" enctype="multipart/form-data">
							{!! csrf_field() !!}
								<input type="file" name="profile_pic" id="upload_profile_pic">
								 <button class="upload_pic_btn">Upload</button> <a id="upload_image_click" href="">Upload Image</a>	
							</form>							
						</div>
						<div class="widget share">
	                        <ul class="boxed">
	                            <li class="light"><a href="#" title="Facebook"><i class="ico i-facebook"></i> <span>Share on Facebook</span></a></li>
	                            <li class="medium"><a href="#" title="Twitter"><i class="ico i-twitter"></i> <span>Share on Twitter</span></a></li>
	                            <li class="dark"><a href="#" title="Favourites"><i class="ico i-favourites"></i> <span>Add to Favourites</span></a></li>
	                        </ul>
	                    </div>
					</div>
					<!--//profile left part-->
					
					<div class="three-fourth wow fadeInRight">
						<nav class="tabs">
							<ul>
                            	<li class="active"><a href="#account" title="Account Settings">Account</a></li>
								<li><a href="#about" title="About me">About me</a></li>
							</ul>
						</nav>
						
                        <!--account settings-->
						<div class="tab-content" id="account">
							<div class="row">
								<dl class="basic two-third">
								    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									<dt>Name</dt>
									<dd> <span class="inline-edit-name">{{ Auth::guard('teacher')->user()->name }}</span> <span id="inline-edit-name" class="inline-edit">Edit</span>
									<span id="inline-update-name">Update</span>
									</dd>
									<dt>Username</dt>
									<dd>{{ Auth::guard('teacher')->user()->username }}</dd>
									<dt>Email</dt>
									<dd>{{ Auth::guard('teacher')->user()->email }}</dd>

									<div id="old-pwd-div">
									<dt>Old Password</dt>
									<dd> <span id="old-pwd"></span></dd>
									</div>

									<dt id="new-pass-content">Password</dt>
									<dd> <span id="new-pwd">******</span> <span id="inline-edit-pwd" class="inline-edit">Edit</span>
									<span id="inline-update-pwd" class="inline-edit">Update</span>
									</dd>								


									<dt>Delete Account</dt>
									<dd>
									<form method="POST" action="{{ url('teacher/myAccount/deleteAccount') }}">									
									 <input type="hidden" name="_token" value="{{ csrf_token() }}">
									<button onclick="return confirm('Are you sure ?');" class="inline-edit">Delete</button>
									</form>
									</dd>

									<div id="message-div">
									<dt id="msg-type"></dt>
									<dd id="display-msg-here" style="background: #fff; color:#444;"> </dd>
									</div>
									
								</dl>
								<div class="one-third">
									&nbsp;
								</div>
							</div>
						</div>
                        <!--//account settings-->
                        
						<!--about-->
						<div class="tab-content" id="about">
							<div class="row">
								<dl class="basic two-third">
									<dt>Name</dt>
									<dd>{{ Auth::guard('teacher')->user()->name }}</dd>
									<dt>Favorite cusine</dt>
									<dd>Thai, Mexican</dd>
                                    <dt>Favorite Chefs</dt>
									<dd>Thai, Mexican</dd>
                                    <dt>Favorite Dietitians</dt>
									<dd>Thai, Mexican</dd>
									<dt>Groups</dt>
									<dd>Blender, sharp knife</dd>
									<dt>Likes</dt>
									<dd>Chilli, Oregano, Basil</dd>
									<dt>Recipes submitted</dt>
									<dd>9</dd>
								</dl>
								
								<div class="one-third">
									&nbsp;
								</div>
							</div>
						</div>
						<!--//about-->

					</div>
				</div>
				<!--//row-->
			</section>
			<!--//content-->

			<!--row-->
        <div class="row">
    
            <!--content-->
            <section class="content three-fourth wow fadeInLeft">
                <!--one-half-->
                <section class="content">
                    <div class="row">
                        <div class="full-width wow fadeInUp">
                            <div class="container box">						
                                <nav class="tabs">
                                    <ul>
                                        <li style="width:33.3%;"><a href="#myCookingCourses" title="My recipes">My Cooking Courses</a></li>
                                        <li style="width:33.3%;"><a href="#addCookingCourse" title="My favorites">Add New Cooking Course</a></li>
                                        <li style="width:33.3%;"><a href="#myRecipes" title="My favorites">My Recipes</a></li>
                                    </ul>
                                </nav>
                                <div class="tab-content" id="myCookingCourses">
                                   <div class="entries row" style="padding: 15px 0;">
                                        <div style="clear:both;"></div>
                                
                                        <div class="quicklinks">
                                            <a href="#" class="button">More recipes</a>
                                            <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
                                        </div>
									</div>
                				</div>
                                <div class="tab-content" id="addCookingCourse">
                                    <div class="entries row" style="padding:20px;">
                                        <form method="POST" action="{{ url('teacher/addCookingCourse') }}" enctype="multipart/form-data">
                                         {!! csrf_field() !!}
                                            <section>
                                                <h2>Cooking Course</h2>
                                                <p>All fields are required.</p>
                                                <div class="f-row">
                                                    <div class="full"><input type="text" name="name" value="{{ old('name') }}" placeholder="Name of Cooking Course" /></div>
                                                </div>
                                            </section>
                         
                                            <section>
                                                <h2>Description</h2>
                                                <div class="f-row">
                                                    <div class="full"><textarea placeholder="Description of Cooking Course" name="description">{{ old('description') }}</textarea></div>
                                                </div>
                                            </section>

                                            <section>
                                                <h2>Photo</h2>
                                                <div class="f-row full">
                                                    <input name="cookingclassphoto" type="file" />
                                                </div>
                                            </section>
                                            
                                            <section>
                                                <h2>Time Period</h2>
                                                <div class="f-row">
                                                    <div class="full">
	                                                   	<select name="time_period">                            	
					                                        <option value="" selected="selected">Select Time Period</option> 
					                                        <option value="One week">One week</option>
					                                        <option value="Two week">Two week</option> 
					                                        <option value="Three week">Three week</option> 
					                                        <option value="Four week">Four week</option> 
					                                        <option value="One Month">One Month</option> 
					                                        <option value="Six Month">Six Month</option> 
					                                        <option value="One Year">One Year</option> 
							                            </select>
                                                    </div>
                                                </div>
                                            </section>

                                            <section>
                                                <h2>Course Type</h2>
                                                <div class="f-row">
                                                    <div class="full">
	                                                   	<select name="type">                            	
					                                        <option value="" selected="selected">Type of Course</option>
					                                        <option value="Begginer">Begginer</option>
					                                        <option value="Intermediate">Intermediate</option>
					                                        <option value="Advanced">Advanced</option> 
							                            </select>
                                                    </div>
                                                </div>
                                            </section>
                                            
                                            <div class="f-row full">
                                                <input type="submit" class="button" id="submitCookingClass" value="Submit Cooking Course" />
                                            </div>
                                        </form>
                                    </div>
                                </div>

								

                                <div class="tab-content" id="myRecipes">
							<div class="entries row">
							@forelse($recipes as $recipe)
								<!--item-->
								<div class="entry one-third">
									<figure>
										<img src="{{ asset('images/img6.jpg') }}" alt="" />
										<figcaption><a href="{{ url('recipe/'.$recipe->slug) }}"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
									</figure>
									<div class="container">
										<h2><a href="{{ url('recipe/'.$recipe->slug) }}">{{ $recipe->title }}</a></h2> 
										<div class="actions">
											<div>
												<div class="difficulty"><i class="ico i-medium"></i><a href="#">{{ $recipe->difficulty }}</a></div>
												<div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
												<div class="comments"><i class="ico i-comments"></i><a href="recipe.html#comments">27</a></div>
											</div>
										</div>
										<div class="actions">
                                                    <div>
                                                        <div class=""><a href="{{ url('recipes/'.$recipe->id.'/edit') }}">Edit</a></div>
                                                        {!! Form::open(['route' => ['recipes.destroy',$recipe->id],'method' => 'DELETE']) !!}
                                                        <div class=""><button id="delete_recipe" onclick="return confirm('You Really want to delete this recipe ?');" class="comment-reply-link">Delete</button></div>
                                                        {!! Form::close() !!}
                                                    </div>
                                        </div>
									</div>
								</div>
								<!--item-->

								@empty
								 <p>No Recipes so far</p>
							@endforelse								
								
							</div>
						</div>
            				</div>
        				</div>
    				</div>
    			</section>
                <!--//one-half-->
            </section>
            <!--//content-->
            
            <!--right sidebar-->
            <aside class="sidebar one-fourth wow fadeInRight">
                <div class="widget members wow fadeInRight" data-wow-delay=".4s">
						<h3>Members Who Join My Classes</h3>
						<div id="members-list-options" class="item-options">
						  <a href="#">Newest</a>
						  <a class="selected" href="#">Active</a>
						  <a href="#">Popular</a>
						</div>
						<ul class="boxed">
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar1.jpg') }}" alt="" /><span>Kimberly C.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar2.jpg') }}" alt="" /><span>Alex J.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar3.jpg') }}" alt="" /><span>Denise M.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar4.jpg') }}" alt="" /><span>Jason H.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar5.jpg') }}" alt="" /><span>Jennifer W.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar6.jpg') }}" alt="" /><span>Anabelle Q.</span></a></div></li>					
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar7.jpg') }}" alt="" /><span>Thomas M.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar8.jpg') }}" alt="" /><span>Michelle S.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar9.jpg') }}" alt="" /><span>Bryan A.</span></a></div></li>		
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar1.jpg') }}" alt="" /><span>Kimberly C.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar2.jpg') }}" alt="" /><span>Alex J.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar3.jpg') }}" alt="" /><span>Denise M.</span></a></div></li>
						</ul>
					</div>
            </aside>
            <!--//right sidebar-->
        </div>
        <!--//row-->
		</div>
		<!--//wrap-->
	</main>
	<!--//main-->
@endsection