@extends('layouts.master')

@section('content')
		
	<!--main-->
	<main class="main" role="main">
		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
					<li>My account</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->
			
		
			<!--content-->
			<section class="content">
				<!--row-->
				<div class="row">
					<!--profile left part-->
					<div class="my_account one-fourth wow fadeInLeft">
						<figure>
							<img id="yieldProfilePic" src="@if(Auth::user()->profile_pic == '') {{ asset('images/avatar4.jpg') }} @else uploads/users/{{ Auth::user()->profile_pic }} @endif" height="270" width="270" alt="{{ Auth::user()->name }}" />	

							@if ($errors->has('profile_pic'))
								<span style="color:#FF8E88;">{{ $errors->first('profile_pic') }}</span>
							@endif						
						</figure>
						<div class="container">
							<h2 id="my-full-name">{{ Auth::user()->name }}</h2> 
							<form method="post" action="{{ url('myAccount/changeProfilePic') }}" enctype="multipart/form-data">
							{!! csrf_field() !!}
								<input type="file" name="profile_pic" id="upload_profile_pic">
								 <button class="upload_pic_btn">Upload</button> <a id="upload_image_click" href="">Upload Image</a>	
							</form>							
						</div>
					</div>
					<!--//profile left part-->
					
					<div class="three-fourth wow fadeInRight">
						<nav class="tabs">
							<ul>
                            	<li class="active"><a href="#account" title="Account Settings">Account</a></li>
								<li><a href="#about" title="About me">About me</a></li>
								<li><a href="#recipes" title="My recipes">My recipes</a></li>
								<li><a href="#favorites" title="My favorites">My favorites</a></li>
								<li><a href="#posts" title="My posts">My posts</a></li>
							</ul>
						</nav>
						
                        <!--account settings-->
						<div class="tab-content" id="account">
							<div class="row">
								<dl class="basic two-third">
								    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									<dt>Name</dt>
									<dd> <span class="inline-edit-name">{{ Auth::user()->name }}</span> <span id="inline-edit-name" class="inline-edit">Edit</span>
									<span id="inline-update-name">Update</span>
									</dd>
									<dt>Username</dt>
									<dd>{{ Auth::user()->username }}</dd>
									<dt>Email</dt>
									<dd>{{ Auth::user()->email }}</dd>

									<div id="old-pwd-div">
									<dt>Old Password</dt>
									<dd> <span id="old-pwd"></span></dd>
									</div>

									<dt id="new-pass-content">Password</dt>
									<dd> <span id="new-pwd">******</span> <span id="inline-edit-pwd" class="inline-edit">Edit</span>
									<span id="inline-update-pwd" class="inline-edit">Update</span>
									</dd>								


									<dt>Delete Account</dt>
									<dd>
									<form method="POST" action="{{ url('myAccount/deleteAccount') }}">									
									 <input type="hidden" name="_token" value="{{ csrf_token() }}">
									<button onclick="return confirm('Are you sure ?');" class="inline-edit">Delete</button>
									</form>
									</dd>

									<div id="message-div">
									<dt id="msg-type"></dt>
									<dd id="display-msg-here" style="background: #fff; color:#444;"> </dd>
									</div>
									
								</dl>
								<span>asd</span>
								<div class="one-third">
									<ul class="boxed gold">
										<li class="light"><a href="#" title="Best recipe"><i class="ico i-had_best_recipe"></i> <span>Had a best recipe</span></a></li>
										<li class="medium"><a href="#" title="Was featured"><i class="ico i-was_featured"></i> <span>Was featured</span></a></li>
										<li class="dark"><a href="#" title="Added a first recipe"><i class="ico i-added_first_recipe"></i> <span>Added a first recipe</span></a></li>
										
										<li class="medium"><a href="#" title="Added 10-20 recipes"><i class="ico i-added_several_recipes"></i> <span>Added 10-20 recipes</span></a></li>
										<li class="dark"><a href="recipes.php" title="Events"><i class="ico i-wrote_blog_post"></i> <span>Wrote a blog post</span></a></li>
										<li class="light"><a href="recipes.php" title="Fish"><i class="ico i-wrote_comment"></i> <span>Wrote a comment</span></a></li>
										
										<li class="dark"><a href="recipes.php" title="Fish"><i class="ico i-won_contest"></i> <span>Won a contest</span></a></li>
										<li class="light"><a href="recipes.php" title="Healthy"><i class="ico i-shared_recipe"></i> <span>Shared a recipe</span></a></li>
										<li class="medium"><a href="#" title="Was featured"><i class="ico i-was_featured"></i> <span>Was featured</span></a></li>
									</ul>
								</div>
							</div>
						</div>
                        <!--//account settings-->
                        
						<!--about-->
						<div class="tab-content" id="about">
							<div class="row">
								<dl class="basic two-third">
									<dt>Name</dt>
									<dd>{{ Auth::user()->name }}</dd>
									<dt>Favorite cusine</dt>
									<dd>Thai, Mexican</dd>
                                    <dt>Favorite Chefs</dt>
									<dd>Thai, Mexican</dd>
                                    <dt>Favorite Dietitians</dt>
									<dd>Thai, Mexican</dd>
									<dt>Groups</dt>
									<dd>Blender, sharp knife</dd>
									<dt>Likes</dt>
									<dd>Chilli, Oregano, Basil</dd>
									<dt>Recipes submitted</dt>
									<dd>9</dd>
								</dl>
								
								<div class="one-third">
									<ul class="boxed gold">
										<li class="light"><a href="#" title="Best recipe"><i class="ico i-had_best_recipe"></i> <span>Had a best recipe</span></a></li>
										<li class="medium"><a href="#" title="Was featured"><i class="ico i-was_featured"></i> <span>Was featured</span></a></li>
										<li class="dark"><a href="#" title="Added a first recipe"><i class="ico i-added_first_recipe"></i> <span>Added a first recipe</span></a></li>
										
										<li class="medium"><a href="#" title="Added 10-20 recipes"><i class="ico i-added_several_recipes"></i> <span>Added 10-20 recipes</span></a></li>
										<li class="dark"><a href="recipes.php" title="Events"><i class="ico i-wrote_blog_post"></i> <span>Wrote a blog post</span></a></li>
										<li class="light"><a href="recipes.php" title="Fish"><i class="ico i-wrote_comment"></i> <span>Wrote a comment</span></a></li>
										
										<li class="dark"><a href="recipes.php" title="Fish"><i class="ico i-won_contest"></i> <span>Won a contest</span></a></li>
										<li class="light"><a href="recipes.php" title="Healthy"><i class="ico i-shared_recipe"></i> <span>Shared a recipe</span></a></li>
										<li class="medium"><a href="#" title="Was featured"><i class="ico i-was_featured"></i> <span>Was featured</span></a></li>
									</ul>
								</div>
							</div>
						</div>
						<!--//about-->
					
						<!--my recipes-->
						<div class="tab-content" id="recipes">
							<div class="entries row">
							@forelse($recipes as $recipe)
								<!--item-->
								<div class="entry one-third">
									<figure>
										<img src="images/img6.jpg" alt="" />
										<figcaption><a href="{{ url('recipe/'.$recipe->slug) }}"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
									</figure>
									<div class="container">
										<h2><a href="{{ url('recipe/'.$recipe->slug) }}">{{ $recipe->title }}</a></h2> 
										<div class="actions">
											<div>
												<div class="difficulty"><i class="ico i-medium"></i><a href="#">{{ $recipe->difficulty }}</a></div>
												<div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
												<div class="comments"><i class="ico i-comments"></i><a href="recipe.html#comments">27</a></div>
											</div>
										</div>
										<div class="actions">
                                                    <div>
                                                        <div class=""><a href="{{ url('recipes/'.$recipe->id.'/edit') }}">Edit</a></div>
                                                        {!! Form::open(['route' => ['recipes.destroy',$recipe->id],'method' => 'DELETE']) !!}
                                                        <div class=""><button id="delete_recipe" onclick="return confirm('You Really want to delete this recipe ?');" class="comment-reply-link">Delete</button></div>
                                                        {!! Form::close() !!}
                                                    </div>
                                        </div>
									</div>
								</div>
								<!--item-->

								@empty
								 <p>No Recipes so far</p>
							@endforelse								
								
							</div>
						</div>
						<!--//my recipes-->
						
						
						<!--my favorites-->
						<div class="tab-content" id="favorites">
							<div class="entries row">
								<!--item-->
								<div class="entry one-third">
									<figure>
										<img src="images/img6.jpg" alt="" />
										<figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
									</figure>
									<div class="container">
										<h2><a href="recipe.php">Thai fried rice with fruit and vegetables</a></h2> 
										<div class="actions">
											<div>
												<div class="difficulty"><i class="ico i-medium"></i><a href="#">medium</a></div>
												<div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
												<div class="comments"><i class="ico i-comments"></i><a href="recipe.html#comments">27</a></div>
											</div>
										</div>
									</div>
								</div>
								<!--item-->
								
								<!--item-->
								<div class="entry one-third">
									<figure>
										<img src="images/img5.jpg" alt="" />
										<figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
									</figure>
									<div class="container">
										<h2><a href="recipe.php">Spicy Morroccan prawns with cherry tomatoes</a></h2> 
										<div class="actions">
											<div>
												<div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
												<div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
												<div class="comments"><i class="ico i-comments"></i><a href="recipe.html#comments">27</a></div>
											</div>
										</div>
									</div>
								</div>
								<!--item-->
								
								<!--item-->
								<div class="entry one-third">
									<figure>
										<img src="images/img8.jpg" alt="" />
										<figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
									</figure>
									<div class="container">
										<h2><a href="recipe.php">Super easy blueberry cheesecake</a></h2> 
										<div class="actions">
											<div>
												<div class="difficulty"><i class="ico i-easy"></i><a href="#">easy</a></div>
												<div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
												<div class="comments"><i class="ico i-comments"></i><a href="recipe.html#comments">27</a></div>
											</div>
										</div>
									</div>
								</div>
								<!--item-->
							</div>
						</div>
						<!--//my favorites-->
						
						<!--my posts-->
						<div class="tab-content" id="posts">
							<!--entries-->
							<div class="entries row">
								<!--item-->
								<div class="entry one-third">
									<figure>
										<img src="images/img12.jpg" alt="" />
										<figcaption><a href="blog_single.php"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
									</figure>
									<div class="container">
										<h2><a href="blog_single.php">Barbeque party</a></h2> 
										<div class="actions">
											<div>
												<div class="date"><i class="ico i-date"></i><a href="#">22 Dec 2014</a></div>
												<div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
											</div>
										</div>
										<div class="excerpt">
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet . . . </p>
										</div>
									</div>
								</div>
								<!--item-->
								
								<!--item-->
								<div class="entry one-third">
									<figure>
										<img src="images/img11.jpg" alt="" />
										<figcaption><a href="blog_single.php"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
									</figure>
									<div class="container">
										<h2><a href="blog_single.php">How to make sushi</a></h2> 
										<div class="actions">
											<div>
												<div class="date"><i class="ico i-date"></i><a href="#">22 Dec 2014</a></div>
												<div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
											</div>
										</div>
										<div class="excerpt">
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet . . . </p>
										</div>
									</div>
								</div>
								<!--item-->
								
								<!--item-->
								<div class="entry one-third">
									<figure>
										<img src="images/img10.jpg" alt="" />
										<figcaption><a href="blog_single.php"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
									</figure>
									<div class="container">
										<h2><a href="blog_single.php">Make your own bread</a></h2> 
										<div class="actions">
											<div>
												<div class="date"><i class="ico i-date"></i><a href="#">22 Dec 2014</a></div>
												<div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
											</div>
										</div>
										<div class="excerpt">
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet . . . </p>
										</div>
									</div>
								</div>
								<!--item-->
							</div>
							<!--//entries-->
						</div>
						<!--//my posts-->
					</div>
				</div>
				<!--//row-->
			</section>
			<!--//content-->
		</div>
		<!--//wrap-->
	</main>
	<!--//main-->
@endsection