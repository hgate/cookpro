@extends('layouts.master')

@section('content')

<div class="wrap clearfix">
    <!--row-->
    <div class="row">

    @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
    @endif

    <!--content-->
        <section class="content center full-width wow fadeInUp">
            <div class="row">
                <div class="one-fifth wow fadeInLeft">
                </div>
                <div class="one-fifth wow fadeInLeft" data-wow-delay=".2s">
                    <header class="s-title">
                        <h2 class="ribbon bright"><a href="{{ url('/register') }}" title="Home" style="color:#fff;">Register as User</a></h2>
                    </header>
                </div>
                <div class="one-fifth wow fadeInLeft" data-wow-delay=".4s">
                    <header class="s-title">
                        <h2 class="ribbon bright"><a href="{{ url('/teacher/register') }}" title="Home" style="color:#fff;">Register as Chef</a></h2>
                    </header>
                </div>                    
                <div class="one-fifth wow fadeInLeft" data-wow-delay=".6s">
                    <header class="s-title">
                        <h2 class="ribbon bright"><a href="{{ url('/dietitian/register') }}" title="Home" style="color:#fff;">Register as Dietitian</a></h2>
                    </header>
                </div>
                <div class="one-fifth wow fadeInLeft" data-wow-delay=".8s">
                </div>
            </div>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/dietitian/register') }}">
         {!! csrf_field() !!}
            <div class="modal container">
                <h3>Register</h3>
                <div class="f-row">
                    <input type="text" placeholder="Your name" name="name" value="{{ old('name') }}">

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="f-row">
                    <input type="email" placeholder="Your email" name="email" value="{{ old('email') }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="f-row">
                    <input type="text" placeholder="Your username" name="username" value="{{ old('username') }}">

                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
                
                <div class="f-row">
                    <input type="password" placeholder="Your password" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="f-row">
                    <input type="password" placeholder="Retype password" name="password_confirmation">

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                
                <div class="f-row bwrap">
                    <input type="submit" value="register" />
                </div>
                <p>Already have an account yet? <a href="{{ url('/login') }}">Log in.</a></p>
            </div>
        </form>    
        </section>
        <!--//content-->
    </div>
    <!--//row-->
</div>
@endsection
