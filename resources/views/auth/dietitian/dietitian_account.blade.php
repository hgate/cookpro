@extends('layouts.master')

@section('content')
		
<!--main-->
<main class="main" role="main">
	<!--wrap-->
    <div class="wrap clearfix">
        <!--breadcrumbs-->
        <nav class="breadcrumbs">
            <ul>
                <li><a href="index-2.html" title="Home">Home</a></li>
                <li>Page</li>
            </ul>
        </nav>
        <?php
        //echo "<pre>";
        //print_r($dietitian);
        ?>
        <!--//breadcrumbs-->
        <section class="content">
			<!--row-->
            <div class="row">
                <!--profile left part-->
                <div class="my_account one-fourth wow fadeInLeft">
                    <figure>
                        <img id="yieldProfilePic" src="@if(Auth::guard('dietitian')->user()->profile_pic == '') {{ asset('images/avatar4.jpg') }} @else {{ asset('uploads/users/') }}/{{ Auth::guard('dietitian')->user()->profile_pic }} @endif" height="270" width="270" alt="{{ Auth::guard('dietitian')->user()->name }}" />	
    
                        @if ($errors->has('profile_pic'))
                            <span style="color:#FF8E88;">{{ $errors->first('profile_pic') }}</span>
                        @endif						
                    </figure>
                    <div class="container">
                        <h2 id="my-full-name">{{ Auth::guard('dietitian')->user()->name }}</h2> 
                        <form method="post" action="{{ url('dietitian/myAccount/changeProfilePic') }}" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                            <input type="file" name="profile_pic" id="upload_profile_pic">
                             <button class="upload_pic_btn">Upload</button> <a id="upload_image_click" href="">Upload Image</a>	
                        </form>							
                    </div>
                    <div class="widget share">
                        <ul class="boxed">
                            <li class="light"><a href="#" title="Facebook"><i class="ico i-facebook"></i> <span>Share on Facebook</span></a></li>
                            <li class="medium"><a href="#" title="Twitter"><i class="ico i-twitter"></i> <span>Share on Twitter</span></a></li>
                            <li class="dark"><a href="#" title="Favourites"><i class="ico i-favourites"></i> <span>Add to Favourites</span></a></li>
                        </ul>
                    </div>
                </div>
                <!--//profile left part-->
                
                <div class="three-fourth wow fadeInRight">
                    <nav class="tabs">
                        <ul>
                            <li class="active"><a href="#account" title="Account Settings">Account</a></li>
                            <li><a href="#about" title="About me">About me</a></li>								
                        </ul>
                    </nav>
                    
                    <!--account settings-->
                    <div class="tab-content" id="account">
                        <div class="row">
                            <dl class="basic two-third">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <dt>Name</dt>
                                <dd> <span class="inline-edit-name">{{ Auth::guard('dietitian')->user()->name }}</span> <span id="inline-edit-name" class="inline-edit">Edit</span>
                                <span id="inline-update-name">Update</span>
                                </dd>
                                <dt>Username</dt>
                                <dd>{{ Auth::guard('dietitian')->user()->username }}</dd>
                                <dt>Email</dt>
                                <dd>{{ Auth::guard('dietitian')->user()->email }}</dd>
    
                                <div id="old-pwd-div">
                                <dt>Old Password</dt>
                                <dd> <span id="old-pwd"></span></dd>
                                </div>
    
                                <dt id="new-pass-content">Password</dt>
                                <dd> <span id="new-pwd">******</span> <span id="inline-edit-pwd" class="inline-edit">Edit</span>
                                <span id="inline-update-pwd" class="inline-edit">Update</span>
                                </dd>								
    
    
                                <dt>Delete Account</dt>
                                <dd>
                                <form method="POST" action="{{ url('dietitian/myAccount/deleteAccount') }}">							
                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button onclick="return confirm('Are you sure ?');" class="inline-edit">Delete</button>
                                </form>
                                </dd>
    
                                <div id="message-div">
                                <dt id="msg-type"></dt>
                                <dd id="display-msg-here" style="background: #fff; color:#444;"> </dd>
                                </div>
                                
                            </dl>
                            
                            <div class="one-third">
                                
                            </div>
                        </div>
                    </div>
                    <!--//account settings-->
                    
                    <!--about-->
                    <div class="tab-content" id="about">
                        <div class="row">
                            <dl class="basic two-third">
                                <dt>Name</dt>
                                <dd>{{ Auth::guard('dietitian')->user()->name }}</dd>
                                <dt>Favorite cusine</dt>
                                <dd>Thai, Mexican</dd>
                                <dt>Favorite Chefs</dt>
                                <dd>Thai, Mexican</dd>
                                <dt>Favorite Dietitians</dt>
                                <dd>Thai, Mexican</dd>
                                <dt>Groups</dt>
                                <dd>Blender, sharp knife</dd>
                                <dt>Likes</dt>
                                <dd>Chilli, Oregano, Basil</dd>
                                <dt>Recipes submitted</dt>
                                <dd>9</dd>
                            </dl>
                            
                            <div class="one-third">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <!--//about-->
    
                </div>
            </div>
			<!--//row-->
		</section>
        
        <!--row-->
        <div class="row">
    
            <!--content-->
            <section class="content three-fourth wow fadeInLeft">
                <!--one-half-->
                <section class="content">
                    <div class="row">
                        <div class="full-width wow fadeInUp">
                            <div class="container box">						
                                <nav class="tabs">
                                    <ul>
                                        <li style="width:33.3%;"><a href="#myDietPlans" title="My recipes">My Diet Plans</a></li>
                                        <li style="width:33.3%;"><a href="#addDietPlan" title="My favorites">Add New Diet Plan</a></li>
                                        <li style="width:33.3%;"><a href="#myRecipes" title="My favorites">My Recipes</a></li>
                                    </ul>
                                </nav>
                                <div class="tab-content" id="myDietPlans">
                                   <div class="entries row" style="padding: 15px 0;">

                                        @forelse($dietplans as $key => $dietplan)           
                                        <!--item-->
                                        <div class="entry one-third wow fadeInLeft">
                                                <figure>
                                                    <img src="{{ asset('images/img6.jpg') }}" alt="" />
                                                    <figcaption><a href="{{ url('diet-plan') }}"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
                                                </figure>
                                                <div class="container">
                                                    <h2><a href="{{ url('diet-plan') }}">{{ $dietplan->name }}</a></h2> 
                                                    <div class="excerpt">
                                                        <p>{{ $dietplan->description }}</p>
                                                    </div>
                                                    <div class="actions">
                                                    <div>
                                                        <div class=""><a class="" href="recipes.php">Edit</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Delete</a></div>
                                                    </div>
                                                </div>
                                                </div>
                                        </div>
                                        <!--item-->
                                        @empty
                                        <p>No Diet Plan found</p>
                                        <!--item-->
                                        @endforelse
    
                                        <div style="clear:both;"></div>
                                
                                        <div class="quicklinks">
                                            <a href="#" class="button">More recipes</a>
                                            <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
                                        </div>
									</div>
                				</div>
                                <div class="tab-content" id="addDietPlan">
                                    <div class="entries row" style="padding:20px;">
                                        <form method="POST" action="{{ url('dietitian/addDietPlan') }}" enctype="multipart/form-data">
                                         {!! csrf_field() !!}
                                            <section>
                                                <h2>Diet Plan</h2>
                                                <p>All fields are required.</p>
                                                <div class="f-row">
                                                    <div class="full"><input type="text" name="name" value="{{ old('name') }}" placeholder="Name of Diet Plan" /></div>
                                                </div>
                                            </section>
                         
                                            <section>
                                                <h2>Description</h2>
                                                <div class="f-row">
                                                    <div class="full"><textarea placeholder="Description of Diet Plan" name="description">{{ old('description') }}</textarea></div>
                                                </div>
                                            </section>
                                            
                
                                            <!-- <section>
                                                <h2>Nutrition Information</h2>
                                                <div id="mainDivNutrition">
                                                          
                                                    <div class="f-row ingredient">
                                                        <div class="third">
                                                            {!! Form::select('nutrition[nutrition][]', array(
                                                                '' => 'Select a category',
                                                                'calories' => 'Calories',
                                                                'protien' => 'Protien',
                                                                'carbs' => 'Carbs',
                                                                'fat' => 'Fat',
                                                                'saturates' => 'Saturates',
                                                                'fibre' => 'Fibre',
                                                                'sugar' => 'Sugar',
                                                                'salt' => 'Salt'
                                                            )) !!}
                                                        </div>
                                                        <div class="third"><input type="text" id="inputNutrition1" name="nutrition[value][]"  value="" placeholder="Nutrition" /></div>
                                                        
                                                        
                                                        <button class="remove" id="removeNutrition">-</button>
                                                    </div>
                
                                                </div>
                                                <div class="f-row full">
                                                    <button class="add" type="button" onclick="adddietNutrition();">Add A Nutrition facts</button>
                                                </div>
                                            </section> -->
                                            <!-- <section>
                                                <h2>Photo</h2>
                                                <div class="f-row full">
                                                    <input name="dietplanphoto" type="file" />
                                                </div>
                                            </section> -->
                                            
                                            <section>
                                                <div class="f-row">
                                                    <div class="third"><input type="text" name="sun_breakfast" value="{{ old('sun_breakfast') }}" placeholder="Sunday Breakfast" /></div>
                                                    <div class="third"><input type="text" name="sun_lunch" value="{{ old('sun_lunch') }}" placeholder="Sunday Lunch" /></div>
                                                    <div class="third"><input type="text" name="sun_dinner" value="{{ old('sun_dinner') }}" placeholder="Sunday Dinner" /></div>
                                                </div>
                                            </section>
                        
                                            <section>
                                                <div class="f-row">
                                                    <div class="third"><input type="text" name="mon_breakfast" value="{{ old('mon_breakfast') }}" placeholder="Monday Breakfast" /></div>
                                                    <div class="third"><input type="text" name="mon_lunch" value="{{ old('mon_lunch') }}" placeholder="Monday Lunch" /></div>
                                                    <div class="third"><input type="text" name="mon_dinner" value="{{ old('mon_dinner') }}" placeholder="Monday Dinner" /></div>
                                                </div>
                                            </section>
                        
                                            <section>
                                                <div class="f-row">
                                                    <div class="third"><input type="text" name="tue_breakfast" value="{{ old('tue_breakfast') }}" placeholder="Tuesday Breakfast" /></div>
                                                    <div class="third"><input type="text" name="tue_lunch" value="{{ old('tue_lunch') }}" placeholder="Tuesday Lunch" /></div>
                                                    <div class="third"><input type="text" name="tue_dinner" value="{{ old('tue_dinner') }}" placeholder="Tuesday Dinner" /></div>
                                                </div>
                                            </section>
                        
                                            <section>
                                                <div class="f-row">
                                                    <div class="third"><input type="text" name="wed_breakfast" value="{{ old('wed_breakfast') }}" placeholder="Wednesday Breakfast" /></div>
                                                    <div class="third"><input type="text" name="wed_lunch" value="{{ old('wed_lunch') }}" placeholder="Wednesday Lunch" /></div>
                                                    <div class="third"><input type="text" name="wed_dinner" value="{{ old('wed_dinner') }}" placeholder="Wednesday Dinner" /></div>
                                                </div>
                                            </section>
                        
                                            <section>
                                                <div class="f-row">
                                                    <div class="third"><input type="text" name="thur_breakfast" value="{{ old('thur_breakfast') }}" placeholder="Thursday Breakfast" /></div>
                                                    <div class="third"><input type="text" name="thur_lunch" value="{{ old('thur_lunch') }}" placeholder="Thursday Lunch" /></div>
                                                    <div class="third"><input type="text" name="thur_dinner" value="{{ old('thur_dinner') }}" placeholder="Thursday Dinner" /></div>
                                                </div>
                                            </section>
                        
                                            <section>
                                                <div class="f-row">
                                                    <div class="third"><input type="text" name="fri_breakfast" value="{{ old('fri_breakfast') }}" placeholder="Friday Breakfast" /></div>
                                                    <div class="third"><input type="text" name="fri_lunch" value="{{ old('fri_lunch') }}" placeholder="Friday Lunch" /></div>
                                                    <div class="third"><input type="text" name="fri_dinner" value="{{ old('fri_dinner') }}" placeholder="Friday Dinner" /></div>
                                                </div>
                                            </section>
                        
                                            <section>
                                                <div class="f-row">
                                                    <div class="third"><input type="text" name="sat_breakfast" value="{{ old('sat_breakfast') }}" placeholder="saturday Breakfast" /></div>
                                                    <div class="third"><input type="text" name="sat_lunch" value="{{ old('sat_lunch') }}" placeholder="saturday Lunch" /></div>
                                                    <div class="third"><input type="text" name="sat_dinner" value="{{ old('sat_dinner') }}" placeholder="saturday Dinner" /></div>
                                                </div>
                                            </section>
                                            
                                            <div class="f-row full">
                                                <input type="submit" class="button" id="submitDietPlan" value="Submit Diet Plan" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-content" id="myRecipes">
                                   <div class="entries row" style="padding: 15px 0;">
          
                                        <!--item-->
                                        <div class="entry one-third wow fadeInLeft">
                                            <figure>
                                                <img src="{{ asset('images/img6.jpg') }}" alt="" />
                                                <figcaption><a href="{{ url('diet-plan') }}"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="{{ url('diet-plan') }}">My Recipe </a></h2> 
                                                <div class="excerpt">
                                                    <p>My Recipe Description</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
    
                                        <div style="clear:both;"></div>
                                
                                        <div class="quicklinks">
                                            <a href="#" class="button">More recipes</a>
                                            <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
                                        </div>
									</div>
                				</div>
            				</div>
        				</div>
    				</div>
    			</section>
                <!--//one-half-->
            </section>
            <!--//content-->
            
            <!--right sidebar-->
            <aside class="sidebar one-fourth wow fadeInRight">
                <div class="widget members wow fadeInRight" data-wow-delay=".4s">
						<h3>Members Who Join My Classes</h3>
						<div id="members-list-options" class="item-options">
						  <a href="#">Newest</a>
						  <a class="selected" href="#">Active</a>
						  <a href="#">Popular</a>
						</div>
						<ul class="boxed">
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar1.jpg') }}" alt="" /><span>Kimberly C.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar2.jpg') }}" alt="" /><span>Alex J.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar3.jpg') }}" alt="" /><span>Denise M.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar4.jpg') }}" alt="" /><span>Jason H.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar5.jpg') }}" alt="" /><span>Jennifer W.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar6.jpg') }}" alt="" /><span>Anabelle Q.</span></a></div></li>					
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar7.jpg') }}" alt="" /><span>Thomas M.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar8.jpg') }}" alt="" /><span>Michelle S.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar9.jpg') }}" alt="" /><span>Bryan A.</span></a></div></li>		
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar1.jpg') }}" alt="" /><span>Kimberly C.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar2.jpg') }}" alt="" /><span>Alex J.</span></a></div></li>
							<li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar3.jpg') }}" alt="" /><span>Denise M.</span></a></div></li>
						</ul>
					</div>
            </aside>
            <!--//right sidebar-->
        </div>
        <!--//row-->
    </div>
		<!--//wrap-->
        <script src="{{asset('js/addRow.js')}}"></script>
	</main>
	<!--//main-->

@endsection