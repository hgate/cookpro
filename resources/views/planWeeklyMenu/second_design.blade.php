@extends('layouts.master')

@section('title', 'Cookpro | Plan Your Weekly Menu')

@section('content')

		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
				  <li>On The Menu</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->
            <!--<section class="content full-width">
            
            	<div class="recipefinder ingredients">
                	<h3>Choose Diet Plan</h3>
					<a class="button gold" href="#">Get your personalized menu plan</a>
                    <a href="#" class="button gold">My Fav</a>
                    <a class="button gold" href="#">Weight Loss</a>
                    <a class="button gold" href="#">Detox Plan</a>
                    <a class="button gold" href="#">Low GI Plan</a>
                    <a class="button gold" href="#">Supefood Plan</a>
                    <a class="button gold" href="#">Low Carb Plan</a>
                    <a class="button gold" href="#">Flat Belly Plan</a>
                    <a class="button gold" href="#">Supefood Plan</a>
                    <a class="button gold" href="#">Active Lifestyle</a>
                    <a href="#" class="button gold">Healthy Recipes</a>
                    <a href="#" class="button gold">Veg Recipes</a>
                    <a href="#" class="button gold">Non- Veg</a>
                    <a href="#" class="button gold">High Protein</a>
                    <a href="#" class="button gold">Real Simple</a>
                    <a href="#" class="button gold">Parsley</a>
                    <a href="#" class="button gold">Tomato</a>
                    <a href="#" class="button gold">Olive oil</a>
				</div>
            </section> -->
            <section class="content full-width">
                	<div class="row">
                    	<div class="container recipefinder">
                        	<div class="row">
                            	<div class="one-third">
                                	<h2>Create Meal Plan</h2>
									<div class="f-row">
                                        <input type="text" placeholder="Name of Your Meal Plan" />
                                        <button class="add">+</button>
                                    	</div>
								</div>
								<div class="one-third">
                                	<h2>My Meal Plans</h2>
									<ul style="margin-top:30px;">
										<li>Vegetarian</li>
										<li>Summer Slimmer Plan</li>
										<li>Flat Belly Plan</li>
										<li>My customize meal plan1</li>
										<li>My customize meal plan2</li>
									</ul>
								</div>
								<div class="one-third">

								</div>
							</div>
							
                            <!--<div class="ingredients">
								<a href="#" class="button gold">My Fav</a><a href="#" class="button gold">Healthy Recipes</a><a href="#" class="button gold">Veg Recipes</a><a href="#" class="button gold">Non- Veg</a><a href="#" class="button gold">High Protein</a><a href="#" class="button gold">Real Simple</a><a href="#" class="button gold">Parsley</a><a href="#" class="button gold">Tomato</a><a href="#" class="button gold">Olive oil</a><a href="#" class="button gold">Parsley</a><a href="#" class="button gold">Tomato</a>
							</div>-->
						</div>
                    </div>
            </section>        
               
            <section class="content full-width">
            		<div class="row">
                    	<div class="ten-percent">
                        	<button class="add">< ></button>
                        </div>
                        <div class="eighty-percent">
                    	<header class="s-title wow fadeInDown" style="text-align:center;">
							<h2 class="ribbon large">April 24 - 30, 2016</h2>
						</header>
                        </div>
                        <div class="ten-percent">
                        </div>
                    </div>
                	<div class="row">
                        <div class="full-width">
                            <div class="container box">						
                                <div>				
							<div style="float:left;"><h2>Meal Plan Name here</h2></div>
                            <div class="f-row full">
							<a href="{{ url('shopping-list') }}"><input type="submit" class="button" id="submitRecipe" value="Add to Shopping List" style="float:right;" /></a>		</div>
                                <nav class="tabs">
                                    <ul>
                                        <li><a href="#tab1" title="Tab1">Sunday</a></li>
                                        <li><a href="#tab2" title="Tab 2">Monday</a></li>
                                        <li><a href="#tab3" title="Tab 3">Tuesday</a></li>
                                        <li><a href="#tab4" title="Tab 4">Wednesday</a></li>
                                        <li><a href="#tab4" title="Tab 4">Thursday</a></li>
                                        <li><a href="#tab4" title="Tab 4">Friday</a></li>
                                        <li><a href="#tab4" title="Tab 4">Saturday</a></li>
                                    </ul>
                                </nav>
                                <div class="tab-content" id="tab1">
                                <section class="content full-width">
									<!--entries-->
                                    <div class="entries row">
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Breakfast</h2>
                                            </div>
                                            <figure>
                                                <img src="images/food1.jpg" alt="" />
                                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="recipe.php">Refreshing banana split with a twist for adults</a></h2> 
                                                <div class="actions">
                                                    <div>
                                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div>
                                                        <div class=""><a href="#">Price : $35</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Edit</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Lunch</h2>
                                            </div>
                                            <figure>
                                                <img src="images/food2.jpg" alt="" />
                                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="recipe.php">Sushi mania: this is the best sushi you have ever tasted</a></h2> 
                                                <div class="actions">
                                                    <div>
                                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div>
                                                        <div class=""><a href="#">Price : $35</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Edit</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Dinner</h2>
                                            </div>
                                            <figure>
                                                <img src="images/food3.jpg" alt="" />
                                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="recipe.php">Princess puffs - an old classic at its best</a></h2> 
                                                <div class="actions">
                                                    <div>
                                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div>
                                                        <div class=""><a href="#">Price : $35</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Edit</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Snack & Desserts</h2>
                                            </div>
                                            <figure>
                                                <img src="images/food5.jpg" alt="" />
                                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="recipe.php">Princess puffs - an old classic at its best</a></h2> 
                                                <div class="actions">
                                                    <div>
                                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div>
                                                        <div class=""><a href="#">Price : $35</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Edit</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item--><!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>&nbsp;</h2>
                                            </div>
                                            <figure>
                                                <img src="images/img7.jpg" alt="" />
                                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="recipe.php">Princess puffs - an old classic at its best</a></h2> 
                                                <div class="actions">
                                                    <div>
                                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div>
                                                        <div class=""><a href="#">Price : $35</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Edit</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        
                                    </div>
                                    <!--//entries-->
                                </section>
                                
                                </div>
                                <div class="tab-content" id="tab2">
                                	<section class="content full-width">
									<!--entries-->
                                    <div class="entries row">
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Breakfast</h2>
                                            </div>
                                            <figure>
                                                <img src="images/food1.jpg" alt="" />
                                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="recipe.php">Refreshing banana split with a twist for adults</a></h2> 
                                                <div class="actions">
                                                    <div>
                                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div>
                                                        <div class=""><a href="#">Price : $35</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Edit</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Lunch</h2>
                                            </div>
                                            <figure>
                                                <img src="images/img7.jpg" alt="" />
                                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="recipe.php">Sushi mania: this is the best sushi you have ever tasted</a></h2> 
                                                <div class="actions">
                                                    <div>
                                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div>
                                                        <div class=""><a href="#">Price : $35</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Edit</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Dinner</h2>
                                            </div>
                                            <figure>
                                                <img src="images/img7.jpg" alt="" />
                                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="recipe.php">Princess puffs - an old classic at its best</a></h2> 
                                                <div class="actions">
                                                    <div>
                                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div>
                                                        <div class=""><a href="#">Price : $35</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Edit</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Snack & Desserts</h2>
                                            </div>
                                            <figure>
                                                <img src="images/img7.jpg" alt="" />
                                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="recipe.php">Princess puffs - an old classic at its best</a></h2> 
                                                <div class="actions">
                                                    <div>
                                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div>
                                                        <div class=""><a href="#">Price : $35</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Edit</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item--><!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>&nbsp;</h2>
                                            </div>
                                            <figure>
                                                <img src="images/img7.jpg" alt="" />
                                                <figcaption><a href="recipe.php"><i class="ico i-view"></i> <span>View recipe</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="recipe.php">Princess puffs - an old classic at its best</a></h2> 
                                                <div class="actions">
                                                    <div>
                                                        <div class="difficulty"><i class="ico i-hard"></i><a href="#">hard</a></div>
                                                        <div class="likes"><i class="ico i-likes"></i><a href="#">10</a></div>
                                                        <div class="comments"><i class="ico i-comments"></i><a href="blog_single.html#comments">27</a></div>
                                                    </div>
                                                </div>
                                                <div class="actions">
                                                    <div>
                                                        <div class=""><a href="#">Price : $35</a></div>
                                                        <div class=""><a class="comment-reply-link" href="recipes.php">Edit</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        
                                    </div>
                                    <!--//entries-->
                                </section>
                                </div>
                                <div class="tab-content" id="tab3"><h6>Tab3</h6> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.</p></div>
                                <div class="tab-content" id="tab4"><h6>Tab4</h6> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.</p></div>
                            </div>
                        </div>
                    </div>
                </section>
			<!--row-->

		</div>
		<!--//wrap-->

@endsection