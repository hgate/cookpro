@extends('layouts.master')

@section('title', 'Cookpro | Plan Your Weekly Menu')

@section('content')

        <!--wrap-->
        <div class="wrap clearfix">
            <!--breadcrumbs-->
            <nav class="breadcrumbs">
                <ul>
                    <li><a href="index-2.html" title="Home">Home</a></li>
                  <li>On The Menu</li>
                </ul>
            </nav>
            <!--//breadcrumbs-->

@if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
    @endif
            <section class="content full-width">
                <div class="row">
                    <div class="full-width">
                        <div class="container box">                     
                            <nav class="tabs">
                                <ul>
                                    <li style="width: 33.3%;"><a href="#tab1" title="Tab1">Create Meal Plan</a></li>
                                    <li style="width: 33.3%;"><a href="#tab2" title="Tab 2">My Meal Plans</a></li>
                                    <li style="width: 33.3%;"><a href="#tab3" title="Tab 3">Get Expert Dietitian Meal Plan</a></li>
                                </ul>
                            </nav>
                            <div class="tab-content" id="tab1">
                                <div class="container recipefinder">
                                    <div class="">
                                        <div class="f-row">
                                        {!! Form::open(array('url' => 'planWeeklyMenu/store', 'method'=>'post')) !!}
                                        <input type="text" name="meal_plan_name" placeholder="Name of Your Meal Plan" />
                                        <button type="submit" class="add">+</button>    
                                        {!! Form::close() !!}   
                                        </div>
                                    </div>
                                </div>        
                            </div>
                            <div class="tab-content" id="tab2">
                            
                                    <ul style="margin-top:30px;">
                                        @if($meal_plan)
                                        @foreach($meal_plan as $plan)
                                        <li><a href="{{URL('planWeeklyMenu',array('meal_plan_id'=>$plan->id))}}">{{$plan->meal_plan_name}}</a></li>
                                        @endforeach
                                        @endif
                                    </ul>
    
                            </div>
                            <div class="tab-content" id="tab3">
                                <div class="row">
                                <div style="margin-top:30px;">
                                <div class="one-third">
                                    <select>
                                        <option selected="selected">Select Diet Plans by Category</option>
                                        <option>Healthy Recipes</option>
                                        <option>Veg Recipes</option>
                                        <option>Non-Veg Recipes</option>
                                        <option>High Protein</option>
                                        <option>Real Simple</option>
                                        <option>Low card Recipes</option>      
                                    </select>
                                </div>
                                <div class="one-third">
                                    <select>
                                        <option selected="selected">Select Dietitican</option>
                                        <option>Anemia Diet Plan</option>
                                        <option>Bone & Joint Disease Diet Plan</option>
                                        <option>Cancer Diet Plan</option>
                                        <option>Cholesterol Diet Plan</option>
                                        <option>Diabetes Diet Plan</option>
                                        <option>Free Diet Plans</option>
                                        <option>Heart Healthy Diet Plan</option>
                                        <option>Infertility Diet Plan</option>
                                        <option>Kidney Disease Diet Plan</option>
                                        <option>Kids Diet Plan</option>
                                        <option>Lifestyle Disease Diet Plan</option>
                                        <option>Liver &amp; GI Disease Diet Plan</option>
                                        <option>PCOD/ PCOS Diet Plan</option>
                                        <option>Personalized Diet Plan</option>
                                        <option>Pregnancy &amp; Lactation Diet Plan</option>
                                        <option>Skin Care Diet Plan</option>
                                        <option>Thyroid Diet Plans</option>
                                        <option>Weight Gain Diet Plan</option>
                                        <option>Weight Loss Diet Plan</option>            
                                    </select>
                                </div>
                                <div class="one-third">
                                    <select>
                                        <option selected="selected">Select a Diet Plans</option>
                                        <option>Anemia Diet Plan</option>
                                        <option>Bone & Joint Disease Diet Plan</option>
                                        <option>Cancer Diet Plan</option>
                                        <option>Cholesterol Diet Plan</option>
                                        <option>Diabetes Diet Plan</option>
                                        <option>Free Diet Plans</option>
                                        <option>Heart Healthy Diet Plan</option>
                                        <option>Infertility Diet Plan</option>
                                        <option>Kidney Disease Diet Plan</option>
                                        <option>Kids Diet Plan</option>
                                        <option>Lifestyle Disease Diet Plan</option>
                                        <option>Liver &amp; GI Disease Diet Plan</option>
                                        <option>PCOD/ PCOS Diet Plan</option>
                                        <option>Personalized Diet Plan</option>
                                        <option>Pregnancy &amp; Lactation Diet Plan</option>
                                        <option>Skin Care Diet Plan</option>
                                        <option>Thyroid Diet Plans</option>
                                        <option>Weight Gain Diet Plan</option>
                                        <option>Weight Loss Diet Plan</option>            
                                    </select>
                                </div>
                                </div>
                            </div>                            
                          </div>
                        </div>
                    </div>
                </div>        
            </section>        
              @if(Auth::check())
            <section class="content full-width">
                    <div class="row">
                        <div class="ten-percent">
                            <button class="add">< ></button>
                        </div>
                        <div class="eighty-percent">
                        <header class="s-title wow fadeInDown" style="text-align:center;">
                            <h2 class="ribbon large">April 24 - 30, 2016</h2>
                        </header>
                        </div>
                        <div class="ten-percent">
                        </div>
                    </div>
                    <div class="row">
                    <div class="full-width wow fadeInUp">
                        <div class="container box">     
                            <div>               
                            <div style="float:left;"><h2>Meal Plan Name here</h2></div>
                            <div class="f-row full">
                            <a href="{{ url('shopping-list') }}"><input type="submit" class="button" id="submitRecipe" value="Add to Shopping List" style="float:right;" /></a>     </div>
                        </div>
                            <table>
                                <tr>
                                    <th>Sunday</th>
                                    <th>Monday</th>
                                    <th>Tuesday</th>
                                    <th>Wednesday</th>
                                    <th>Thursday</th>
                                    <th>Friday</th>
                                    <th>Saturday</th>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                </div>
                </section>
                @endif
            <!--row-->

        </div>
        <!--//wrap-->

@endsection