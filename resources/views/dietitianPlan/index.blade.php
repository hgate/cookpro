@extends('layouts.master')

@section('title', 'Cookpro | dietitian Plans')

@section('content')

		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
					<li>Online Diet Plans</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->
			<!--row-->
			<div class="row">
				<!--<header class="s-title wow fadeInLeft">
					<h1>Online Diet Plans</h1>
				</header>-->
		
				<!--content-->
				<section class="content three-fourth wow fadeInUp">
					<!--entries-->
					<div class="entries row">
						@forelse($dietplans as $key => $dietplan)
						<!--item-->
						<div class="entry one-third wow fadeInLeft">
							<figure>
								<img src="{{ asset('images/img2.jpg') }}" alt="" />
								<figcaption><a href="{{ url('dietitian/diet-plan/'.$dietplan->slug) }}"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
							</figure>
							<div class="container">
								<h2><a href="{{ url('dietitian/diet-plan/'.$dietplan->slug) }}">{{ $dietplan->name }}</a></h2> 
								<div class="excerpt">
									<p>{{ $dietplan->description }}</p>
								</div>
							</div>
						</div>
						<!--item-->
						@empty
                        	<p>No recipes found</p>
                    	@endforelse

						<div class="quicklinks">
							<a href="#" class="button">More recipes</a>
							<a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
						</div>
					</div>
					<!--//entries-->
				</section>
                <aside class="sidebar one-fourth wow fadeInLeft">
					 <header class="s-title">
                        <h2 class="ribbon bright"><a href="choose_goal.php" title="Home" style="color:#fff;">Choose Goal</a></h2>
                    </header>
                    <div class="widget members wow fadeInLeft" data-wow-delay=".4s">
						<h3>Diet Plan by Categories</h3>
						<ul class="boxed">
							<li class="light"><a href="recipes.php" title="Appetizers"><i class="ico i-appetizers"></i> <span>Vegetarian</span></a></li>
							<li class="medium"><a href="recipes.php" title="Cocktails"><i class="ico i-cocktails"></i> <span>Weight Loss</span></a></li>
							<li class="dark"><a href="recipes.php" title="Deserts"><i class="ico i-deserts"></i> <span>Active Lifestyle</span></a></li>
							
							<li class="medium"><a href="recipes.php" title="Cocktails"><i class="ico i-eggs"></i> <span>Healthy Pregnancy</span></a></li>
							<li class="dark"><a href="recipes.php" title="Equipment"><i class="ico i-equipment"></i> <span>Detox Plan</span></a></li>
							<li class="light"><a href="recipes.php" title="Events"><i class="ico i-events"></i> <span>Low GI Plan</span></a></li>
						
							<li class="dark"><a href="recipes.php" title="Fish"><i class="ico i-fish"></i> <span>Supefood Plan</span></a></li>
							<li class="light"><a href="recipes.php" title="Ftness"><i class="ico i-fitness"></i> <span>Low Carb Plan</span></a></li>
							<li class="medium"><a href="recipes.php" title="Healthy"><i class="ico i-healthy"></i> <span>Flat Belly Plan</span></a></li>

						</ul>
					</div>
					<div class="widget members">
						<h3>Get a Expert Consultation</h3>
						<div id="members-list-options" class="item-options">
						  <a href="#">Newest</a>
						  <a class="selected" href="#">Active</a>
						  <a href="#">Popular</a>
						</div>
						<ul class="boxed">
							<li><div class="avatar"><a href="{{ url('dietitian/dietitian-profile/1') }}"><img src="{{ asset('images/avatar1.jpg') }}" alt="" /><span>Kimberly C.</span></a></div></li>
							<li><div class="avatar"><a href="{{ url('dietitian/dietitian-profile/2') }}"><img src="{{ asset('images/avatar2.jpg') }}" alt="" /><span>Alex J.</span></a></div></li>
							<li><div class="avatar"><a href="{{ url('dietitian/dietitian-profile') }}"><img src="{{ asset('images/avatar3.jpg') }}" alt="" /><span>Denise M.</span></a></div></li>
							<li><div class="avatar"><a href="{{ url('dietitian/dietitian-profile') }}"><img src="{{ asset('images/avatar4.jpg') }}" alt="" /><span>Jason H.</span></a></div></li>
							<li><div class="avatar"><a href="{{ url('dietitian/dietitian-profile') }}"><img src="{{ asset('images/avatar5.jpg') }}" alt="" /><span>Jennifer W.</span></a></div></li>
							<li><div class="avatar"><a href="{{ url('dietitian/dietitian-profile') }}"><img src="{{ asset('images/avatar6.jpg') }}" alt="" /><span>Anabelle Q.</span></a></div></li>
							<li><div class="avatar"><a href="{{ url('dietitian/dietitian-profile') }}"><img src="{{ asset('images/avatar7.jpg') }}" alt="" /><span>Thomas M.</span></a></div></li>
							<li><div class="avatar"><a href="{{ url('dietitian/dietitian-profile') }}"><img src="{{ asset('images/avatar8.jpg') }}" alt="" /><span>Michelle S.</span></a></div></li>
							<li><div class="avatar"><a href="{{ url('dietitian/dietitian-profile') }}"><img src="{{ asset('images/avatar9.jpg') }}" alt="" /><span>Bryan A.</span></a></div></li>
						</ul>
					</div>
                    
                    <div class="widget wow fadeInLeft" data-wow-delay=".6s">
						<h3>Advertisment</h3>
						<a href="#"><img src="{{ asset('images/advertisment.jpg') }}" alt="" /></a>
					</div>
				</aside>
				<!--//content-->
			</div>
			<!--//row-->
		</div>
		<!--//wrap-->

@endsection