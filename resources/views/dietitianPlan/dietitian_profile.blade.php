@extends('layouts.master')

@section('title', 'Cookpro | Submit Your Recipe Here')

@section('content')
        
<!--main-->
<main class="main" role="main">
    <!--wrap-->
    <div class="wrap clearfix">
        <!--breadcrumbs-->
        <nav class="breadcrumbs">
            <ul>
                <li><a href="index-2.html" title="Home">Home</a></li>
                <li>Page</li>
            </ul>
        </nav>
        <?php
        //echo "<pre>";
        //print_r($profile);
        ?>
        <!--//breadcrumbs-->
        <section class="content">
            <!--row-->
            <div class="row">
                <!--profile left part-->
                <div class="my_account one-fourth wow fadeInLeft">
                    <figure>
                        <img src="{{ asset('images/avatar4.jpg') }}" height="270" width="270" alt="" />    
                    </figure>
                    <div class="container">
                        <h2 id="my-full-name">{{ $profile->name }}</h2> 
                    </div>
                    <div class="widget share">
                        <ul class="boxed">
                            <li class="light"><a href="#" title="Facebook"><i class="ico i-facebook"></i> <span>Share on Facebook</span></a></li>
                            <li class="medium"><a href="#" title="Twitter"><i class="ico i-twitter"></i> <span>Share on Twitter</span></a></li>
                            <li class="dark"><a href="#" title="Favourites"><i class="ico i-favourites"></i> <span>Add to Favourites</span></a></li>
                        </ul>
                    </div>
                </div>
                <!--//profile left part-->
                
                <div class="three-fourth wow fadeInRight">
                    <nav class="tabs">
                        <ul>
                            <li><a href="#about" title="About me">About me</a></li>                             
                        </ul>
                    </nav>
                    
                    <!--about-->
                    <div class="tab-content" id="about">
                        <div class="row">
                            <dl class="basic two-third">
                                <dt>Name</dt>
                                <dd>{{ $profile->name }}</dd>
                                <dt>Favorite cusine</dt>
                                <dd>Thai, Mexican</dd>
                                <dt>Favorite Chefs</dt>
                                <dd>Thai, Mexican</dd>
                                <dt>Favorite Dietitians</dt>
                                <dd>Thai, Mexican</dd>
                                <dt>Groups</dt>
                                <dd>Blender, sharp knife</dd>
                                <dt>Likes</dt>
                                <dd>Chilli, Oregano, Basil</dd>
                                <dt>Recipes submitted</dt>
                                <dd>9</dd>
                            </dl>
                            
                            <div class="one-third">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <!--//about-->
    
                </div>
            </div>
            <!--//row-->
        </section>
        
        <!--row-->
        <div class="row">
    
            <!--content-->
            <section class="content three-fourth wow fadeInLeft">
                <!--one-half-->
                <section class="content">
                    <div class="row">
                        <div class="full-width wow fadeInUp">
                            <div class="container box">                     
                                <nav class="tabs">
                                    <ul>
                                        <li style="width:33.3%;"><a href="#myDietPlans" title="My recipes">My Diet Plans</a></li>
                                        <li style="width:33.3%;"><a href="#myRecipes" title="My favorites">My Recipes</a></li>
                                    </ul>
                                </nav>
                                <div class="tab-content" id="myDietPlans">
                                   <div class="entries row" style="padding: 15px 0;">

                                        <div style="clear:both;"></div>
                                
                                        <div class="quicklinks">
                                            <a href="#" class="button">More recipes</a>
                                            <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content" id="myRecipes">
                                   <div class="entries row" style="padding: 15px 0;">
          
                                        <!--item-->
                                        <div class="entry one-third wow fadeInLeft">
                                            <figure>
                                                <img src="{{ asset('images/img6.jpg') }}" alt="" />
                                                <figcaption><a href="{{ url('diet-plan') }}"><i class="ico i-view"></i> <span>View post</span></a></figcaption>
                                            </figure>
                                            <div class="container">
                                                <h2><a href="{{ url('diet-plan') }}">My Recipe </a></h2> 
                                                <div class="excerpt">
                                                    <p>My Recipe Description</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
    
                                        <div style="clear:both;"></div>
                                
                                        <div class="quicklinks">
                                            <a href="#" class="button">More recipes</a>
                                            <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--//one-half-->
            </section>
            <!--//content-->
            
            <!--right sidebar-->
            <aside class="sidebar one-fourth wow fadeInRight">
                <div class="widget members wow fadeInRight" data-wow-delay=".4s">
                        <h3>Members Who Join My Classes</h3>
                        <div id="members-list-options" class="item-options">
                          <a href="#">Newest</a>
                          <a class="selected" href="#">Active</a>
                          <a href="#">Popular</a>
                        </div>
                        <ul class="boxed">
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar1.jpg') }}" alt="" /><span>Kimberly C.</span></a></div></li>
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar2.jpg') }}" alt="" /><span>Alex J.</span></a></div></li>
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar3.jpg') }}" alt="" /><span>Denise M.</span></a></div></li>
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar4.jpg') }}" alt="" /><span>Jason H.</span></a></div></li>
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar5.jpg') }}" alt="" /><span>Jennifer W.</span></a></div></li>
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar6.jpg') }}" alt="" /><span>Anabelle Q.</span></a></div></li>                  
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar7.jpg') }}" alt="" /><span>Thomas M.</span></a></div></li>
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar8.jpg') }}" alt="" /><span>Michelle S.</span></a></div></li>
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar9.jpg') }}" alt="" /><span>Bryan A.</span></a></div></li>     
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar1.jpg') }}" alt="" /><span>Kimberly C.</span></a></div></li>
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar2.jpg') }}" alt="" /><span>Alex J.</span></a></div></li>
                            <li><div class="avatar"><a href="instructor_profile.php"><img src="{{ asset('images/avatar3.jpg') }}" alt="" /><span>Denise M.</span></a></div></li>
                        </ul>
                    </div>
            </aside>
            <!--//right sidebar-->
        </div>
        <!--//row-->
    </div>
        <!--//wrap-->
        <script src="{{asset('js/addRow.js')}}"></script>
    </main>
    <!--//main-->

@endsection