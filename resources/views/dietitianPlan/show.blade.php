@extends('layouts.master')

@section('title', 'Cookpro | dietitian Plans')

@section('content')

        <!--wrap-->
        <div class="wrap clearfix">
            <!--breadcrumbs-->
            <nav class="breadcrumbs">
                <ul>
                    <li><a href="index-2.html" title="Home">Home</a></li>
                    <li>Online Diet Plans</li>
                </ul>
            </nav>
            <!--//breadcrumbs-->
            <?php
           /* echo "<pre>";
            print_r($dietplan);*/
            ?>
            <!--row-->
            <div class="row">
                
                <section class="content full-width wow fadeInUp">
                    <!--one-half-->
                        <div class="f-row full">
                        <input type="submit" class="button" id="submitRecipe" value="Personalize this Plan" style="float:right;" />
                        </div>
                    <section class="container">
                        
                        <h1>{{ $dietplan->name }}</h1>
                        <p>{{ $dietplan->description }}</p>
                    </section>
                    
                </section>
                <section class="content full-width wow fadeInUp">
                    <!--<div class="row">
                                <div style="float:left;">
                                    View Menu | Vegetatian | 
                                </div>
                                <div style="float:left;">
                                    <select>
                                        <option selected="selected">1200</option>
                                        <option>1600</option>
                                        <option>2000</option>
                                        <option>2500</option>           
                                    </select>
                                </div>    
                                <div style="float:left;">    
                                     Calories | Duration of Plan
                                </div>     
                                <div style="float:left;">
                                    <select>
                                        <option selected="selected">Three Day</option>
                                        <option>Four Day</option>
                                        <option>Five Day</option>
                                        <option>Seven Day</option>         
                                    </select>
                                </div>
                    </div>-->
                    
                </section>
                <div class="full-width wow fadeInUp">
                        <div class="container box">                     
                            <h2>Nutritional Information</h2>
                            <table>
                                <tr>
                                    <th>Calories</th>
                                    <th>of which: Mono-unsaturates</th>
                                    <th>of which: Sugars</th>
                                    <th>Sodium</th>
                                </tr>
                                <tr>
                                    <td>1,511kCal</td>
                                    <td>14.1g</td>
                                    <td>101.3g</td>
                                    <td>1,841.1mg</td>
                                </tr>
                                <tr>
                                    <th>Fat</th>
                                    <th>of which: Poly-unsaturates </th>
                                    <th>Fibre</th>
                                    <th>Salt</th>
                                </tr>
                                <tr>
                                    <td>36.6g</td>
                                    <td>7.7g</td>
                                    <td>8.7g</td>
                                    <td>3,280.7mg</td>
                                </tr>
                                <tr>
                                    <th>of which: Saturates</th>
                                    <th>Carbohydrates</th>
                                    <th>Protein</th>
                                    <th>Zinc</th>
                                </tr>
                                <tr>
                                    <td>14.8g</td>
                                    <td>220.0g</td>
                                    <td>54.7g</td>
                                    <td>5.9mg</td>
                                </tr>
                            </table>
                        </div>
                    </div>        
                
                <section class="content full-width">
                    <div class="row">
                        <div class="full-width">
                            <div class="container box">                     
                                <h2>Your Weekly Plan of April 11</h2>
                                <nav class="tabs">
                                    <ul>
                                        <li><a href="#tab1" title="Tab1">Sunday</a></li>
                                        <li><a href="#tab2" title="Tab 2">Monday</a></li>
                                        <li><a href="#tab3" title="Tab 3">Tuesday</a></li>
                                        <li><a href="#tab4" title="Tab 4">Wednesday</a></li>
                                        <li><a href="#tab5" title="Tab 4">Thursday</a></li>
                                        <li><a href="#tab6" title="Tab 4">Friday</a></li>
                                        <li><a href="#tab7" title="Tab 4">Saturday</a></li>
                                    </ul>
                                </nav>
                                <div class="tab-content" id="tab1">
                                    <section class="content full-width">
                                    <!--entries-->
                                    <div class="entries row">
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Breakfast</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->sun_breakfast }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Lunch</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->sun_lunch }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Dinner</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->sun_dinner }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Snack & Desserts</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->

                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>&nbsp;</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                    </div>
                                    <!--//entries-->
                                    </section>
                                
                                </div>
                                <div class="tab-content" id="tab2">
                                    <section class="content full-width">
                                    <!--entries-->
                                    <div class="entries row">
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Breakfast</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->mon_breakfast }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Lunch</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->mon_lunch }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Dinner</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->mon_dinner }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Snack & Desserts</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->

                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>&nbsp;</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                    </div>
                                    <!--//entries-->
                                    </section>
                                </div>
                                <div class="tab-content" id="tab3">
                                    <section class="content full-width">
                                    <!--entries-->
                                    <div class="entries row">
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Breakfast</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->tue_breakfast }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Lunch</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->tue_lunch }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Dinner</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->tue_dinner }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Snack & Desserts</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->

                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>&nbsp;</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                    </div>
                                    <!--//entries-->
                                    </section>
                                </div>
                                <div class="tab-content" id="tab4">
                                    <section class="content full-width">
                                    <!--entries-->
                                    <div class="entries row">
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Breakfast</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->wed_breakfast }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Lunch</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->wed_lunch }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Dinner</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->wed_dinner }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Snack & Desserts</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->

                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>&nbsp;</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                    </div>
                                    <!--//entries-->
                                    </section> 
                                </div>
                                <div class="tab-content" id="tab5">
                                    <section class="content full-width">
                                    <!--entries-->
                                    <div class="entries row">
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Breakfast</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->thur_breakfast }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Lunch</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->thur_lunch }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Dinner</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->thur_dinner }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Snack & Desserts</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->

                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>&nbsp;</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                    </div>
                                    <!--//entries-->
                                    </section>
                                </div>
                                <div class="tab-content" id="tab6">
                                    <section class="content full-width">
                                    <!--entries-->
                                    <div class="entries row">
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Breakfast</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->fri_breakfast }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Lunch</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->fri_lunch }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Dinner</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->fri_dinner }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Snack & Desserts</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->

                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>&nbsp;</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                    </div>
                                    <!--//entries-->
                                    </section>
                                </div>
                                <div class="tab-content" id="tab7">
                                    <section class="content full-width">
                                    <!--entries-->
                                    <div class="entries row">
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Breakfast</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->sat_breakfast }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Lunch</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->sat_lunch }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Dinner</h2>
                                            </div>
                                            <dl class="basic">
                                                <dd>{{ $dietplan->sat_dinner }}</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>Snack & Desserts</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->

                                        <!--item-->
                                        <div class="entry one-third">
                                            <div class="tab-content-heading">
                                                <h2>&nbsp;</h2>
                                            </div>
                                            <dl class="basic">
                                                <dt>One hard boiled egg</dt>
                                                <dd>cal 85/ pro7/ carb 0/ fat 6</dd>
                                                <dt>One half grapefruit</dt>
                                                <dd>cal 52/ pro1/ carb 13/ fat 0</dd>
                                                <dt>One whole toast</dt>
                                                <dd>cal 69/ pro 3/ carb13/ fat 1</dd>
                                            </dl>
                                        </div>
                                        <!--item-->
                                        
                                    </div>
                                    <!--//entries-->
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!--//row-->
        </div>
        <!--//wrap-->

@endsection