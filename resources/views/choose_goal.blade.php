@extends('layouts.master')

@section('title', 'Cookpro | Search For Recipe')

@section('content') 
	<div class="full-images-bg dietain">
        <div class="wrap clearfix">
            <div class="banner-txt text-center">
            	<h3>We Have Best Dietitians Here</h3>
            </div>
            <div class="container recipefinder"> 
                <div class="row">
                    <div class="one-third">
                        <select>
                            <option selected="selected">Select a Diet Plans</option>
                            <option>Anemia Diet Plan</option>
                            <option>Bone & Joint Disease Diet Plan</option>
                            <option>Cancer Diet Plan</option>
                            <option>Cholesterol Diet Plan</option>
                            <option>Diabetes Diet Plan</option>
                            <option>Free Diet Plans</option>
                            <option>Heart Healthy Diet Plan</option>
                            <option>Infertility Diet Plan</option>
                            <option>Kidney Disease Diet Plan</option>
                            <option>Kids Diet Plan</option>
                            <option>Lifestyle Disease Diet Plan</option>
                            <option>Liver & GI Disease Diet Plan</option>
                            <option>PCOD/ PCOS Diet Plan</option>
                            <option>Personalized Diet Plan</option>
                            <option>Pregnancy & Lactation Diet Plan</option>
                            <option>Skin Care Diet Plan</option>
                            <option>Thyroid Diet Plans</option>
                            <option>Weight Gain Diet Plan</option>
                            <option>Weight Loss Diet Plan</option>            
                        </select>
                    </div>
                    <div class="one-third">
                        <select>
                            <option selected="selected">Type of Consultation</option>
                            <option>Indian diet plan</option>
                            <option>Email Consultation</option>
                            <option>Free Diet Plan</option>
                            <option>In Clinic Consultation</option>
                            <option>Onsite Consultation</option>
                            <option>Paid Diet Plan</option>
                            <option>Personalised: Daily follow up</option>
                            <option>Skype Call</option>
                            <option>Standard: Weekly followup</option>
                            <option>Telephonic Consultation</option>
                            <option>Whats app consultation</option>          
                        </select>
                    </div>
                    <div class="one-third">
                        <select>
                            <option selected="selected">Duration</option>
                            <option>14 days</option>
                            <option>20 mins</option>
                            <option>30 mins</option>
                            <option>7 Days</option>
                            <option>One month</option>
                            <option>One Time</option>
                            <option>Three months</option>        
                        </select>
                    </div>
                     <div class="one-third submit">
                        <input type="submit" value="Submit">
                    </div>
                </div>
            </div>
		</div>
	</div>	
	<!--main-->
	<main class="main" role="main">
		<!--wrap-->
		<div class="wrap clearfix">
			<!--breadcrumbs-->
			<nav class="breadcrumbs">
				<ul>
					<li><a href="index-2.html" title="Home">Home</a></li>
					<li>Page</li>
				</ul>
			</nav>
			<!--//breadcrumbs-->

			<!--row-->
			<div class="row">
				<header class="s-title wow fadeInLeft">
					<h1>Page with a left sidebar</h1>
				</header>
				
				<!--content-->
				<section class="content three-fourth wow fadeInRight">
					<section class="container">
                    							
                        <h2>Step 1 :  Choose a goal</h2>
                        <h3>This information will help us offer you a personalized menu plan that is sure to please!</h3>
                        <div class="f-row full">
                            <input type="radio" id="r1" name="radio"/>
                            <label for="r1">I am still working on it</label>
                        </div>
                        <div class="f-row full">
                            <input type="radio" id="r2" name="radio"/>
                            <label for="r2">I am ready to publish this recipe</label>
                        </div>
						
					</section>

					<section class="container">
                    							
                        <h2>Step 1 :  Choose a goal</h2>
                        <h3>This information will help us offer you a personalized menu plan that is sure to please!</h3>
                        <div class="f-row full">
                            <input type="radio" id="r1" name="radio"/>
                            <label for="r1">I am still working on it</label>
                        </div>
                        <div class="f-row full">
                            <input type="radio" id="r2" name="radio"/>
                            <label for="r2">I am ready to publish this recipe</label>
                        </div>
						
					</section>
				</section>
                <aside class="sidebar one-fourth wow fadeInLeft">
                	<div data-wow-delay=".2s" class="widget wow fadeInRight animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInRight;">
						<h3>Your Personal Nutritionist</h3>
						<ul class="articles_latest">
							<li>
								<a href="blog_single.php">
									<img alt="" src="images/img9.jpg">
									<h6>Lose weight</h6>
								</a>
							</li>
							<li>
								<a href="blog_single.php">
									<img alt="" src="images/img10.jpg">
									<h6>Reduce cholesterol</h6>
								</a>
							</li>
							<li>
								<a href="blog_single.php">
									<img alt="" src="images/img11.jpg">
									<h6>Increase exercise</h6>
								</a>
							</li>
							<li>
								<a href="blog_single.php">
									<img alt="" src="images/img12.jpg">
									<h6>Manage diabetes</h6>
								</a>
							</li>
						</ul>
					</div>
					<div class="widget">
						<ul class="categories left">
							<li><a href="#">All recipes</a></li>
							<li class="active"><a href="#">Tips and Tricks</a></li>
							<li><a href="#">Events</a></li>
							<li><a href="#">Inspiration</a></li>
							<li><a href="#">Category name</a></li>
							<li><a href="#">Lorem ipsum</a></li>
							<li><a href="#">Dolor</a></li>
							<li><a href="#">Sit amet</a></li>
						</ul>
					</div>
					
					<div class="widget">
						<h3>Search</h3>
						<div class="f-row">
							<input type="search" placeholder="Enter your search term" />
						</div>
						<div class="f-row">
							<input type="submit" value="Search" />
						</div>
					</div>
					<div class="widget members">
						<h3>Our members</h3>
						<div id="members-list-options" class="item-options">
						  <a href="#">Newest</a>
						  <a class="selected" href="#">Active</a>
						  <a href="#">Popular</a>
						</div>
						<ul class="boxed">
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar1.jpg" alt="" /><span>Kimberly C.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar2.jpg" alt="" /><span>Alex J.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar3.jpg" alt="" /><span>Denise M.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar9.jpg" alt="" /><span>Jason H.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar8.jpg" alt="" /><span>Jennifer W.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar4.jpg" alt="" /><span>Anabelle Q.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar7.jpg" alt="" /><span>Thomas M.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar5.jpg" alt="" /><span>Michelle S.</span></a></div></li>
							<li><div class="avatar"><a href="my_profile.php"><img src="images/avatar6.jpg" alt="" /><span>Bryan A.</span></a></div></li>
						</ul>
					</div>
				</aside>
				<!--//content-->
			</div>
			<!--//row-->
		</div>
		<!--//wrap-->
	</main>
	<!--//main-->
	
@endsection