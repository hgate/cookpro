  <header class="head" role="banner">
    <div class="advetisement">
       <div class="wrap clearfix">
           <div class="add-center">
              <div class="add-left">
                 <img src="{{ asset('images/supermarket.png') }}">
              </div>
              <div class="add-left">
                 <img src="{{ asset('images/fresh.png') }}">
              </div>
              <div class="add-left">
                 <img src="{{ asset('images/peapod.png') }}">
              </div>
           </div>
           <div class="login_details">
              <nav class="user-nav right" role="navigation">
                <ul>
                @if(!Auth::check() && !Auth::guard('teacher')->check() && !Auth::guard('dietitian')->check())
                  <li><a href="{{ url('/register') }}" style="color:#f4716a;" title="My account"><span>Register</span></a></li>
                  <li><a href="{{ url('/login') }}" style="color:#f4716a;" title="My account"><span>login</span></a></li>
                  @else
                   @if(Auth::check())
                     <li><a href="{{ url('myAccount') }}" style="color:#f4716a;" title="My account"><span>{{ Auth::user()->name }}</span></a></li>
                     <li><a href="{{ url('/logout') }}" style="color:#f4716a;" title="Logout"><span>Logout</span></a></li>
                     @elseif(Auth::guard('teacher')->check())
                     <li><a href="{{ url('teacher/myAccount') }}" style="color:#f4716a;" title="My account"><span>{{ Auth::guard('teacher')->user()->name }}</span></a></li>
                     <li><a href="{{ url('/teacher/logout') }}" style="color:#f4716a;" title="Logout"><span>Logout</span></a></li>
                     @elseif(Auth::guard('dietitian')->check())
                     <li><a href="{{ url('dietitian/myAccount') }}" style="color:#f4716a;" title="My account"><span>{{ Auth::guard('dietitian')->user()->name }}</span></a></li>
                     <li><a href="{{ url('/dietitian/logout') }}" style="color:#f4716a;" title="Logout"><span>Logout</span></a></li>
                  @endif
                @endif                      
                  <li><a href="#contact.php" style="color:#f4716a;" title="Customer Care"><span>Customer Care</span></a></li>
                 </ul>
        </nav>
           </div>
       </div>
    </div>
        

    <div class="top-header">
      <div class="wrap clearfix">
        <div class="logo-dv">
          <a href="{{ url('/') }}" title="SocialChef" class="logo">
            <img src="{{ asset('images/ico/logo.png') }}" alt="SocialChef logo" />
          </a>
        </div>
        <nav class="main-nav" role="navigation" id="menu">
          <ul> 
            <li class="current-menu-item"><a href="{{ url("planWeeklyMenu") }}" title="Recipes"><span>On The Menu</span></a></li>
            <li><a href="{{ url("listrecipes") }}" title="Recipes"><span>Recipes</span></a></li>
            <li><a href="{{ url("teacher/cookingclasses") }}" title="CookingClasses"><span>Cooking Classes</span></a>
              <!--<ul>
               <li><a href="{{ url('cooking-catalog') }}" title="Home" style="color:#fff;">Cooking Catalog</a></li>
               <li><a href="{{ url('/cooking-membership') }}" title="Home" style="color:#fff;">Member Ship</a></li>
              </ul>-->
            </li>
            <li><a href="{{ url("dietitian/dietitianPlan") }}" title="Pricing tables"><span>Dietician Plan</span></a>
              <!--<ul>
                <li><a href="{{ url('choose-goal') }}" title="Home" style="color:#fff;">Choose Goal</a></li>
                <li><a href="{{ url('dietitian-membership') }}" title="Home"  style="color:#fff;">Member Ship</a></li>
              </ul>-->
            </li>
            <li><a href="{{ url("shop") }}" title="Recipes"><span>Shop</span></a></li>
            <li></li>
          </ul>
        </nav>
        <nav class="user-nav" role="navigation">
          <ul>
            <li class="light"><a href="{{ url("searchrecipe") }}" title="Search for recipes"><i class="ico i-search"></i> <span>Search for recipes</span></a></li>
            <li class="dark"><a href="{{ url("submitrecipe") }}" title="Submit a recipe"><i class="ico i-submitrecipe"></i> <span>Submit a recipe</span></a></li>
          </ul>
        </nav>
      </div>
    </div>
  </header>